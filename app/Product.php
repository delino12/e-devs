<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use EDEV\Service;

class Product extends Model
{
    /*
    |---------------------------------------------
    | ADD NEW PRODUCT
    |---------------------------------------------
    */
    public function addNewProduct($payload){
    	if($this->alreadyExist($payload)){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $payload->pname.' already exist!',
    		];
    	}else{
    		$new_product 				= new Product();
    		$new_product->name 			= $payload->pname;
    		$new_product->type 			= $payload->ptype;
    		$new_product->description 	= $payload->pdescription;
    		$new_product->type 			= $payload->ptype;
    		$new_product->status 		= "active";
    		if($new_product->save()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> $payload->pname.' has been added successfully!',
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Failed to add '.$payload->pname,
    			];
    		}
    	}

    	// return
    	return $data;
    }

    /*
    |---------------------------------------------
    | CHECK ALREADY EXISTED
    |---------------------------------------------
    */
    public function alreadyExist($payload){
    	$already_exist = Product::where("name", $payload->pname)->first();
    	if($already_exist == null){
    		return false;
    	}else{
    		return true;
    	}
    }

    /*
    |---------------------------------------------
    | LOAD PRODUCTS
    |---------------------------------------------
    */
    public function getProducts(){
    	$products = Product::all();
    	if(count($products) > 0){
    		$product_box = [];
    		foreach ($products as $pl) {
    			// get services 
    			$services 		= new Service();
    			$service_info 	= $services->getProductServices($pl->id);

    			$data = [
    				'id'  		=> $pl->id,
    				'name'  	=> $pl->name,
    				'type'  	=> $pl->type,
    				'details'  	=> $pl->descriptions,
    				'status'  	=> $pl->status,
    				'services'  => $service_info,
    				'date'  	=> $pl->created_at->diffForHumans(),
    			];

    			array_push($product_box, $data);
    		}
    	}else{
    		$product_box = [];
    	}

    	// return 
    	return $product_box;
    }

    /*
    |---------------------------------------------
    | LOAD PRODUCTS
    |---------------------------------------------
    */
    public function getCableProducts(){
        $products = Product::where('type', 'cable')->get();
        if(count($products) > 0){
            $product_box = [];
            foreach ($products as $pl) {
                // get services 
                $services       = new Service();
                $service_info   = $services->getProductServices($pl->id);

                $data = [
                    'id'        => $pl->id,
                    'name'      => $pl->name,
                    'type'      => $pl->type,
                    'details'   => $pl->descriptions,
                    'status'    => $pl->status,
                    'services'  => $service_info,
                    'date'      => $pl->created_at->diffForHumans(),
                ];

                array_push($product_box, $data);
            }
        }else{
            $product_box = [];
        }

        // return 
        return $product_box;
    }

    /*
    |---------------------------------------------
    | LOAD ONE PRODUCT
    |---------------------------------------------
    */
    public function getOneProduct($id){
    	$product = Product::where('id', $id)->first();
    	if($product !== null){
    		// get services 
			$services 		= new Service();
			$service_info 	= $services->getProductServices($id);
    		
    		$data = [
    			'id'  		=> $product->id,
				'name'  	=> $product->name,
				'type'  	=> $product->type,
				'details'  	=> $product->descriptions,
				'status'  	=> $product->status,
				'services'  => $service_info,
    			'date'  	=> $product->created_at->diffForHumans(),
    		];
    	}else{
    		$data = [];
    	}

    	// return
    	return $data;
    }
}
