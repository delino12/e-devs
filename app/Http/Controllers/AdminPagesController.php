<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Product;
use EDEV\Admin;
use Auth;

class AdminPagesController extends Controller
{
    // redirect not logged in;
    protected $redirectUrl = 'admin/login';

    /*
    |---------------------------------------------
    | AUTHENTICATE ADMIN PAGES
    |---------------------------------------------
    */
    public function __construct(){
        $this->middleware("auth:admin");
    }

    /*
    |---------------------------------------------
    | SHOW DASHBOARD
    |---------------------------------------------
    */
    public function dashboard(){
        $admin = Admin::where("email", Auth::user()->email)->first();
    	return view("__admin.dashboard", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW SETTINGS
    |---------------------------------------------
    */
    public function settings(){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.settings", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW USERS
    |---------------------------------------------
    */
    public function users(){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.users", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW TRANSACTIONS
    |---------------------------------------------
    */
    public function transactions(){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.transactions", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW AUDIT TRAIL
    |---------------------------------------------
    */
    public function audits(){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.audits", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW ONE PRODUCT
    |---------------------------------------------
    */
    public function viewOneProduct($id){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.view-product", compact('admin', 'id'));
    }
 	
 	/*
    |---------------------------------------------
    | LOGOUT ADMIN SECTION
    |---------------------------------------------
    */
    public function logout(){
        Auth::logout();
        return redirect('/admin/login');
    }
}
