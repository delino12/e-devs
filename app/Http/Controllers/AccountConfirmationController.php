<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Activation;

class AccountConfirmationController extends Controller
{
    /*
    |---------------------------------------------
    | VERIFY ACTIVATED ACCOUNT
    |---------------------------------------------
    */
    public function verify(Request $request){
    	$token = $request->token;
    	if(!$token){
    		// return redirect
    		return redirect("/");
    	}else{
    		$activation = new Activation();
    		$data 		= $activation->activateAccount($token);
    		$message 	= $data["message"];
    		// return back
    		return redirect("/")->with("message", $message);
    	}
    }
}
