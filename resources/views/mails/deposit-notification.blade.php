<!DOCTYPE html>
<html>
<head>
	<title>E-DEVS</title>
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
        body {
            font-family: 'Amiko';
            font-size: 12px;
        }
        table, td, th {    
            border: 1px solid #ddd;
            text-align: left;
        }
        table {
            border-collapse: collapse;
            width: 50%;
        }
        th, td {
            padding: 15px;
        }
    </style>
</head>
<body>
	<div style="padding: 10px;">
		<div class="card" style="font-size: 12px;box-shadow: 1px 1px 10px 1px solid #CCC;">
			<div class="card-header">
				
			</div>
			<div class="card-body">
				<h4>Transaction successful!</h4>
				<p>You have deposited &#8358;{{ $data["amount"] }}</p>
				<br />
				<table>
					<tr>
						<td>Transaction date:</td>
						<td>{{ date("D M Y h:i a") }}</td>
					</tr>

					<tr>
						<td>Transaction Type:</td>
						<td>{{ $data['trans_type'] }}</td>
					</tr>

					<tr>
						<td>Transaction ID:</td>
						<td>{{ $data['trans_ref'] }}</td>
					</tr>

					<tr>
						<td>Amount:</td>
						<td>&#8358; {{ $data['amount'] }}</td>
					</tr>

					<tr>
						<td>Charge:</td>
						<td>(&#8358; {{ $data['trans_fee'] }})</td>
					</tr>

					<tr>
						<td>Available Balance:</td>
						<td>&#8358; {{ $data['balance'] }}</td>
					</tr>
				</table>
				<p>
					<br /><br />
					Login to view transaction details.
					<br /><br />
					<button style="padding: 0.7em;border:1px solid #999;background-color: #FFF;">
						<a href="{{env("APP_URL")}}" style="text-decoration: none;">View Transaction</a>
					</button>
				</p>
				<br /><br />
				<div class="alert alert-danger">
					<p>
						<h4>Contact E-devs I.T Manager</h4>
					 	<strong>Name:</strong> Ekpoto Liberty Bernard <br />
					 	<strong>Email:</strong> support@e-devs.com <br />
					 	<strong>Phone:</strong> (+234) 81 271 60258
					</p>
				</div>
			</div>

			<div class="card-footer">
				Copyright &copy; {{ date("Y") }}
			</div>
		</div>
	</div>
</body>
</html>