<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('admin-assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('admin-assets/css/nifty.min.css') }}" rel="stylesheet">

    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ asset('admin-assets/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">

    <!--=================================================-->
    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ asset('admin-assets/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('admin-assets/plugins/pace/pace.min.js') }}"></script>

    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('admin-assets/css/demo/nifty-demo.min.css') }}" rel="stylesheet">

    <!--Custom scheme [ OPTIONAL ]-->
    <link href="{{ asset('admin-assets/css/themes/type-c/theme-navy.min.css') }}" rel="stylesheet">


    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{ asset('admin-assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">        
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
<body>
    <div id="container" class="effect aside-float aside-bright mainnav-sm page-fixedbar">
        <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
        @include('__components.admin-header')

        <div class="boxed">
            <!--CONTENT CONTAINER-->
            <!--===================================================-->
            <div id="content-container">
                @include('__components.admin-fixedbar');

                <!--Page content-->
                <!--===================================================-->
                <div id="page-content">
					@yield('contents')
                </div>
                <!--===================================================-->
                <!--End page content-->

            </div>
            <!--===================================================-->
            <!--END CONTENT CONTAINER-->
            
            @include('__components/admin-sidebar')
        </div>

        <!-- FOOTER -->
        <!--===================================================-->
        <footer id="footer">
            <p class="pad-lft">&#0169; {{ Date("Y") }} E-DEVS</p>
        </footer>
        <!--===================================================-->
        <!-- END FOOTER -->

        <!-- SCROLL PAGE BUTTON -->
        <!--===================================================-->
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
        <!--===================================================-->
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->

    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--jQuery [ REQUIRED ]-->
    <script src="{{ asset('admin-assets/js/jquery.min.js') }}"></script>

    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ asset('admin-assets/js/bootstrap.min.js') }}"></script>

    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ asset('admin-assets/js/nifty.min.js') }}"></script>
    
    <!--Flot Chart [ OPTIONAL ]-->
    <script src="{{ asset('admin-assets/plugins/flot-charts/jquery.flot.min.js') }}"></script>
	<script src="{{ asset('admin-assets/plugins/flot-charts/jquery.flot.categories.min.js') }}"></script>
	<script src="{{ asset('admin-assets/plugins/flot-charts/jquery.flot.orderBars.min.js') }}"></script>
	<script src="{{ asset('admin-assets/plugins/flot-charts/jquery.flot.tooltip.min.js') }}"></script>
	<script src="{{ asset('admin-assets/plugins/flot-charts/jquery.flot.resize.min.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>

    <!--DataTables [ OPTIONAL ]-->
    <script src="{{ asset('admin-assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-assets/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>

    <!-- numerial js -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

    <!--DataTables Sample [ SAMPLE ]-->
    <script src="{{ asset('admin-assets/js/demo/tables-datatables.js') }}"></script>

    @yield('scripts')

    <script type="text/javascript">
        // load modules
        loadAllUsers();
        displayPlatformBalance();
        displayPaystackBalance();
        displayTransactionSummary();
        displayMobileBalance();

        // load platform balance
        function displayPlatformBalance() {
            $.get('{{ url('admin/get/balance/2') }}', function(data) {
                $(".platform-balance").html(`
                    &#8358;${data.available}
                `);
            });
        }

        // load platform balance
        function displayMobileBalance() {
            $.get('{{ url('admin/get/balance/3') }}', function(data) {
                $(".mobile-balance").html(`
                    &#8358;${data}
                `);
            });
        }

        // load platform balance
        function displayPaystackBalance() {
            $.get('{{ url('admin/get/balance/1') }}', function(data) {
                if(data.status == true){
                    $(".paystack-balance").html(`
                        ${data.data.balance}
                    `);
                }
            });
        }

        // load platform balance
        function displayFlutterwaveBalance() {
            $.get('{{ url('admin/get/balance/4') }}', function(data) {
                if(data.status == true){
                    $(".flutterwave-balance").html(`
                        ${data.data.balance}
                    `);
                }
            });
        }

        // load platform transactions stats
        function displayTransactionSummary() {
            $.get('{{ url('admin/all/transaction/stats') }}', function(data) {
                $(".successful-transaction").html(`
                    ${data.success}
                `);
                $(".failed-transaction").html(`
                    ${data.failed}
                `);
                $(".total-deposit").html(`
                    ${data.deposit}
                `);
                $(".total-transfer").html(`
                    ${data.transfer}
                `);

                $(".total-withdraw").html(`
                    ${data.withdraw}
                `);
            });
        }

        // load all users
        function loadAllUsers() {
            $.get('{{ url('admin/get/all/users') }}', function(data) {
                $(".load-all-users").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    var avatar;
                    if(val.biodata.avatar == null){
                        avatar = `{{ asset('admin-assets/img/profile-photos/3.png') }}`;
                    }else{
                        avatar = val.biodata.avatar;
                    }

                    $(".load-all-users").append(`
                        <tr>
                            <td class="min-w-td">${sn}</td>
                            <td><img src="${avatar}" alt="Profile Picture" class="img-circle img-sm"></td>
                            <td><a class="btn-link" href="javascript:void(0);">${val.name}</a></td>
                            <td>${val.email}</td>
                            <td>
                                <span class="label label-table label-info">
                                    &#8358; ${val.account.balance}
                                </span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-default btn-hover-success demo-psi-pen-5 add-tooltip" href="javascript:void(0);" data-original-title="Edit" data-container="body"></a>
                                    <a class="btn btn-sm btn-default btn-hover-danger demo-pli-trash add-tooltip" href="javascript:void(0);" data-original-title="Delete" data-container="body"></a>
                                    <a class="btn btn-sm btn-default btn-hover-warning demo-pli-unlock add-tooltip" href="javascript:void(0);" data-original-title="Ban user" data-container="body"></a>
                                </div>
                            </td>
                        </tr>
                    `);
                });

                // datatable
                $("#users-table").dataTable();

                // total registered
                $(".total-registered").html(sn);
            });
        }
    </script>
</body>
</html>
