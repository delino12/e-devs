<?php

namespace EDEV\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransferConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $template_type;
    protected $data;

    public function __construct($template_type, $data)
    {
        $this->template_type = $template_type;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        if($this->template_type == "sender"){
            // send to sender
            return $this->subject("E-devs debit alert")->view('mails.transfer-notify-sender', compact('data'));
        }else{
            // send to receiver
            return $this->subject("E-devs credit alert")->view('mails.transfer-notify-receiver', compact('data'));
        }
        
    }
}
