<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use EDEV\Account;
use EDEV\Transaction;
use Auth;

class Airtime extends Model
{
    /*
    |---------------------------------------------
    | TOP UP AIRTIME
    |---------------------------------------------
    */
    public function topUpAirtime($user_id, $request){
    	
    	$mobile 	= $request->mobile;
    	$amount 	= $request->amount;
    	$network 	= $request->network;
    	$note 		= $request->note;

    	$topup_type = $request->topType;
    	// validate field
    	if(empty($network)){
    		return $data = [
    			'status' 	=> 'error',
    			'message' 	=> 'select a network provider',
    		];
    	}

    	if(empty($topup_type)){
    		return $data = [
    			'status' 	=> 'error',
    			'message' 	=> 'select topup recharge option',
    		];
    	}

        if(strlen($mobile) !== 11){
            return $data = [
                'status'    => 'error',
                'message'   => 'Invalid phone number',
            ];
        }

    	// check topup type
    	if($topup_type == "data"){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Data services is currently not available!',
    		];
            // check user account balance
            $check_account = new Account();
            if(!$check_account->verifyBalance($user_id, $amount)){
                $data = [
                    'status'    => 'error',
                    'message'   => 'insufficient balance!',
                ];
            }else{
                // process payload data bundle top up
                $data = $this->processApiTopupData($user_id, $mobile, $amount, $note, $network);
            }
    	}else{
    		// check user account balance
	    	$check_account = new Account();
	    	if(!$check_account->verifyBalance($user_id, $amount)){
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'insufficient balance!',
	    		];
	    	}else{
	    		// process payload artime recharge top up
	    		$data = $this->processApiTopupAirtime($user_id, $mobile, $amount, $note, $network);
	    	}
    	}

    	// return data
    	return $data;
    }

    /*
    |---------------------------------------------
    | PROCESS API TOPUP AIRTIME 
    |---------------------------------------------
    */
    public function processApiTopupAirtime($user_id, $mobile, $amount, $note, $network){

    	$endpoint 	= env("MOBILE_API_AIRTIME_ENDPOINT");
		$userid 	= env("MOBILE_API_USERID");
		$passwd 	= env("MOBILE_API_PASSWD");
		$postdata 	= "?userid=".$userid."&pass=".$passwd."&network=".$network."&phone=".$mobile."&amt=".$amount;
		$uri = $endpoint.$postdata;

        // init request
        $client         = new Client();
        $api_response   = $client->request("POST", $uri);
        $res            = $api_response->getBody()->getContents();

        $trans_type = "Airtime Recharge";

        // return result
	    return $this->apiResponse($user_id, $amount, $mobile, $note, $trans_type, $res);
    }

    /*
    |---------------------------------------------
    | PROCESS API TOPUP DATABUNDLE 
    |---------------------------------------------
    */
    public function processApiTopupData($user_id, $mobile, $amount, $note, $network){

        $endpoint   = env("MOBILE_API_DATA_ENDPOINT");
        $userid     = env("MOBILE_API_USERID");
        $passwd     = env("MOBILE_API_PASSWD");
        $postdata   = "?userid=".$userid."&pass=".$passwd."&network=".$network."&phone=".$mobile."&amt=".$amount;
        $uri = $endpoint.$postdata;

        // init request
        $client         = new Client();
        $api_response   = $client->request("POST", $uri);
        $res            = $api_response->getBody()->getContents();

        // transaction type 
        $trans_type = "Data Bundle Subscription";

        // return result
        return $this->apiResponse($user_id, $amount, $mobile, $note, $trans_type, $res);
    }


    /*
    |---------------------------------------------
    | GET API RESPONSE
    |---------------------------------------------
    */
    public function apiResponse($user_id, $amount, $mobile, $note, $trans_type, $api_response){

    	$filtered = explode("|", $api_response);
    	$res = [
    		"code" 		=> $filtered[0],
    		"message" 	=> $filtered[1]
    	];

    	if($res["code"] == 100){
    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> $res["message"],
    		];

            // status
            $status = "success";

            // debit user account
            $account = new Account();
            $account->debitAccount($user_id, $amount);

            // record transactions
            $transactions = new Transaction();
            $transactions->saveMobileTransaction($user_id, $amount, $mobile, $note, $trans_type, $status);

    	}elseif($res["code"] == 101){

    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];

    	}elseif($res["code"] == 102){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}elseif($res["code"] == 103){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}elseif($res["code"] == 104){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}elseif($res["code"] == 105){

    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}elseif($res["code"] == 106){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}elseif($res["code"] == 107){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}elseif($res["code"] == 108){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}elseif($res["code"] == 109){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $res["message"],
    		];
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'oops!, failed to proccess request',
    		];

            $status = "failed";

            // record transactions
            $transactions = new Transaction();
            $transactions->saveMobileTransaction($user_id, $amount, $mobile, $note, $trans_type, $status);
    	}

    	// return
    	return $data;
    }
}
