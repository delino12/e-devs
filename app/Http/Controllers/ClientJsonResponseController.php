<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Transaction;
use EDEV\Activation;
use EDEV\Account;
use EDEV\Bank;
use EDEV\User;
use EDEV\Product;
use EDEV\Business;
use EDEV\BioData;
use EDEV\Service;
use EDEV\Payment;
use Auth;

class ClientJsonResponseController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATE CLIENT
    |-----------------------------------------
    */
    public function __construct(){
        // body
        $this->middleware('auth');
    }
    
    /*
    |-----------------------------------------
    | LOAD USER TRANSACTIONS
    |-----------------------------------------
    */
    public function getUserTransaction(){
    	// body
    	$transaction 	= new Transaction();
    	$data 			= $transaction->loadUserTransaction();

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | LOAD USER TRANSACTIONS
    |-----------------------------------------
    */
    public function getSingleTransaction(Request $request){
        // body
        $transid        = $request->transid;
        $transaction    = new Transaction();
        $data           = $transaction->loadSingleTransaction($transid);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | GET USER ACCOUNT BALANCE
    |-----------------------------------------
    */
    public function getUserAccountBalance(){
        // body
        $account    = new Account();
        $data       = $account->getUserAccount();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | UPDATE USER BANK ACCOUNT INFORMATION
    |-----------------------------------------
    */
    public function addUserBankInfo(Request $request){
        // body
        $bank_number = $request->bank_number;
        $bank_name   = $request->bank_name;
        $bank_code   = $request->bank_code;

        // pass to Account
        $save_bank  = new Bank();
        $data       = $save_bank->addBankInformation($bank_number, $bank_name, $bank_code);

        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET TOTAL DEPOSIT BALANCE
    |---------------------------------------------
    */
    public function totalDeposit(){
        $user_id = Auth::user()->id;

        $transactions   = new Transaction();
        $data           = $transactions->getTotalCountDeposit($user_id);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET TOTAL WITHDRAWAL
    |---------------------------------------------
    */
    public function totalWithdraw(){
        $user_id = Auth::user()->id;
        
        $transactions   = new Transaction();
        $data           = $transactions->getTotalCountWithdraw($user_id);
        
        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET TOTAL SUCCESSFUL TRANSACTION
    |---------------------------------------------
    */
    public function totalSuccessfulTransaction(){
        $user_id = Auth::user()->id;
        
        $transactions   = new Transaction();
        $data           = $transactions->getTotalSuccessTrans($user_id);
        
        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET TOTAL PENDING TRANSACTIONS
    |---------------------------------------------
    */
    public function totalPendingTransaction(){
        $user_id = Auth::user()->id;
        
        $transactions   = new Transaction();
        $data           = $transactions->getTotalPendingTrans($user_id);
        
        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET TOTAL FAILED TRANSACTIONS
    |---------------------------------------------
    */
    public function totalFailedTransaction(){
        $user_id = Auth::user()->id;
        
        $transactions   = new Transaction();
        $data           = $transactions->getTotalFailedTrans($user_id);
        
        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET ACCOUNT ACTIVATION STATUS
    |---------------------------------------------
    */
    public function getAccountStatus(){
        $user_email = Auth::user()->email;

        $activation     = new Activation();
        $data           = $activation->getActivationStatus($user_email);
        
        // return response
        return response()->json($data); 
    }

    /*
    |---------------------------------------------
    | RESEND ACTIVATION LINK
    |---------------------------------------------
    */
    public function resendActivation(){
        $email = Auth::user()->email;
        
        $user = new User();
        $data = $user->resendNewActivationLink($email);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CREATE NEW BUSINESS
    |---------------------------------------------
    */
    public function createNewBusiness(Request $request){
        $new_business   = new Business();
        $data           = $new_business->addBusiness($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET USER BANK
    |---------------------------------------------
    */
    public function getUserBank(){
        $bank = new Bank();
        $data = $bank->getUserBank();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET USER AGENTS
    |---------------------------------------------
    */
    public function getUserAgents(){
        $business   = new Business();
        $data       = $business->getAllAgents();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET USER BUSINESS
    |---------------------------------------------
    */
    public function loadBusiness(){
        $business   = new Business();
        $data       = $business->getAllBusiness();

        // return response
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | UPLOAD USER AVATAR
    |-----------------------------------------
    */
    public function uploadAvatar(Request $request){
        // update profile image
        $update_data    = new BioData();
        $data           = $update_data->updateAvatar($request->avatar);
        
        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | UPDATE BIO DATA
    |---------------------------------------------
    */
    public function updateBiodata(Request $request){
        // update profile image
        $update_data    = new BioData();
        $data           = $update_data->updateProfile($request);

        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAD PROFILE INFORMATION
    |---------------------------------------------
    */
    public function loadProfileInfo(){
        // update profile image
        $profile_info   = new BioData();
        $data           = $profile_info->loadProfileInfo();

        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CHANGE PASSWORD
    |---------------------------------------------
    */
    public function changePassword(Request $request){
        $user = new User();
        $data = $user->changeUserPassword($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAD TRANSACTIONS STATS
    |---------------------------------------------
    */
    public function loadTransactionStats(){
        $user_id = Auth::user()->id;
        
        $transactions   = new Transaction();
        $data           = $transactions->getTransactionStats($user_id);
        
        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET DSTV PACKAGES
    |---------------------------------------------
    */
    public function getDstvPackages(){
        // $dstv = Product::where()->
    }

    /*
    |---------------------------------------------
    | GET ALL PRODUCTS
    |---------------------------------------------
    */
    public function getAllCableProducts(){
        $products   = new Product();
        $data       = $products->getCableProducts();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET PRODUCTS SERVICES
    |---------------------------------------------
    */
    public function getCableServices($id){
        $services   = new Service();
        $data       = $services->getProductServices($id);

        // return response
        return response()->json($data);   
    }
}
