<!--Fixedbar-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div class="page-fixedbar-container">
    <div class="page-fixedbar-content">
        <div class="nano">
            <div class="nano-content">

                <div class="pad-all">
                    <span class="pad-ver text-main text-sm text-uppercase text-bold">Total Incoming</span>
                    <p class="text-sm">{{ Date("F' d D, Y") }}</p>
                    <p class="text-2x text-main paystack-balance"></p>
                    <button class="btn btn-block btn-info mar-top">Paystack Balance</button>
                </div>

                <div class="pad-all">
                    <span class="pad-ver text-main text-sm text-uppercase text-bold">Total Incoming</span>
                    <p class="text-sm">{{ Date("F' d D, Y") }}</p>
                    <p class="text-2x text-main">&#8358;3,798.54</p>
                    <button class="btn btn-block btn-success mar-top">Flutterwave Balance</button>
                </div>

                <div class="pad-all">
                    <span class="pad-ver text-main text-sm text-uppercase text-bold">Total Incoming</span>
                    <p class="text-sm">{{ Date("F' d D, Y") }}</p>
                    <p class="text-2x text-main platform-balance">...</p>
                    <button class="btn btn-block btn-primary mar-top">E-DEVS Balance</button>
                </div>

                <div class="pad-all">
                    <span class="pad-ver text-main text-sm text-uppercase text-bold">Total Incoming</span>
                    <p class="text-sm">{{ Date("F' d D, Y") }}</p>
                    <p class="text-2x text-main mobile-balance">...</p>
                    <button class="btn btn-block btn-primary mar-top">Mobile Airtime Balance</button>
                </div>


                <hr class="new-section-xs">
                <div class="pad-hor">
                    <p class="pad-ver text-main text-sm text-uppercase text-bold">Transactions Count</p>
                </div>

                <!--===================================================-->
                <ul class="list-unstyled text-center pad-btm mar-no row">
                    <li class="col-xs-6">
                        <span class="text-2x text-normal text-main successful-transaction"></span>
                        <p class="text-muted mar-no">Successful</p>
                    </li>
                    <li class="col-xs-6">
                        <span class="text-2x text-normal text-main failed-transaction"></span>
                        <p class="text-muted mar-no">Failed</p>
                    </li>
                </ul>

                <hr class="new-section-xs">

                <p class="pad-all text-main text-sm text-uppercase text-bold">Additional Actions</p>

                <!--Simple Menu-->
                <div class="list-group bg-trans">
                    <a href="javascript:void(0);" class="list-group-item"><span class="label label-info pull-right">New</span><i class="demo-pli-credit-card-2 icon-lg icon-fw"></i> Payment Options</a>
                    <a href="javascript:void(0);" class="list-group-item"><i class="demo-pli-support icon-lg icon-fw"></i> Message Center</a>
                </div>

                <ul class="list-group pad-btm bg-trans">
                    <li class="list-header">
                        <p class="text-main text-sm text-uppercase text-bold mar-no">Public Settings</p>
                    </li>
                    <li class="list-group-item">
                        <div class="pull-right">
                            <input class="toggle-switch" id="demo-switch-4" type="checkbox" checked>
                            <label for="demo-switch-4"></label>
                        </div>
                        Online status
                    </li>
                    <li class="list-group-item">
                        <div class="pull-right">
                            <input class="toggle-switch" id="demo-switch-5" type="checkbox" checked>
                            <label for="demo-switch-5"></label>
                        </div>
                        Show offline contact
                    </li>
                    <li class="list-group-item">
                        <div class="pull-right">
                            <input class="toggle-switch" id="demo-switch-6" type="checkbox" checked>
                            <label for="demo-switch-6"></label>
                        </div>
                        Show my device icon
                    </li>
                </ul>

                <hr>
                <p class="pad-all text-main text-sm text-uppercase text-bold">Account Settings</p>
                <ul class="list-group bg-trans">
                    <li class="pad-top list-header">
                        <p class="text-main text-sm text-uppercase text-bold mar-no">Account Settings</p>
                    </li>
                    <li class="list-group-item">
                        <div class="pull-right">
                            <input class="toggle-switch" id="demo-switch-1" type="checkbox" checked>
                            <label for="demo-switch-1"></label>
                        </div>
                        <p class="mar-no text-main">Show my personal status</p>
                        <small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
                    </li>
                    <li class="list-group-item">
                        <div class="pull-right">
                            <input class="toggle-switch" id="demo-switch-2" type="checkbox" checked>
                            <label for="demo-switch-2"></label>
                        </div>
                        <p class="mar-no text-main">Show offline contact</p>
                        <small class="text-muted">Aenean commodo ligula eget dolor. Aenean massa.</small>
                    </li>
                    <li class="list-group-item">
                        <div class="pull-right">
                            <input class="toggle-switch" id="demo-switch-3" type="checkbox">
                            <label for="demo-switch-3"></label>
                        </div>
                        <p class="mar-no text-main">Invisible mode </p>
                        <small class="text-muted">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </small>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End Fixedbar-->