<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
	/*
    |---------------------------------------------
    | CHECK IF DEFAULT ADMIN ALREADY EXIST
    |---------------------------------------------
    */
    public function checkAlreadyExist(){
    	$check_admin = Admin::where("email", "admin@merriscoop.com")->first();
    	if($check_admin !== null){
    		return true;
    	}else{
    		return false;
    	} 
    }

    /*
    |---------------------------------------------
    | ADD DEFAULT ADMIN
    |---------------------------------------------
    */
    public function addDefaultAdmin(){
    	$default_admin 				= new Admin();
    	$default_admin->name 		= "EDEVS Admin";
    	$default_admin->email 		= "admin@e-devs.com";
    	$default_admin->password 	= bcrypt("Idorliberty12");
    	$default_admin->level 		= "alpha";
    	$default_admin->role 		= "CEO";
    	$default_admin->save();
    }
}
