<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use EDEV\User;
use Auth;

class Account extends Model
{
    /*
    |---------------------------------------------
    | USER RELATION WITH ACCOUNT
    |---------------------------------------------
    */
    public function user(){
        $relative       = User::class;
        $related_key    = "user_id";

        // return chain
        return $this->belongsTo($relative, $related_key);
    }

    /*
    |-----------------------------------------------------------------------------
    | GET USER ACCOUNT INFO
    |-----------------------------------------------------------------------------
    */
    public function getUserAccount(){
    	// body
    	$user_id = Auth::user()->id;
    	$account = Account::where('user_id', $user_id)->first();
        if($account !== null){
            $account = [
                'id'            => $account->id,
                'user_id'       => $account->user_id,
                'account_id'    => $account->account_id,
                'balance'       => number_format($account->balance, 2),
                'status'        => $account->status,
                'isActive'      => $account->isActive,
                'last_updated'  => $account->updated_at->diffForHumans(),
                'date_created'  => $account->created_at->diffForHumans()
            ];
        }else{
            $account = [];
        }
    	return $account;
    }

    /*
    |-----------------------------------------------------------------------------
    | GET USER ACCOUNT INFO
    |-----------------------------------------------------------------------------
    */
    public function getAccountInfoFromAdmin($user_id){
        // body
        $account = Account::where('user_id', $user_id)->first();
        if($account !== null){
            $account = [
                'id'            => $account->id,
                'user_id'       => $account->user_id,
                'account_id'    => $account->account_id,
                'balance'       => number_format($account->balance, 2),
                'status'        => $account->status,
                'isActive'      => $account->isActive,
                'last_updated'  => $account->updated_at->diffForHumans(),
                'date_created'  => $account->created_at->diffForHumans()
            ];
        }else{
            $account = [];
        }
        
        return $account;
    }

    /*
    |-----------------------------------------------------------------------------
    | CREDIT USER ACCOUNT
    |-----------------------------------------------------------------------------
    */
    public function creditAccount($user_id, $amount){
    	// body
    	$account 					= Account::where('user_id', $user_id)->first();
    	$update_account         	= Account::find($account->id);
        $update_account->balance 	= $update_account->balance + $amount;
        if($update_account->update()){
            return true;
        }else{
            return false;
        }
    }

    /*
    |-----------------------------------------------------------------------------
    | DEBIT USER ACCOUNT
    |-----------------------------------------------------------------------------
    */
    public function debitAccount($user_id, $amount){
    	// body
    	$account 					= Account::where('user_id', $user_id)->first();
    	$update_account         	= Account::find($account->id);
        $update_account->balance 	= $update_account->balance - $amount;
        if($update_account->update()){
            return true;
        }else{
            return false;
        }
    }

    /*
    |-----------------------------------------
    | QUERY USER ACCOUNT BALANCE STATE
    |-----------------------------------------
    */
    public function verifyBalance($user_id, $amount){
    	// body
    	$account = Account::where('user_id', $user_id)->first();
    	if($account->balance < $amount){
    		return false;
    	}else{
    		// balance is low
    		return true;
    	}
    }

    /*
    |---------------------------------------------
    | GET SUM TOTAL ALL USER BALANCE
    |---------------------------------------------
    */
    public function getTotalBalance(){
        $total = Account::sum("balance");

        $data = [
            'available' => number_format($total, 2),
        ];

        // return
        return $data;
    }
}
