<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Payment;
use EDEV\Account;
use EDEV\Transaction;
use EDEV\Transfer;
use Auth;

class ClientPaymentsController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATE CLIENT
    |-----------------------------------------
    */
    public function __construct(){
    	// body
    	$this->middleware('auth');
    }

    /*
    |-----------------------------------------
    | CLIENT DEPOSIT TO E-DEVS
    |-----------------------------------------
    */
    public function chargeCard(Request $request){
    	// body
    	$payment = new Payment();
    	$data 	 = $payment->chargeClientCard($request);

    	// return response.
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | CLIENT VERIFY CHARGE
    |-----------------------------------------
    */
    public function verifyCharge(Request $request){
        // body
        $payment = new Payment();
        $data    = $payment->verifyClientCharge($request);

        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CREATE TRANSFER RECIEPTS
    |---------------------------------------------
    */
    public function createReciept(Request $request){
        // body
        $transfer   = new Transfer();
        $data       = $transfer->initiateTransfer($request);

        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | TRANSTER TO SAME USER
    |---------------------------------------------
    */
    public function internalTransfer(Request $request){
        // body
        $transfer   = new Transfer();
        $data       = $transfer->sendAmount($request);

        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | FETCH ALL ACCEPTED BANK LIST
    |---------------------------------------------
    */
    public function getAcceptedBankList(){
        // body
        $transfer   = new Transfer();
        $data       = $transfer->fetchBankList();

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | RESOLVE BANK ACCOUNT INFORMATION
    |---------------------------------------------
    */
    public function resolveBankAccount(Request $request){
        // body
        $transfer   = new Transfer();
        $data       = $transfer->resolveBank($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | COMPLETE BANK TRANSFER
    |---------------------------------------------
    */
    public function completeTransfer(Request $request){

        if(empty($request->amount)){
            $data = [
                'status'    => 'error',
                'message'   => 'Amount is required!',
            ];
        }elseif(!is_numeric($request->amount)){
            $data = [
                'status'    => 'error',
                'message'   => 'Enter a valid amount',
            ];
        }else{
            // body
            $transfer   = new Transfer();
            $data       = $transfer->processExternalTansfer($request);
        }
        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | FETCH CABLE CUSTOMER INFORMATION
    |---------------------------------------------
    */
    public function fetchCustomerInfo(Request $request){
        $payments   = new Payment();
        $data       = $payments->getCustomerCableInfo($request);

        // return response
        return response()->json($data);
    }
}
