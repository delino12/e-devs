<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\User;
use Auth;

class LoginUserController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATION
    |-----------------------------------------
    */
    public function __construct(){
    	// body
    	$this->middleware('guest');
    }

    /*
    |-----------------------------------------
    | showLogin
    |-----------------------------------------
    */
    public function showLogin(){
    	// body
    	return view('__auth.login');
    }

    /*
    |-----------------------------------------
    | processLogin
    |-----------------------------------------
    */
    public function processLogin(Request $request){
    	// body
    	$email      = $request->email;
        $password   = $request->password;

        // verify login
        $check_exist = User::where('email', $email)->first();
        if($check_exist == null){

            $data = [
                'status'    => 'error',
                'message'   => 'Invalid Username/Email try again !'
            ];

        }else{

            // body
            if(Auth::attempt(['email' => $email, 'password' => $password])){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Login successful !'
                ];

            }else{

                $data = [
                    'status'    => 'error',
                    'message'   => 'Invalid password... try again !'
                ];
            }
        }

        // return response.
        return response()->json($data);
    }
}
