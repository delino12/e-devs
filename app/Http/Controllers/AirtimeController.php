<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Transaction;
use EDEV\Account;
use EDEV\Airtime;
use EDEV\Bank;
use Auth;

class AirtimeController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATE CLIENT
    |-----------------------------------------
    */
    public function __construct(){
        // body
        $this->middleware('auth');
    }

    /*
    |---------------------------------------------
    | PROCESS AIRTIME & DATA TOPUP
    |---------------------------------------------
    */
    public function processTopupMobile(Request $request){
    	$user_id 			= Auth::user()->id;
    	$process_airtime 	= new Airtime();
    	$data 				= $process_airtime->topUpAirtime($user_id, $request);

    	// return response
    	return response()->json($data);
    }
}
