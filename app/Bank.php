<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Bank extends Model
{
    /*
    |---------------------------------------------
    | ADD BANK INFORMATION
    |---------------------------------------------
    */
    public function addBankInformation($bank_number, $bank_name, $bank_code){
    	$user_id = Auth::user()->id;

    	if(strlen($bank_number) == 10){
	    	// check if bank info already exist
	    	if($this->verifyAlreadyExist($user_id, $bank_number)){
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> $bank_number.' already exist !',
	    		];

	    	}else{

	    		// close other bank information
	    		$update_old = Bank::where("user_id", $user_id)->get();
	    		if(count($update_old) > 3){
	    			$data = [
	    				'status' 	=> 'error',
	    				'message' 	=> 'You can not add more than 3 bank account information!',
	    			];
	    		}elseif(count($update_old) > 0){
	    			// update old bank information
	    			foreach ($update_old as $od) {
	    				$update 			= Bank::find($od->id);
	    				$update->isActive 	= 0;
	    				$update->update();
	    			}


	    			// add new bank information
		    		$new_info 					= new Bank();
		    		$new_info->user_id 			= $user_id;
		    		$new_info->account_bank 	= $bank_name;
		    		$new_info->account_code 	= $bank_code;
		    		$new_info->account_number 	= $bank_number;
		    		$new_info->isActive 		= 1;
		    		if($new_info->save()){
		    			$data = [
		    				'status' 	=> 'success',
		    				'message' 	=> 'Bank information created successfully!',
		    			];
		    		}else{
		    			$data = [
		    				'status' 	=> 'error',
		    				'message' 	=> 'Bank creation was not successful!',
		    			];
		    		}
	    		}else{
	    			// add new bank information
		    		$new_info 					= new Bank();
		    		$new_info->user_id 			= $user_id;
		    		$new_info->account_bank 	= $bank_name;
		    		$new_info->account_code 	= $bank_code;
		    		$new_info->account_number 	= $bank_number;
		    		$new_info->isActive 		= 1;
		    		if($new_info->save()){
		    			$data = [
		    				'status' 	=> 'success',
		    				'message' 	=> 'Bank information created successfully!',
		    			];
		    		}else{
		    			$data = [
		    				'status' 	=> 'error',
		    				'message' 	=> 'Bank creation was not successful!',
		    			];
		    		}
	    		}
	    	}
	    }else{
	    	$data = [
	    		'status'  => 'error',
	    		'message' => 'oops, Would like you to verify your account number once again!',
	    	];
	    }

    	// return
    	return $data;

    }

    /*
    |---------------------------------------------
    | CHECK IF USER ALREADY HAVE ACCOUNT
    |---------------------------------------------
    */
    public function verifyAlreadyExist($user_id, $bank_number){
    	$check_account = Bank::where([["user_id", $user_id], ["account_number", $bank_number]])->first();
    	if($check_account !== null){
    		return true;
    	}else{
    		return false;
    	}
    }

    /*
    |---------------------------------------------
    | GET USER BANK
    |---------------------------------------------
    */
    public function getUserBank(){
    	$user_id 	= Auth::user()->id;
    	$user_bank 	= Bank::where("user_id", $user_id)->get();
    	
    	// return
    	return $user_bank;
    }

    /*
    |---------------------------------------------
    | UPDATE BANK INFORMATION
    |---------------------------------------------
    */
    public function updateBank($bank_id){
    	$update_bank 					= Bank::find($bank_info->id);
		$update_bank->account_bank 		= $bank_name;
		$update_bank->account_code 		= $bank_code;
		$update_bank->account_number 	= $bank_number;
		$update_bank->isActive 			= 1;
		
		if($update_bank->update()){
			$data = [
				'status' 	=> 'success',
				'message' 	=> 'Bank information updated successfully!',
			];
		}else{
			$data = [
				'status' 	=> 'error',
				'message' 	=> 'Update bank information was not successful!',
			];
		}

		// return
		return $data;
    }
}
