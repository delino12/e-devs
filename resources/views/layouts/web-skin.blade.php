<!doctype html>
<html lang="en">
  <head>
    <title>@yield('title')</title>
    <link rel="icon" href="{{asset('/images/logo.png')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="E-Devs, The future is flexible, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business." />
    <meta name="keywords" content="Bank Transfers, Payment Solutions, Bills & Cable Subscriptions, Airtime & Data Purchase, Api Friendly" />
    <meta name="author" content="Ekpoto Liberty" />
    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="E-Devs is a payment platform helping software developers, businesses and other SMEs with payment integration and automation. No integration stress, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business. "/>
    <meta property="og:image" content="https://e-devs.com/images/feature.gif"/>
    <meta property="og:url" content="https://e-devs.com"/>
    <meta property="og:site_name" content="E-Devs"/>
    <meta property="og:description" content="We at E-Devs we say payment should not be stressful, transfer, pay and receive money anytime or everytime."/>

    {{-- twitter --}}
    <meta name="twitter:title" content="E-Devs is a payment platform helping businesses and other SMEs with payment collection and automation. No integration stress, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business. " />
    <meta name="twitter:image" content="https://e-devs.com/images/feature.gif" />
    <meta name="twitter:url" content="https://e-devs.com" />
    <meta name="twitter:card" content="E-Devs, The future is flexible, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business." />

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@6.6.2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">

    <link rel="stylesheet" href="/fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/fonts/flaticon/font/flaticon.css">

    <!-- Theme Style -->
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/toast.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132345822-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-132345822-1');
    </script>


    <!-- Start of Async Drift Code -->
    <script>
      "use strict";
      !function() {
        var t = window.driftt = window.drift = window.driftt || [];
        if (!t.init) {
          if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
          t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
          t.factory = function(e) {
            return function() {
              var n = Array.prototype.slice.call(arguments);
              return n.unshift(e), t.push(n), t;
            };
          }, t.methods.forEach(function(e) {
            t[e] = t.factory(e);
          }), t.load = function(t) {
            var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
            o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
            var i = document.getElementsByTagName("script")[0];
            i.parentNode.insertBefore(o, i);
          };
        }
      }();
      drift.SNIPPET_VERSION = '0.3.1';
      drift.load('mpdx3e9kz73m');
    </script>
    <!-- End of Async Drift Code -->
  </head>
  <body>
    <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
    <header role="banner" style="box-shadow: 1px 1px 4px 1px #CCC;">
      <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
          <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{ asset('images/logo.png') }}" /> e-Devs</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse navbar-dark" id="navbarsExample05">
            <ul class="navbar-nav mx-auto">
              @if(Auth::check())
                <li class="nav-item">
                  <a class="nav-link active" href="javascript:void(0);">
                    <i class="fa fa-home"></i> Home
                  </a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link active" href="/register">Get Started</a>
                </li>
              @endif

              @if(Auth::check())
                <li class="nav-item">
                  <a class="nav-link active" href="/dashboard">
                    <i class="fa fa-dashboard"></i> 
                    Dashboard
                  </a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-code-fork"></i>  Documentation
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdown05">
                    <a class="dropdown-item" href="{{url('docs')}}">Api Reference</a>
                    <a class="dropdown-item" href="{{url('docs/guide')}}">Developer's Guide</a>
                  </div>
                </li>
              @else
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="courses.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Instant Payment</a>
                  <div class="dropdown-menu" aria-labelledby="dropdown04">
                    <a class="dropdown-item" href="{{url('dashboard')}}">Quick Funds Transfer</a>
                    <a class="dropdown-item" href="{{url('bills')}}">Pay Bills</a>
                    <a class="dropdown-item" href="{{url('dashboard')}}">Buy Airtime & Databundle</a>
                    <a class="dropdown-item" href="{{url('withdraw')}}">Quick Withdraw</a>
                    <a class="dropdown-item" href="{{url('deposit')}}">Instant Deposit</a>
                  </div>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Documentation</a>
                  <div class="dropdown-menu" aria-labelledby="dropdown05">
                    <a class="dropdown-item" href="{{url('docs')}}">Api Reference</a>
                    <a class="dropdown-item" href="{{url('docs/guide')}}">Developer's Guide</a>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{url('support')}}">Support</a>
                </li>
              @endif

              @if(Auth::check())
                
              @else
                <li class="nav-item">
                  <a class="nav-link" href="{{url('blog')}}">Blog</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('about')}}">About</a>
                </li>
              @endif
            </ul>
            <ul class="navbar-nav absolute-right">
              
            @if(Auth::check())
              <li class="nav-item">
                <a href="{{url('profile')}}" class="nav-link">{{ Auth::user()->email }}</a>
              </li>
              <li class="nav-item">
                <a href="{{url('logout')}}" class="nav-link"><i class="fa fa-sign-out"></i> Logout</a>
              </li>
            @else
              <li class="nav-item">
                <a href="{{url('login')}}" class="nav-link">Login</a>
              </li>
              <li class="nav-item">
                <a href="{{url('register')}}" class="nav-link">Register</a>
              </li>
            @endif
            </ul>
            
          </div>
        </div>
      </nav>
    </header>
    <!-- END header -->
    @yield('contents')
      
    <footer class="site-footer" style="background-image: url(images/main-bg.jpg);">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-4">
            <h3>About</h3>
            <p>E-Devs is a payment platform that allow one to recieved payment or send payment from anywhere.</p>
          </div>
          <div class="col-md-6 ml-auto">
            <div class="row">
              <div class="col-md-4">
                <ul class="list-unstyled">
                  <li><a href="#">About Us</a></li>
                  <li><a href="{{ url('/docs') }}">Api Documentation</a></li>
                  <li><a href="#">Developer's Guide</a></li>
                </ul>
              </div>
              <div class="col-md-4">
                <ul class="list-unstyled">
                  <li><a href="#">Bills Payment</a></li>
                  <li><a href="#">Funds Transfer</a></li>
                  <li><a href="#">Send Payment</a></li>
                  <li><a href="#">Receive Payment</a></li>
                </ul>
              </div>
              <div class="col-md-4">
                <ul class="list-unstyled">
                  <li><a href="#">Partners</a></li>
                  <li><a href="#">Support</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="#">Location</a></li>
                  <li><a href="{{ url('policy') }}">Privacy</a></li>
                  <li><a href="{{ url('terms') }}">Terms</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <p>
              E-devs Copyright &copy;<script>document.write(new Date().getFullYear());</script> 
              All rights reserved
            </p>
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->
    
    <!-- loader -->
    <div id="loader" class="show fullscreen">
      <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/>
      </svg>
    </div>

    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/jquery-migrate-3.0.0.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/jquery.waypoints.min.js"></script>
    <script src="/js/jquery.stellar.min.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/typed.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@6.6.2/dist/sweetalert2.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="/js/toast.js"></script>
    <script type="text/javascript">
      @if(Auth::check())
        // Ajax data
        displayBalance();
        // get user account balance
        function displayBalance() {
          $.get('{{url('load/user/account/balance')}}', function(data) {
            $(".account_balance").html(`&#8358; ${data.balance}`);
          });
        }
      @endif
    </script>
    @yield('scripts')
  </body>
</html>