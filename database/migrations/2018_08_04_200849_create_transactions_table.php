<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('trans_ref');
            $table->string('trans_note');
            $table->string('trans_type');
            $table->string('trans_status');
            $table->string('trans_desc');
            $table->string('trans_from');
            $table->string('trans_to');
            $table->decimal('amount', 18, 4);
            $table->decimal('fee', 18, 4);
            $table->decimal('balance', 18, 4);
            $table->boolean('isDeleted', false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
