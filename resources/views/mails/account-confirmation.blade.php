<!DOCTYPE html>
<html>
<head>
	<title>E-DEVS</title>
	<link href='https://fonts.googleapis.com/css?family=Amiko' rel='stylesheet'>
	<style type="text/css">
        body {
            font-family: 'Amiko';
            font-size: 12px;
        }
        table, td, th {    
            border: 1px solid #ddd;
            text-align: left;
        }
        table {
            border-collapse: collapse;
            width: 50%;
        }
        th, td {
            padding: 15px;
        }
    </style>
</head>
<body>
	<div style="padding: 10px;">
		<div class="card" style="font-size: 12px;box-shadow: 1px 1px 10px 1px solid #CCC;">
			<div class="card-header">
				
			</div>
			<div class="card-body">
				<h4>Account confirmation!</h4>
				<p>
					Hi! <br /><br />

					You have signed up on the E-devs platform, 
					but your account is not activated. To ensure the security of your information, 
					please verify your email address. To do this, click on the button below.
				</p>

				<br /><br />
				<button style="padding: 0.7em;border:1px solid #999;background-color: #FFF;">
					<a href="{{env("APP_URL")}}/account/confirmation/?token={{$data["token"]}}" style="text-decoration: none;">Confirm</a>
				</button>

				<br /><br />
				<div class="alert alert-danger">
					<p>
						<h4>Contact E-devs I.T Manager</h4>
					 	<strong>Name:</strong> Ekpoto Liberty Bernard <br />
					 	<strong>Email:</strong> support@e-devs.com <br />
					 	<strong>Phone:</strong> (+234) 81 271 60258
					</p>
				</div>
			</div>

			<div class="card-footer">
				Copyright &copy; {{ date("Y") }}
			</div>
		</div>
	</div>
</body>
</html>