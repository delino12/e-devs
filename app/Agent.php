<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    /*
    |---------------------------------------------
    | GENERATE AGENT
    |---------------------------------------------
    */
    public function generateAgent($biz_id, $total_agent){
    	// create new agents
    	for($i = 0; $i < $total_agent; $i++){
    		$agent_id 	= rand(000, 999).rand(000, 999);
    		$agent_code = $agent_id;

    		// create new agent
    		$this->addNewAgent($biz_id, $agent_id, $agent_code);
    	}
    }


    /*
    |---------------------------------------------
    | ADD NEW AGENT
    |---------------------------------------------
    */
    public function addNewAgent($biz_id, $agent_id, $agent_code){
    	// create new agent
    	$new_agent 				= new Agent();
    	$new_agent->biz_id		= $biz_id;
    	$new_agent->agent_code 	= $agent_id;
    	$new_agent->agent_pass 	= md5($agent_code);
    	$new_agent->status 		= "inactive";
    	$new_agent->save();	
    }
}
