<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas sidebar-dark" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="{{ url('dashboard') }}">
        <i class="menu-icon icon-diamond"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('transfer') }}">
        <i class="menu-icon icon-speedometer"></i>
        <span class="menu-title">Quick Transfer</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('deposit') }}">
        <i class="menu-icon icon-wallet"></i>
        <span class="menu-title">Quick Deposit</span>
      </a>
    </li>
    <li class="nav-item d-none d-md-block">
      <a class="nav-link" href="{{ url('bills') }}">
        <i class="menu-icon icon-layers"></i>
        <span class="menu-title">Pay Bills</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('airtime') }}" >
        <i class="menu-icon icon-equalizer"></i>
        <span class="menu-title">Airtime & Databundle</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('transactions') }}">
        <i class="menu-icon icon-grid"></i>
        <span class="menu-title">Transactions</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('notifications') }}">
        <i class="menu-icon icon-support"></i>
        <span class="menu-title">Notifications</span>
        <div class="badge badge-danger">4</div>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('withdraw') }}">
        <i class="menu-icon icon-wallet"></i>
        <span class="menu-title">Quick Withdraw</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('business') }}">
        <i class="menu-icon icon-briefcase"></i>
        <span class="menu-title">Add Business</span>
      </a>
    </li>
    
    <li class="nav-item">
      <a class="nav-link" href="{{ url('setting') }}">
        <i class="menu-icon icon-settings"></i>
        <span class="menu-title">Settings</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('logout') }}">
        <i class="menu-icon icon-logout"></i>
        <span class="menu-title">Logout</span>
      </a>
    </li>
  </ul>
</nav>