@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
	E-DEVS | Admin Settings
@endsection

{{-- contents --}}
@section('contents')
	<!--Activated Users Chart-->
	<!--===================================================-->
	<div class="panel">
	    <!--Chart information-->
	    <div class="panel-body">
	        <div class="row mar-top">
	            <div class="col-md-5">
	                <h3 class="text-main text-normal text-2x mar-no">Settings</h3>
	                
	            </div>
	            <div class="col-md-7">
	            	<span class="pull-right">
	            		<button class="btn btn-primary btn-sm" onclick="showAddProductModal()">
	            			<i class="fa fa-plus"></i> Add Product
	            		</button>
	            	</span>
	                <table class="table">
	                	<thead>
	                		<tr>
	                			<th>Products</th>
	                			<th>Status</th>
	                			<th>Option</th>
	                		</tr>
	                	</thead>
	                	<tbody class="load-products">
	                		<tr>
		                		<td></td>
		                		<td></td>
		                		<td></td>
		                	</tr>
	                	</tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
	<!--===================================================-->
	<!--End Activated Users chart-->

	<div class="row">
	    <div class="col-lg-12">
	    </div>
	</div>

	@include('__components.admin-modals')
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		// load modules
		loadProducts();

		// show add modal product
		function showAddProductModal() {
			$("#add-product-modal").modal('show');
		}

		// add product
		$("#add-product-btn").click(function(){
			var token 		= $("#token").val();
			var name 		= $("#product_name").val();
			var type 		= $("#product_type").val();
			var description = $("#product_description").val();

			var params = {
				_token: token,
				pname: name,
				ptype: type,
				pdescription: description
			};

			// admin add product
			$.post('{{ url('admin/add/product') }}', params, function(data, textStatus, xhr) {
				if(data.status == "success"){
					swal(
						"success",
						data.message,
						data.status
					);
					// hide modal
					$("#add-product-modal").modal('hide');
					loadProducts();
				}else{
					swal(
						"oops",
						data.message,
						data.status
					);
				}
			});
		});

		// get products
		function loadProducts() {
			$.get('{{ url('admin/get/product') }}', function(data) {
				$(".load-products").html("");
				$.each(data, function(index, val) {
					$(".load-products").append(`
						<tr>
							<td>${val.name}</td>
							<td>${val.status}</td>
							<td><a href="{{ url('admin/view/product') }}/${val.id}"> view more</a></td>
						</tr>
					`);
				});
			});
		}
	</script>
@endsection