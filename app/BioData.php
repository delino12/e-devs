<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use EDEV\User;
use Auth;

class BioData extends Model
{
    /*
    |-----------------------------------------
    | ADD USER RELATIONSHIP 
    |-----------------------------------------
    */
    public function user(){
        // body
        $relative   = User::class;
        $relative_key = "user_id";
        return $this->belongsTo($relative);
    }

    /*
    |-----------------------------------------
    | UPLOAD USER AVATAR
    |-----------------------------------------
    */
    public function updateAvatar($avatar){
        // body
        $user_id = Auth::user()->id;
        // find and updates
        $bio_data = BioData::where('user_id', $user_id)->first();
        if($bio_data !== null){
            $update_biodata             = BioData::find($bio_data->id);
            $update_biodata->avatar     = $avatar;
            $update_biodata->update();
            $data = [
                'status'    => 'success',
                'message'   => 'profile updated successfully !'
            ];

        }else{

            $data = [
                'status'    => 'error',
                'message'   => 'Update your profile information before updating profile image! ',
            ];            

            // $new_update             = new BioData();
            // $new_update->user_id    = $user_id;
            // $new_update->avatar     = $avatar;
            // if($new_update->save()){
            //     $data = [
            //         'status'    => 'success',
            //         'message'   => 'profile updated successfully !'
            //     ];
            // }else{
            //     $data = [
            //         'status'    => 'error',
            //         'message'   => 'could not find a valid user bio data'
            //     ];
            // }
        }
        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | UPDATE PROFILE INFORMATION
    |---------------------------------------------
    */
    public function updateProfile($payload){
        // body
        $user_id = Auth::user()->id;
        // find and updates
        $bio_data = BioData::where('user_id', $user_id)->first();
        if($bio_data !== null){
            $update_biodata             = BioData::find($bio_data->id);
            $update_biodata->firstname  = $payload->firstname;
            $update_biodata->lastname   = $payload->lastname;
            $update_biodata->phone      = $payload->phone;
            $update_biodata->avatar     = $payload->avatar;
            $update_biodata->gender     = $payload->gender;
            $update_biodata->country    = $payload->country;
            $update_biodata->dob        = $payload->dob;
            $update_biodata->address    = $payload->address;
            $update_biodata->state      = $payload->state;
            $update_biodata->avatar     = $payload->avatar;
            $update_biodata->update();

            $data = [
                'status'    => 'success',
                'message'   => 'profile updated successfully !'
            ];

            // update user 
            $update_user        = User::find($user_id);
            $update_user->name  = $payload->firstname.' '.$payload->lastname;
            $update_user->email = $payload->email;
            $update_user->update();

        }else{

            $new_update             = new BioData();
            $new_update->user_id    = $user_id;
            $new_update->firstname  = $payload->firstname;
            $new_update->lastname   = $payload->lastname;
            $new_update->phone      = $payload->phone;
            $new_update->avatar     = $payload->avatar;
            $new_update->gender     = $payload->gender;
            $new_update->country    = $payload->country;
            $new_update->dob        = $payload->dob;
            $new_update->address    = $payload->address;
            $new_update->state      = $payload->state;
            $new_update->avatar     = $payload->avatar;
            if($new_update->save()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'profile updated successfully !'
                ];

                // update user 
                $update_user        = User::find($user_id);
                $update_user->name  = $payload->firstname.' '.$payload->lastname;
                $update_user->email = $payload->email;
                $update_user->update();
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'could not find a valid user bio data'
                ];
            }
        }
        // return
        return $data;
    }


    /*
    |---------------------------------------------
    | LOAD USER PROFILE INFORMATION
    |---------------------------------------------
    */
    public function loadProfileInfo(){
        // body
        $user_id = Auth::user()->id;

        // find and updates
        $bio_data = BioData::where('user_id', $user_id)->first();
        if($bio_data !== null){
            $data = [
                'user_id'   => $bio_data->user_id,
                'firstname' => ucfirst($bio_data->firstname),
                'lastname'  => ucfirst($bio_data->lastname),
                'phone'     => $bio_data->phone,
                'avatar'    => $bio_data->avatar,
                'email'     => Auth::user()->email,
                'gender'    => ucfirst($bio_data->gender),
                'country'   => ucfirst($bio_data->country),
                'dob'       => date("d M Y", strtotime($bio_data->dob)),
                'address'   => $bio_data->address,
                'state'     => ucfirst($bio_data->state),
                'date'      => $bio_data->updated_at->diffForHumans()
            ];
        }else{
            // mutate bio data
            $new_biodata            = new BioData();
            $new_biodata->user_id   = $user_id;
            $new_biodata->save();

            // find and updates
            $bio_data = BioData::where('user_id', $user_id)->first();
            if($bio_data !== null){
                $data = [
                    'user_id'   => $bio_data->user_id,
                    'firstname' => ucfirst($bio_data->firstname),
                    'lastname'  => ucfirst($bio_data->lastname),
                    'phone'     => $bio_data->phone,
                    'avatar'    => $bio_data->avatar,
                    'email'     => Auth::user()->email,
                    'gender'    => ucfirst($bio_data->gender),
                    'country'   => ucfirst($bio_data->country),
                    'dob'       => date("d M Y", strtotime($bio_data->dob)),
                    'address'   => $bio_data->address,
                    'state'     => ucfirst($bio_data->state),
                    'date'      => $bio_data->updated_at->diffForHumans()
                ];
            }else{
                $data = [];
            }
        }

        // return 
        return $data;
    }


    /*
    |---------------------------------------------
    | LOAD USER PROFILE INFORMATION
    |---------------------------------------------
    */
    public function loadProfileInfoFromAdmin($user_id){
        // find and updates
        $bio_data = BioData::where('user_id', $user_id)->first();
        if($bio_data !== null){
            $data = [
                'user_id'   => $bio_data->user_id,
                'firstname' => ucfirst($bio_data->firstname),
                'lastname'  => ucfirst($bio_data->lastname),
                'phone'     => $bio_data->phone,
                'avatar'    => $bio_data->avatar,
                'gender'    => ucfirst($bio_data->gender),
                'country'   => ucfirst($bio_data->country),
                'dob'       => date("d M Y", strtotime($bio_data->dob)),
                'address'   => $bio_data->address,
                'state'     => ucfirst($bio_data->state),
                'date'      => $bio_data->updated_at->diffForHumans()
            ];
        }else{

            // get information from user
            $user = User::where("id", $user_id)->first();

            $names = explode(" ", $user->name);
            if(count($names) > 0){
                $firstname  = $names[0];
                $lastname   = $names[1];
            }else{
                $firstname  = $names[0];
                $lastname   = "-";
            }

            // mutate bio data
            $new_biodata            = new BioData();
            $new_biodata->user_id   = $user_id;
            $new_biodata->firstname = $firstname;
            $new_biodata->lastname  = $lastname;
            $new_biodata->gender    = "none";
            $new_biodata->save();

            // find and updates
            $bio_data = BioData::where('user_id', $user_id)->first();
            if($bio_data !== null){
                $data = [
                    'user_id'   => $bio_data->user_id,
                    'firstname' => ucfirst($bio_data->firstname),
                    'lastname'  => ucfirst($bio_data->lastname),
                    'phone'     => $bio_data->phone,
                    'avatar'    => $bio_data->avatar,
                    'gender'    => ucfirst($bio_data->gender),
                    'country'   => ucfirst($bio_data->country),
                    'dob'       => date("d M Y", strtotime($bio_data->dob)),
                    'address'   => $bio_data->address,
                    'state'     => ucfirst($bio_data->state),
                    'date'      => $bio_data->updated_at->diffForHumans()
                ];
            }else{
                $data = [];
            }
        }

        // return 
        return $data;
    }
}
