@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
	E-DEVS | Admin Dashboard
@endsection

{{-- contents --}}
@section('contents')
	<!--Activated Users Chart-->
	<!--===================================================-->
	<div class="panel">
	    <!--Chart information-->
	    <div class="panel-body">
	        <div class="row mar-top">
	            <div class="col-md-5">
	                <h3 class="text-main text-normal text-2x mar-no">Activated Users</h3>
	                <h5 class="text-uppercase text-muted text-normal">Report for last 7-days ago</h5>
	                <hr class="new-section-xs">
	                <div class="row mar-top">
	                    <div class="col-sm-5">
	                        <div class="text-lg"><p class="text-5x text-thin text-main mar-no total-registered"></p></div>
	                        <p class="text-sm">Since 1st Oct 2018, <span class="total-registered"></span> peoples already registered</p>
	                    </div>
	                    <div class="col-sm-7">
	                        <div class="list-group bg-trans mar-no">
	                            <a class="list-group-item" href="javascript:void(0);"><i class="demo-pli-information icon-lg icon-fw"></i> User Details</a>
	                            <a class="list-group-item" href="javascript:void(0);"><i class="demo-pli-mine icon-lg icon-fw"></i> Usage Profile</a>
	                            <a class="list-group-item" href="javascript:void(0);"><span class="label label-info pull-right">New</span><i class="demo-pli-credit-card-2 icon-lg icon-fw"></i> Payment Options</a>
	                        </div>
	                    </div>
	                </div>
	                <button class="btn btn-pink mar-ver">View Details</button>
	            </div>
	            <div class="col-md-7">
	                <hr class="new-section-xs bord-no">
	                <ul class="list-inline text-center">
	                    <li><span class="label label-info">354</span> Students</li>
	                    <li><span class="label label-warning">74</span> Parents</li>
	                    <li><span class="label label-default">25</span> Teachers</li>
	                </ul>
	            </div>
	        </div>
	    </div>
	</div>
	<!--===================================================-->
	<!--End Activated Users chart-->

	<div class="row">
	    <div class="col-lg-3">
	        <div class="row">
	            <div class="col-sm-3 col-lg-6">

	                <!--Tile-->
	                <!--===================================================-->
	                <div class="panel panel-primary panel-colorful">
	                    <div class="pad-all text-center">
	                        <span class="text-3x text-thin total-deposit"></span>
	                        <p>Deposit</p>
	                        <i class="fa fa-money"></i>
	                    </div>
	                </div>
	                <!--===================================================-->


	                <!--Tile-->
	                <!--===================================================-->
	                <div class="panel panel-warning panel-colorful">
	                    <div class="pad-all text-center">
	                        <span class="text-3x text-thin total-transfer"></span>
	                        <p>Transfer</p>
	                        <i class="fa fa-exchange"></i>
	                    </div>
	                </div>
	                <!--===================================================-->

	            </div>
	            <div class="col-sm-3 col-lg-6">

	                <!--Tile-->
	                <!--===================================================-->
	                <div class="panel panel-purple panel-colorful">
	                    <div class="pad-all text-center">
	                        <span class="text-3x text-thin total-withdraw"></span>
	                        <p>Withdraw</p>
	                        <i class="fa fa-credit-card"></i>
	                    </div>
	                </div>
	                <!--===================================================-->

	                <!--Tile-->
	                <!--===================================================-->
	                <div class="panel panel-dark panel-colorful">
	                    <div class="pad-all text-center">
	                        <span class="text-3x text-thin total-bills">0</span>
	                        <p>Bills</p>
	                        <i class="demo-psi-receipt-4 icon-lg"></i>
	                    </div>
	                </div>
	                <!--===================================================-->
	            </div>
	            <div class="col-sm-6 col-lg-12">

	            </div>
	        </div>
	    </div>
	    <div class="col-lg-9">
	        <div class="panel">
	            <div class="panel-body">
	                <div class="table-responsive">
	                    <table class="table table-vcenter mar-top" id="users-table" cellspacing="0" width="100%">
	                        <thead>
	                            <tr>
	                                <th class="min-w-td">#</th>
	                                <th class="min-w-td">User</th>
	                                <th>Full Name</th>
	                                <th>Email</th>
	                                <th>Balance</th>
	                                <th class="text-center">Actions</th>
	                            </tr>
	                        </thead>
	                        <tbody class="load-all-users"></tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		
	</script>
@endsection