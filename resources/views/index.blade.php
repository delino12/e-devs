@extends('layouts.web-skin')

{{-- title section --}}
@section('title')
  Online Payment | E-Devs
@endsection

{{-- contents --}}
@section('contents')
	<div style="height: 4rem;"></div>
  <section class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img src="{{ asset('images/intro.gif') }}">
        </div>
        <div class="col-md-6">
          <div style="box-shadow: 0rem 0rem 0.5rem 0rem rgba(0,0,0,0.5);border-radius: 0rem;padding: 15px;">
            <p style="font-size: 16px;font-weight: 100;">Quick Funds Transfer</p>
            <p style="font-size: 16px;font-weight: 100;">Airtime & Databundle Purchase</p>
            <p style="font-size: 16px;font-weight: 100;">Instant Withdraw</p>
            <p style="font-size: 16px;font-weight: 100;">Receive Money via E-Devs Wallet</p>
            <p style="font-size: 16px;font-weight: 100;">Pay Dstv, Gotv & Startimes Bills</p>
            <p style="font-size: 16px;font-weight: 100;">Send Money using Our Mobile Money Wallet</p>
            <p style="font-size: 16px;font-weight: 100;">Start your Business using E-Devs Multi-Agent App</p>
          </div>
        </div>
      </div>
      <div style="height: 100px;"></div>
      <div class="row">
        <div class="col-md-6">
          <div class="col-md-6">
          <p style="font-size: 16px;font-weight: 100;">Coming soon</p>
          <p style="font-size: 16px;font-weight: 100;">Android & iOS</p>
          <div class="row">
            <div class="col-md-6">
              <img src="{{ asset('images/google.png') }}" width="130" height="40">
            </div>
            <div class="col-md-6">
              <img src="{{ asset('images/apple.png') }}" width="130" height="40">
            </div>
          </div>
        </div>
        </div>
        <div class="col-md-6">
          <img src="{{ asset('images/payment.gif') }}">
        </div>
      </div>
    </div>
  </section>
  <!-- END section -->
@endsection

{{-- scripts --}}
@section('scripts')
  
@endsection