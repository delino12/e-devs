<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\User;
use EDEV\Activation;
use EDEV\Wallet;

class RegisterUserController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATION
    |-----------------------------------------
    */
    public function __construct(){
    	// body
    	$this->middleware('guest');
    }

    /*
    |-----------------------------------------
    | SHOW REGISTER FORM
    |-----------------------------------------
    */
    public function showRegister(){
    	// body
    	return view('__auth.register');
    }

    /*
    |-----------------------------------------
    | PROCESS REGISTRATION
    |-----------------------------------------
    */
    public function processRegister(Request $request){
    	// body
        $new_user = new User();
        if($new_user->alreadyExistEmail($request)){
            $data = [
                'status'  => 'error',
                'message' => $request->email.' already exist!'
            ];
        }elseif($new_user->addUser($request)){
            $data = [
                'status'  => 'success',
                'message' => 'Registration successful!'
            ];
        }else{
            $data = [
                'status'  => 'error',
                'message' => 'Registration not successful!'
            ];
        }

        // return response.
        return response()->json($data);
    }
}
