@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
  E-DEVS | Admin View Product
@endsection

{{-- contents --}}
@section('contents')
  <!--Activated Users Chart-->
  <!--===================================================-->
  <div class="panel">
    <!--Chart information-->
    <div class="panel-body">
      <div class="row mar-top">
          <div class="col-md-12">
            <a href="{{ url('admin/settings') }}" class="btn btn-default">
              <i class="fa fa-arrow-left"></i> Back
            </a>
          </div>
      </div>
      <hr />
      <div class="row mar-top">  
        <div class="col-md-5">
          <h3 class="text-main text-normal text-2x mar-no product_name"></h3> 
        </div>

        <div class="col-md-7">
          <span class="pull-right">
            <button class="btn btn-primary btn-sm" onclick="showAddServicesModal()">
              <i class="fa fa-plus"></i> Add Services
            </button>
          </span>
        </div>

        <div class="col-md-12">
          <div class="row services-card"></div>
          <div class="row services-airtime"></div>
          <div class="row services-bundle"></div>
        </div>
      </div>
    </div>
  </div>
  <!--===================================================-->
  <!--End Activated Users chart-->

  <div class="row">
      <div class="col-lg-12">
      </div>
  </div>

  @include('__components.admin-modals')
@endsection

{{-- scripts --}}
@section('scripts')
  <script type="text/javascript">
    // load modules
    loadServices();

    // show add modal product
    function showAddServicesModal() {
      $("#add-services-modal").modal('show');
    }

    // add product
    $("#add-service-btn").click(function(){
      var token     = $("#token").val();
      var name      = $("#service_name").val();
      var price     = $("#service_price").val();

      var params = {
        _token: token,
        name: name,
        price: price,
        product_id: '{{ $id }}'
      };

      // admin add product
      $.post('{{ url('admin/add/service') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          // hide modal
          $("#add-services-modal").modal('hide');
          swal(
            "success",
            data.message,
            data.status
          );
          loadServices();
        }else{
          $("#add-services-modal").modal('hide');
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });
    });

    // get products
    function loadServices() {
      $.get('{{ url('admin/get/services') }}/{{ $id }}', function(data) {
        $(".services-card").html("");
        $(".services-airtime").html("");

        if(data.name == 'Airtime'){
          $(".services-airtime").html(`
            <h1 class="lead">AIRTIME (Voice Call/SMS)</h1>
            <hr />
            <div class="col-md-6">
              <h1 class="lead">MTN</h1>
              <hr />
              <div class="mtn-services"></div>
            </div>

            <div class="col-md-6">
              <h1 class="lead">GLO</h1>
              <hr />
              <div class="glo-services"></div>
            </div>

            <div class="col-md-6">
              <h1 class="lead">AIRTEL</h1>
              <hr />
              <div class="airtel-services"></div>
            </div>

            <div class="col-md-6">
              <h1 class="lead">9MOBILE</h1>
              <hr />
              <div class="etisalat-services"></div>
            </div>
          `);

          // get category set
          $.get('{{ url('admin/airtime/service') }}/{{ $id }}', function(data) {
            /*optional stuff to do after success */
            $(".mtn-services").html("");
            $(".glo-services").html("");
            $(".airtel-services").html("");
            $(".etisalat-services").html("");
            $.each(data.services.airtime.MTN, function(index, val) {
              $(".mtn-services").append(`
                <div class="col-md-4">
                  <div class="card">
                    <div class="card-body">
                      <h1 class="lead">${val.package}</h1>
                      <h4>&#8358;${numeral(val.price).format('0,0.00')}</h4>
                    </div>
                  </div>
                </div>
              `)
            });

            $.each(data.services.airtime.GLO, function(index, val) {
              $(".glo-services").append(`
                <div class="col-md-4">
                  <div class="card">
                    <div class="card-body">
                      <h1 class="lead">${val.package}</h1>
                      <h4>&#8358;${numeral(val.price).format('0,0.00')}</h4>
                    </div>
                  </div>
                </div>
              `)
            });

            $.each(data.services.airtime.AIRTEL, function(index, val) {
              $(".airtel-services").append(`
                <div class="col-md-4">
                  <div class="card">
                    <div class="card-body">
                      <h1 class="lead">${val.package}</h1>
                      <h4>&#8358;${numeral(val.price).format('0,0.00')}</h4>
                    </div>
                  </div>
                </div>
              `)
            });

            $.each(data.services.airtime.ETISALAT, function(index, val) {
              $(".etisalat-services").append(`
                <div class="col-md-4">
                  <div class="card">
                    <div class="card-body">
                      <h1 class="lead">${val.package}</h1>
                      <h4>&#8358;${numeral(val.price).format('0,0.00')}</h4>
                    </div>
                  </div>
                </div>
              `)
            });
            
          });
        }else{
          $.each(data.services, function(index, val) {
             $(".services-card").append(`
              <div class="col-md-4">
                <div class="card">
                  <div class="card-body">
                    <h1 class="lead">${val.package}</h1>
                    <h4>&#8358;${numeral(val.price).format('0,0.00')}</h4>
                  </div>
                </div>
              </div>
            `)
          });
        }
      });
    }
  </script>
@endsection