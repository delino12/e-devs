{{-- Add Bank Modal --}}
<div id="add-bank-modal" class="modal fade small" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title"><h4>Add Bank Information</h4></div>
      </div>
      <div class="modal-body">
        <form method="post" onsubmit="return updateBankAccount()">
          <div class="row">
              <div class="form-group col-sm-6">
                  <label for="bank-number">Account number</label>
                   <input type="text" class="form-control" maxlength="10" id="bank-number" placeholder="Enter account no Eg. 010000000" required="">
              </div>
              <div class="form-group col-sm-6">
                  <label for="bank-name">Bank Name</label>
                  <select id="bank-name" class="form-control"> 
                      <option value=""> -- select bank --</option>
                  </select>
              </div>
              <div class="form-group col-sm-6">
                  <button class="btn btn-primary">
                      <i class="fa fa-edit"></i> Add
                  </button>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- Add Business Modal --}}
<div id="add-business-modal" class="modal fade small" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title"><h4>Add New Business Information</h4></div>
      </div>
      <div class="modal-body p-3">
        <form method="post" onsubmit="return createNewBusiness();">
          <div class="row">
            <div class="form-group col-sm-12">
                <label for="business_name"><i class="fa fa-home"></i> Business Name</label>
                <input type="text" class="form-control" maxlength="34" id="business_name" placeholder="Eg, Cable & Bills Solutions" required="">
            </div>

            <div class="form-group col-sm-12">
                <label for="business_name"><i class="fa fa-users"></i> Total No of Agent</label>
                <input type="number" min="1" class="form-control" maxlength="34" id="total_agent" placeholder="Eg, 20" required="">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <div class="form-check form-check-flat">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="prod_1"> Dstv Subscriptions
                  </label>
                </div>
                <div class="form-check form-check-flat">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="prod_2"> Gotv Subscriptions
                  </label>
                </div>
                <div class="form-check form-check-flat">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="prod_3"> Bank Transfer & Deposit
                  </label>
                </div>
                <div class="form-check form-check-flat">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="prod_4"> Airtime & Data Bundle
                  </label>
                </div>
                <div class="form-check form-check-flat">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="prod_5"> SunKing Subscription
                  </label>
                </div>
                <div class="form-check form-check-flat">
                  <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="prod_6"> 
                    Electricity & Water Bills (currently not available)
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group col-sm-12">
              <button class="btn btn-primary">
                <i class="fa fa-lock"></i> Add new business
              </button>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- Add & Update Profile Modal --}}
<div id="update-profile-modal" class="modal fade small" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title"><h4>Add Bank Information</h4></div>
      </div>
      <div class="modal-body pt-2">
        <form method="post" onsubmit="return updateBioData()">
            <input type="hidden" id="avatar">
            <div class="row">
              @if(!empty($user->name))
                <?php
                  $names = explode(" ", $user->name);
                ?>
                <div class="form-group col-md-6">
                    <label for="first-name">First Name</label>
                     <input type="text" class="form-control" maxlength="10" id="first-name" placeholder="First name here" required="" value="{{ $names[0] }}">
                </div>
                <div class="form-group col-md-6">
                    <label for="last-name">Last Name</label>
                    <input type="text" class="form-control" maxlength="10" id="last-name" placeholder="Last name here" required="" value="{{ $names[1] }}">
                </div>
              @else
                <div class="form-group col-md-6">
                    <label for="first-name">First Name</label>
                     <input type="text" class="form-control" maxlength="10" id="first-name" placeholder="First name here" required="">
                </div>
                <div class="form-group col-md-6">
                    <label for="last-name">Last Name</label>
                    <input type="text" class="form-control" maxlength="10" id="last-name" placeholder="Last name here" required="">
                </div>
              @endif
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" class="form-control" maxlength="50" id="email" placeholder="Enter email" value="{{ $user->email }}" required="">
              </div>
              <div class="form-group col-md-6">
                  <label for="phone">Phone</label>
                  <input type="text" class="form-control" maxlength="11" id="phone" placeholder="Enter mobile phone" required="">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                  <label for="nationality">Gender</label>
                  <select class="form-control" id="gender">
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
              </div>
              <div class="form-group col-md-6">
                  <label for="date">Date of Birth</label>
                  <input type="date" class="form-control" id="date_of_birth" required="">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                  <label for="country">Nationality</label>
                  <select class="form-control" id="country">
                    <option value="nigeria">Nigeria</option>
                  </select>
              </div>
              <div class="form-group col-md-6">
                  <label for="state">State</label>
                  <select class="form-control" id="state">
                    <option value="">-- select --</option>
                  </select>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                  <label for="bank-number">Address</label>
                  <textarea class="form-control" rows="5" cols="10" maxlength="300" id="address" placeholder="Enter address"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                  <button class="btn btn-primary">
                      <i class="fa fa-edit"></i> Save & Update
                  </button>
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- Deposit Modal --}}
<div id="deposit-modal" class="modal fade small" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Card Deposit</h4>
        </div>
        <div class="modal-body">  
          <ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
              <li class="nav-item">
                  <a style="padding: 0.8em;" class="nav-link active" data-toggle="pill" href="#nav-tab-card">
                  <i class="fa fa-credit-card"></i> Credit Card</a>
              </li>
              <li class="nav-item">
                  <a style="padding: 0.8em;" class="nav-link" data-toggle="pill" href="#nav-tab-bank">
                  <i class="fa fa-university"></i>  Bank Transfer</a>
              </li>
          </ul>
          <div class="tab-content">
              <div class="tab-pane fade show active" id="nav-tab-card">
                  <form class="deposit-form" role="form" onsubmit="return processDeposit()" autocomplete="off">
                      <div class="form-group">
                          <h3 class="text-center" id="title-deposit-amount"></h3>
                          <hr />
                      </div>
                      <div class="form-group" id="cl-1">
                          <label for="cardNumber">Card number</label>
                          <div class="input-group">
                              <input type="number" pattern="[0-9]*" class="form-control" id="cc_nn" placeholder="Enter card no" onkeydown="verifyCardBin()">
                              <div class="input-group-append">
                                  <span class="input-group-text text-muted">
                                    <span class="cc_type">
                                      <i class="fab fa-cc-visa"></i> 
                                      <i class="fab fa-cc-mastercard"></i>  
                                    </span>
                                  </span>
                              </div>
                          </div>
                      </div> <!-- form-group.// -->

                      <div class="row" id="cl-2">
                          <div class="col-sm-8">
                              <div class="form-group">
                                  <label><span class="hidden-xs">Expiration</span> </label>
                                  <div class="input-group">
                                      <select class="form-control" id="cc_exp_mo">
                                          <option value="01">01</option>
                                          <option value="02">02</option>
                                          <option value="03">03</option>
                                          <option value="04">04</option>
                                          <option value="05">05</option>
                                          <option value="06">06</option>
                                          <option value="07">07</option>
                                          <option value="08">08</option>
                                          <option value="09">09</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                      </select>
                                      <select class="form-control" id="cc_exp_yr">
                                          <option value="18">2018</option>
                                          <option value="19">2019</option>
                                          <option value="20">2020</option>
                                          <option value="21">2021</option>
                                          <option value="22">2022</option>
                                          <option value="23">2023</option>
                                          <option value="23">2024</option>
                                          <option value="23">2025</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
                                  <input type="password" id="cc_cvv" title="Three digits at back of your card" maxlength="3" pattern="\d{3}" class="form-control" required="" placeholder="***">
                              </div> <!-- form-group.// -->
                          </div>
                      </div> <!-- row.// -->
                      <div class="form-group" align="middle">
                        <button style="padding: 0.8em;" id="init-transaction" class="btn btn-primary" type="button">
                          <span id="submit-btn">Proceed</span> 
                          <span id="loading-registration" style="display: none;"> 
                            <img src="{{asset('svg/loading.svg')}}"> We're processing your transaction!
                          </span>
                        </button>
                      </div>
                  </form>

                  <form class="verify-deposit" style="display: none;" onsubmit="return verifyTransaction();">
                      <div class="form-group row">
                          <div class="col-md-12 text-center">
                              <div id="charge_message"></div>
                          </div>
                      </div>

                      <div class="form-group row">
                          <input type="hidden" id="charge_refrence" value="">
                          <input type="hidden" id="charge_verify_type" value="">
                          <div class="col-md-3"></div>
                          <div class="col-md-6" id="verification-method"></div>
                          <div class="col-md-3"></div>
                      </div>
                      <hr>
                      <div class="form-group row">
                          <div class="col-md-3"></div>
                          <div class="col-md-6">
                              <button type="submit" class="btn btn-success">
                                  Verify 
                                  <img src="svg/loading.svg" class="verify-loading" style="display: none;">
                              </button>
                          </div>
                          <div class="col-md-3"></div>
                      </div>
                  </form>
              </div> <!-- tab-pane.// -->
              <div class="tab-pane fade" id="nav-tab-bank">
                  <p>Bank account details</p>
                  <dl class="param">
                    <dt>BANK: </dt>
                    <dd>Guaranty Trust Bank (GTB)</dd>
                  </dl>
                  <dl class="param">
                    <dt>Accaunt number:</dt>
                    <dd>0137803827</dd>
                  </dl>
                  <dl class="param">
                    <dt>Account Name:</dt>
                    <dd>E-Devs Limited</dd>
                  </dl>
                  <p>
                      <strong style="color:#F00;">Note:</strong> 
                      Deposit via bank require all users to use the teller number provided by the bank teller. 
                      Input tell number into <b><i>deposit with teller form.</i></b> to credit your e-devs wallet balance 
                  </p>
              </div>
          </div> <!-- tab-content .// -->
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>

{{-- Add Bank Modal --}}
<div id="show-auth-modal" class="modal fade small" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title"><h4>Authentication (Enter Active password)</h4></div>
      </div>
      <div class="modal-body">
        <form method="post" onsubmit="return completePasswordChange()">
          <input type="hidden" id="verified_new_password" name="">
          <div class="row">
              <div class="form-group col-md-12">
                  <label for="old-password">Enter Password</label>
                   <input type="password" class="form-control" maxlength="10" id="old_password" placeholder="*******" required="">
              </div>
          </div>
          <div class="row">
              <div class="form-group col-md-12">
                  <button class="btn btn-primary">
                      <i class="fa fa-edit"></i> Verify
                  </button>
              </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>