{{-- Add Product Modal --}}
<div class="modal" id="add-product-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title">Create new product</h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label>Product Name</label>
                    <input type="text" class="form-control" id="product_name" placeholder="What is the product name" name="" required="">
                </div>
                <div class="form-group">
                    <label>Type</label>
                    <select class="form-control" id="product_type">
                        <option value="">--select--</option>
                        <option value="cable">Cable</option>
                        <option value="airtime">Airtime</option>
                        <option value="databundle">Databundle</option>
                        <option value="transfer">Fund Transfer</option>
                        <option value="ekedc">EKEDC (Electricity)</option>
                        <option value="phcn">PHCN (Electricity)</option>
                        <option value="solar">SunKing (Solar)</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="product_description" cols="10" rows="5" placeholder="Describe this product..."></textarea>
                </div>

                <div class="form-group">
                    <div id="add-product-success text-success"></div>
                    <div id="add-product-error text-danger"></div>
                    <div id="add-product-info text-info"></div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button class="btn btn-primary" id="add-product-btn" type="button">Save changes</button>
            </div>
        </div>
    </div>
</div>


{{-- Add Services Modal --}}
<div class="modal" id="add-services-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                <h4 class="modal-title">Create new product</h4>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label>Service Name</label>
                    <input type="text" class="form-control" id="service_name" placeholder="What is the service name" name="" required="">
                </div>
                <div class="form-group">
                    <label>Amount(&#8358;)</label>
                    <input type="number" step="any" min="0" class="form-control" id="service_price" placeholder="00.00" name="" required="">
                </div>

                <div class="form-group">
                    <div id="add-service-success text-success"></div>
                    <div id="add-service-error text-danger"></div>
                    <div id="add-service-info text-info"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button class="btn btn-primary" id="add-service-btn" type="button">Save changes</button>
            </div>
        </div>
    </div>
</div>