<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use EDEV\Mail\AirtimeTopupConfirmationMail;
use EDEV\Mail\SendDepositNotifyMail;
use EDEV\Mail\TransferConfirmationMail;
use EDEV\Account;
use EDEV\BankTransferLog;
use EDEV\User;
use Auth;

class Transaction extends Model
{
    /*
    |-----------------------------------------
    | LOG USER TRANSACTION
    |-----------------------------------------
    */
    public function saveTransaction($results, $amount, $trans_type = "deposit"){
    	// body
    	$user_id 		= Auth::user()->id;
    	$trans_to 		= "admin@e-devs.com";
    	$trans_desc 	= "E-DEVS Quick deposit";
    	$trans_status 	= "pending";
    	$trans_ref		= $results["data"]["reference"]; 
    	$amount 		= $amount;
    	$trans_from		= Auth::user()->email;
    	$trans_note		= "Wallet Deposit";
    	
    	$fee 	 = 0 * $amount;
    	$balance = $fee + $amount;

    	$save_transaction		 		= new Transaction();
    	$save_transaction->user_id 		= $user_id;
    	$save_transaction->trans_ref 	= $trans_ref;
    	$save_transaction->trans_note 	= $trans_note;
    	$save_transaction->trans_type 	= $trans_type;
    	$save_transaction->trans_status = $trans_status;
    	$save_transaction->trans_desc 	= $trans_desc;
    	$save_transaction->trans_from 	= $trans_from;
    	$save_transaction->trans_to 	= $trans_to;
    	$save_transaction->amount 		= $amount;
    	$save_transaction->fee 			= $fee;
    	$save_transaction->balance 		= $balance;
    	$save_transaction->isDeleted 	= false;
    	$save_transaction->save();
    }

    /*
    |-----------------------------------------
    | RECORD AIRTEL DATA
    |-----------------------------------------
    */
    public function saveMobileTransaction($user_id, $amount, $mobile, $note, $trans_type, $status){
        // body
        $trans_to       = $mobile;
        $trans_desc     = "Mobile Purchase";
        $trans_status   = $status;
        $trans_ref      = 'E-DEVS-ASC-'.rand(000,999).rand(111,999).rand(222,999);
        $trans_from     = Auth::user()->email;
        $trans_note     = $note;

        if(empty($trans_note)){
            $trans_note = "none";
        }
        
        $fee     = 0.00;
        $balance = $fee + $amount;

        $save_transaction               = new Transaction();
        $save_transaction->user_id      = $user_id;
        $save_transaction->trans_ref    = $trans_ref;
        $save_transaction->trans_note   = $trans_note;
        $save_transaction->trans_type   = $trans_type;
        $save_transaction->trans_status = $trans_status;
        $save_transaction->trans_desc   = $trans_desc;
        $save_transaction->trans_from   = $trans_from;
        $save_transaction->trans_to     = $trans_to;
        $save_transaction->amount       = $amount;
        $save_transaction->fee          = $fee;
        $save_transaction->balance      = $balance;
        $save_transaction->isDeleted    = false;
        $save_transaction->save();
    }

    /*
    |---------------------------------------------
    | SAVE INTERNAL TRANSFER TRANSACTION
    |---------------------------------------------
    */
    public function saveInternalTransferTransaction($user_id, $account_id, $amount, $note){
        // body
        
        $trans_desc     = "E-DEVS Internal Transfer";
        $trans_status   = "success";
        $trans_ref      = 'E-DEVS-ASC-'.rand(000,999).rand(111,999).rand(222,999);
        $sender         = Account::where("user_id", $user_id)->first();
        $receiver       = Account::where("account_id", $account_id)->first();

        $trans_to       = $receiver->account_id;
        $trans_from     = $sender->account_id;
        $trans_note     = $note;
        $trans_type     = "Wallet Transfer";

        if(empty($trans_note)){
            $trans_note = "none";
        }
        
        $fee     = 0.00;
        $balance = $fee + $amount;

        $save_transaction               = new Transaction();
        $save_transaction->user_id      = $user_id;
        $save_transaction->trans_ref    = $trans_ref;
        $save_transaction->trans_note   = $trans_note;
        $save_transaction->trans_type   = $trans_type;
        $save_transaction->trans_status = $trans_status;
        $save_transaction->trans_desc   = $trans_desc;
        $save_transaction->trans_from   = $trans_from;
        $save_transaction->trans_to     = $trans_to;
        $save_transaction->amount       = $amount;
        $save_transaction->fee          = $fee;
        $save_transaction->balance      = $balance;
        $save_transaction->isDeleted    = false;
        $save_transaction->save();

        // get user account name
        if(!empty($sender->user->name)){
            $sender->account_id = $sender->user->name;
        }

        // get user account name
        if(!empty($receiver->user->name)){
            $receiver->account_id = $receiver->user->name;
        }

        // send mail to sender
        $sender_mail_data = [
            "trans_type"        => $trans_type,
            "trans_ref"         => $trans_ref,
            "amount_sent"       => number_format($amount, 2),
            "sender_balance"    => number_format($sender->balance, 2),
            "receiver_id"       => $receiver->account_id,
            "sender_mail"       => $sender->user->email
        ];
        $this->sendMailToSender($sender_mail_data);

        // send mail to receiver
        $receiver_mail_data = [
            "trans_type"        => $trans_type,
            "trans_ref"         => $trans_ref,
            "amount_sent"       => number_format($amount, 2),
            "receiver_balance"  => number_format($receiver->balance, 2),
            "sender_id"         => $sender->account_id,
            "receiver_mail"     => $receiver->user->email
        ];
        $this->sendMailToReceiver($receiver_mail_data);

        return true;
    }

    /*
    |---------------------------------------------
    | SAVE EXTERNAL TRANSFER TRANSACTION
    |---------------------------------------------
    */
    public function saveExternalTransferTransaction($user_id, $res_data, $amount, $note, $payload){
        // body
        
        $trans_desc     = "E-DEVS Bank Transfer";
        $trans_status   = "success";
        $trans_ref      = 'E-DEVS-ASC-'.rand(000,999).rand(111,999).rand(222,999);
        $sender         = Account::where("user_id", $user_id)->first();
        // $receiver       = Account::where("account_id", $account_id)->first();

        $trans_to       = $payload["data"]["transfer_code"];
        $trans_from     = $sender->account_id;
        $trans_note     = $note;
        $trans_type     = "Bank Transfer";

        if(empty($trans_note)){
            $trans_note = "none";
        }
        
        $fee     = 0.00;
        $balance = $fee + $amount;

        $save_transaction               = new Transaction();
        $save_transaction->user_id      = $user_id;
        $save_transaction->trans_ref    = $trans_ref;
        $save_transaction->trans_note   = $trans_note;
        $save_transaction->trans_type   = $trans_type;
        $save_transaction->trans_status = $trans_status;
        $save_transaction->trans_desc   = $trans_desc;
        $save_transaction->trans_from   = $trans_from;
        $save_transaction->trans_to     = $trans_to;
        $save_transaction->amount       = $amount;
        $save_transaction->fee          = $fee;
        $save_transaction->balance      = $balance;
        $save_transaction->isDeleted    = false;
        $save_transaction->save();

        $log_bank_transfer = new BankTransferLog();
        $log_bank_transfer->addLog($user_id, $res_data);

        return true;
    }

    /*
    |-----------------------------------------
    | VERIFY PENDING TRANSACTION
    |-----------------------------------------
    */
    public function verifyPendingTransaction($result){
    	// body
    	$trans_ref 		= $result["data"]["tx"]["txRef"];
    	$charge_status 	= $result["data"]["data"]["responsemessage"];
    	$charge_code    = $result["data"]["data"]["responsecode"];

    	if($charge_code == "Approved"){
    		$transaction = Transaction::where('trans_ref', $trans_ref)->first();
	    	if($transaction !== null){
	    		// find and update transaction
	    		$update_transaction 				= Transaction::find($transaction->id);
	    		$update_transaction->trans_status 	= 'success'; 
	    		$update_transaction->update();

	    		$amount 	= $transaction->amount - $transaction->fee;
	    		$user_id 	= $transaction->user_id;

	    		// update user account
	    		$account = new Account();
	    		$account->creditAccount($user_id, $amount);
	    	}
    	}else{
            $transaction = Transaction::where('trans_ref', $trans_ref)->first();
            if($transaction !== null){
                // find and update transaction
                $update_transaction                 = Transaction::find($transaction->id);
                $update_transaction->trans_status   = 'failed'; 
                $update_transaction->update();

                $amount     = $transaction->amount - $transaction->fee;
                $user_id    = $transaction->user_id;
            }
        }
    }

    /*
    |-----------------------------------------
    | LOAD USER TRANSACTIONS
    |-----------------------------------------
    */
    public function loadUserTransaction(){
        // body
        $user_id = Auth::user()->id;
        $transactions = Transaction::where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        if(count($transactions) > 0){
            $trans_box = [];
            foreach ($transactions as $el) {
                $data = [
                    'id'            => $el->id,
                    'user_id'       => $el->user_id,
                    'trans_ref'     => $el->trans_ref,
                    'trans_note'    => $el->trans_note,
                    'trans_type'    => ucfirst($el->trans_type),
                    'trans_status'  => $el->trans_status,
                    'trans_desc'    => $el->trans_desc,
                    'trans_from'    => $el->trans_from,
                    'trans_to'      => $el->trans_to,
                    'amount'        => number_format($el->amount, 2),
                    'fee'           => number_format($el->fee, 2),
                    'balance'       => number_format($el->balance, 2),
                    'isDeleted'     => $el->isDeleted,
                    'date'          => $el->updated_at->diffForHumans()
                ];

                array_push($trans_box, $data);
            }
        }else{
            $trans_box = [];
        }

        // return
        return $trans_box;
    }

    /*
    |-----------------------------------------
    | LOAD USER TRANSACTIONS
    |-----------------------------------------
    */
    public function loadSingleTransaction($transid){
        // body
        $user_id = Auth::user()->id;
        $transactions = Transaction::where([['id', $transid], ['user_id', $user_id]])->first();
        if($transactions !== null) {

            // check if transaction type 
            // check if bank transaction
            if($transactions->trans_type == "Bank Transfer"){
                $bank_info = new BankTransferLog();
                $bank_data = $bank_info->getBankTransferRecipent($transactions->trans_to);
            }else{
                $bank_data = [
                    'status'            => 'info',
                    'account_bank'      => '',
                    'account_number'    => '',
                    'account_name'      => ''
                ];
            }

            $data = [
                'id'            => $transactions->id,
                'user_id'       => $transactions->user_id,
                'trans_ref'     => $transactions->trans_ref,
                'trans_note'    => $transactions->trans_note,
                'trans_type'    => ucfirst($transactions->trans_type),
                'trans_status'  => $transactions->trans_status,
                'trans_desc'    => $transactions->trans_desc,
                'trans_from'    => $transactions->trans_from,
                'trans_to'      => $transactions->trans_to,
                'amount'        => number_format($transactions->amount, 2),
                'fee'           => number_format($transactions->fee, 2),
                'balance'       => number_format($transactions->balance, 2),
                'isDeleted'     => $transactions->isDeleted,
                'date'          => $transactions->created_at->diffForHumans(),
                'details'       => $bank_data
            ];
        }else{
            $data = [];
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | GET TOTAL DEPOSITED TRANSACTION
    |-----------------------------------------
    */
    public function getTotalCountDeposit($user_id){
        $transaction = Transaction::where([
            ["user_id", $user_id],
            ["trans_status", "success"],
            ["trans_type", "Deposit"]
        ])->get();

        $data = ["total_deposit" => count($transaction)];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET TOTAL WITHDRAW TRANSACTION
    |-----------------------------------------
    */
    public function getTotalCountWithdraw($user_id){
        $transaction = Transaction::where([
            ["user_id", $user_id],
            ["trans_status", "success"],
            ["trans_type", "Withdraw"]
        ])->get();

        $data = ["total_withdraw" => count($transaction)];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET TOTAL SUCCESSFUL TRANSACTION
    |-----------------------------------------
    */
    public function getTotalSuccessTrans($user_id){
        $transaction = Transaction::where([
            ["user_id", $user_id],
            ["trans_status", "success"]
        ])->get();

        $data = ["total_successful" => count($transaction)];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET TOTAL FAILED TRANSACTION
    |-----------------------------------------
    */
    public function getTotalFailedTrans($user_id){
        $transaction = Transaction::where([
            ["user_id", $user_id],
            ["trans_status", "failed"]
        ])->get();

        $data = ["total_failed" => count($transaction)];

        // return 
        return $data;
    }

    /*
    |-----------------------------------------
    | GET TOTAL PENDING TRANSACTION
    |-----------------------------------------
    */
    public function getTotalPendingTrans($user_id){
        $transaction = Transaction::where([
            ["user_id", $user_id],
            ["trans_status", "pending"]
        ])->get();

        $data = ["total_pending" => count($transaction)];

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | GET GRAPH TRANSACTIONS DATA
    |---------------------------------------------
    */
    public function getTransactionData($user_id){
        $transactions = Transaction::where("user_id", $user_id)->get();
        if(count($transactions)){
            $trans_box = [];
            foreach ($transactions as $el) {
                $data  = [
                    "date"   => $el->created_at->getTimeStamp(),
                    "amount" => $el->amount
                ];
                array_push($trans_box, $data);
            }   
        }else{
            $trans_box  = [];
        }

        // return
        return $trans_box;
    }

    /*
    |---------------------------------------------
    | GET TRANSACTION STATS
    |---------------------------------------------
    */
    public function getTransactionStats($user_id){
        $successful = Transaction::where([['user_id', $user_id], ['trans_status', 'success']])->count();
        $pending    = Transaction::where([['user_id', $user_id], ['trans_status', 'pending']])->count();
        $failed     = Transaction::where([['user_id', $user_id], ['trans_status', 'failed']])->count();

        $data = [
            'success' => number_format($successful),
            'pending' => number_format($pending),
            'failed'  => number_format($failed)
        ];

        // return data
        return $data;
    }

    /*
    |---------------------------------------------
    | GET TRANSACTION STATS FROM ADMIN
    |---------------------------------------------
    */
    public function getTransactionStatsAdmin(){
        $successful = Transaction::where('trans_status', 'success')->count();
        $pending    = Transaction::where('trans_status', 'pending')->count();
        $failed     = Transaction::where('trans_status', 'failed')->count();


        $total_deposit      = Transaction::where('trans_type', 'deposit')->count();
        $total_withdraw     = Transaction::where('trans_type', 'withdraw')->count();
        $wallet_transfer    = Transaction::where('trans_type', 'Wallet Transfer')->count();
        $bank_transfer      = Transaction::where('trans_type', 'Bank Transfer')->count();


        $data = [
            'success' => number_format($successful),
            'pending' => number_format($pending),
            'failed'  => number_format($failed),
            'deposit' => number_format($total_deposit),
            'transfer'=> number_format($wallet_transfer + $bank_transfer),
            'withdraw'=> number_format($total_withdraw),
        ];

        // return data
        return $data;
    }

    /*
    |---------------------------------------------
    | SEND MAIL TO SENDER
    |---------------------------------------------
    */
    public function sendMailToSender($payload){
        // send mail to receiver
        // $mail_data      = collect($payload);
        $template_type  = "sender";
        \Mail::to($payload["sender_mail"])->send(new TransferConfirmationMail($template_type, $payload));
    }

    /*
    |---------------------------------------------
    | SEND MAIL TO SENDER
    |---------------------------------------------
    */
    public function sendMailToReceiver($payload){
        // send mail to receiver
        // $mail_data      = collect($payload);
        $template_type  = "receiver";
        \Mail::to($payload["receiver_mail"])->send(new TransferConfirmationMail($template_type, $payload));
    }
}
