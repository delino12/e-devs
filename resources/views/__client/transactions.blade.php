@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Transactions
@endsection

{{-- contents --}}
@section('contents')
  <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
  <!-- Styles -->
  <style>
    #chartdiv {
      width : 100%;
      height  : 500px;
    }                  
  </style>
  <div class="content-wrapper">
    <div class="row mb-4">
      <div class="col-12 d-flex align-items-center justify-content-between">
        <h4 class="page-title">Transactions</h4>
        <div class="d-flex align-items-center">
          <div class="wrapper mr-4 d-none d-sm-block">
            <p class="mb-0">
              <b class="mb-0">{{ date("D M' Y") }}</b>
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 card-statistics">
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3 grid-margin stretch-card card-tile">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between pb-2">
                  <h5>Total Deposit</h5>
                  <i class="icon-wallet"></i>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="text-muted">count</p>
                  <p class="text-muted total-deposit">
                    <img src="svg/loading.svg" height="10px" width="10px" />
                  </p>
                </div>
                <div class="progress progress-md">
                  <div class="progress-bar bg-info w-25" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3 grid-margin stretch-card card-tile">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between pb-2">
                  <h5>Total Withdraw</h5>
                  <i class="icon-wallet"></i>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="text-muted">count</p>
                  <p class="text-muted total-withdraw">
                    <img src="svg/loading.svg" height="10px" width="10px" />
                  </p>
                </div>
                <div class="progress progress-md">
                  <div class="progress-bar bg-success w-25" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3 grid-margin stretch-card card-tile">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between pb-2">
                  <h5>Failed Transaction</h5>
                  <i class="icon-wallet"></i>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="text-muted">count</p>
                  <p class="text-muted total-failed">
                    <img src="svg/loading.svg" height="10px" width="10px" />
                  </p>
                </div>
                <div class="progress progress-md">
                  <div class="progress-bar bg-danger w-25" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-3 grid-margin stretch-card card-tile">
            <div class="card">
              <div class="card-body">
                <div class="d-flex justify-content-between pb-2">
                  <h5>Successful Transaction</h5>
                  <i class="icon-wallet"></i>
                </div>
                <div class="d-flex justify-content-between">
                  <p class="text-muted">count</p>
                  <p class="text-muted total-success">
                    <img src="svg/loading.svg" height="10px" width="10px" />
                  </p>
                </div>
                <div class="progress progress-md">
                  <div class="progress-bar bg-warning w-25" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 grid-margin" id="show-graph-div" style="display: none;">
        <div id="chartdiv"></div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 grid-margin">
        <div class="card support-pane-card">
          <div class="card-header">
            Transactions 
            <button id="show-graph" class="btn btn-default pull-right">
              <i class="fa fa-area-chart"></i> Show statistics
            </button>
          </div>
          <div class="card-body">
            <table class="table trans_table" style="font-size: 12px;">
              <thead>
                  <tr>
                      <th>S/N</th>
                      <th>Status</th>
                      <th>Transaction</th>
                      <th>Amount</th>
                      <th>Ref ID</th>
                      <th>Date</th>
                      <th>More</th>
                  </tr>
              </thead>
              <tbody class="load-user-transaction">
                  <tr>
                      <td>Loading...</td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->  
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <!-- Resources -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

    <!-- Chart code -->
    <script type="text/javascript">
      // init chart data
      loadChartData();

      // load chart data
      function loadChartData() {
        $.get('{{url('get/transaction/graph/data')}}', function(data) {
          var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "marginRight": 80,
            "dataProvider": data,
            "valueAxes": [{
              "position": "left",
              "title": "Transactions amount"
            }],
            "graphs": [{
              "id": "g1",
              "fillAlphas": 0.4,
              "valueField": "amount",
              "balloonText": "<div style='margin:5px; font-size:19px;'>Transactions:<b>[[value]]</b></div>"
            }],
            "chartCursor": {
              "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
              "cursorPosition": "mouse",
              "selectWithoutZooming": true
            },
            "categoryField": "date",
            "categoryAxis": {
              "minPeriod": "mm",
              "parseDates": true
            }
          });

          chart.addListener("rendered", zoomChart);
          zoomChart();
          function zoomChart() {
              chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
          }
        });
      }

      // show graphs
      $("#show-graph").click(function (e){
        e.preventDefault();
        $("#show-graph-div").toggle();
      });
    </script>
@endsection