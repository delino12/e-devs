@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Deposit
@endsection

{{-- contents --}}
@section('contents')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Quick Deposit</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h6>Total Transaction</h6>
              <div class="w-75 mx-auto mt-4">
                <div class="d-flex justify-content-between text-center mb-2">
                  <div class="wrapper text-success">
                    <h4 class="sd"></h4>
                    <small class="text-muted">Successful Deposit</small>
                  </div>
                  <div class="wrapper text-danger">
                    <h4 class="pd"></h4>
                    <small class="text-muted">Pending Deposit</small>
                  </div>
                </div>
              </div>
              {{-- <div id="morris-line-example"></div> --}}
            </div>
          </div>
        </div>
        <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="account_number">Deposit amount</label>
                        <input type="number" class="form-control" step="any" min="1" id="deposit_amount" placeholder="Eg. 1000">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <button class="btn btn-primary" onclick="showDepositModal()">Deposit</button>
                    </div>
                </div>
                <br />
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex">
                      <div class="inner flex-grow">
                        <p>
                          <span class="text-danger">Note: </span>
                          <span class="text-warning">Deposit allowed is between <strong>&#8358;1.00</strong> - <strong>&#8358;500,000.00</strong></span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="card support-pane-card">
            <div class="card-body">
              <table class="table small trans_table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Status</th>
                        <th>Transaction</th>
                        <th>Amount</th>
                        <th>Ref ID</th>
                        <th>Date</th>
                        <th>More</th>
                    </tr>
                </thead>
                <tbody class="load-user-transaction">
                    <tr>
                        <td>Loading...</td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->

    <!-- include modal component -->
    @include('__components/modals')
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script type="text/javascript">

    // init transactions
    $("#init-transaction").click(function(){
      processDeposit();
    });

    // show deposit modal
    function showDepositModal() {
      const depositAmount = $("#deposit_amount").val();
      if(depositAmount == ""){
        swal(
          "oops",
          "Enter deposit amount!",
          "error"
        );
      }else{
        // execeeded limit
        if(depositAmount < 1){
          swal(
            "oops",
            "Minimum allowed deposit is NGN1.00",
            "error"
          );
        }else{
          if(depositAmount > 500000){
            swal(
              "oops",
              "Amount must not exceeds NGN500,000.00",
              "error"
            );
          }else{
            $("#title-deposit-amount").html(`
              &#8358; ${numeral(depositAmount).format('0,0.00')}
            `);
            $("#deposit-modal").modal('show');
          }
        }
      }
    }

    // start deposit
    function processDeposit(){
      $("#loading-registration").fadeIn();
      $("#submit-btn").hide();

      let token         = $("#token").val();
      let cardNo        = $("#cc_nn").val();
      let cardExpMonth  = $("#cc_exp_mo").val();
      let cardExpYear   = $("#cc_exp_yr").val();;
      let cvv           = $("#cc_cvv").val();
      let amount        = $("#deposit_amount").val();

      // query string
      var params = {
        _token: token,
        cardNo: cardNo,
        cardExpMonth: cardExpMonth,
        cardExpYear: cardExpYear,
        cvv: cvv,
        amount: amount
      };

      // charge client card
      $.post('{{url('charge/client/card')}}', params, function(data, textStatus, xhr) {
        if(data.status == true){
          // $("#charge_message").html(data.data.status);
          $("#charge_refrence").val(data.data.reference);
          $("#charge_verify_type").val(data.data.status);
          $(".deposit-form").fadeOut(100);
          $(".verify-deposit").fadeIn(100);

          if(data.data.status === "pending"){
            $("#charge_message").html("Transaction pending...");
            $("#verification-method").html(`
              <p class="text-center">${data.data.status}</p>
            `);
          }else if(data.data.status === "send_otp"){
            $("#charge_message").html("OTP Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">${data.data.display_text}</label>
              <input type="number" min="0" class="form-control" id="charge_otp" placeholder="Enter OTP" required="">
            `);
          }else if(data.data.status === "send_pin"){
            $("#charge_message").html("Pin Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">Enter Pin</label>
              <input type="password" class="form-control" id="charge_otp" placeholder="Enter pin" required="">
            `);
          }else if(data.data.status === "send_birthday"){
            $("#charge_message").html("Birthday Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">${data.data.display_text}</label>
              <input type="date" class="form-control" id="charge_otp" required="">
            `);
          }else if(data.data.status === "send_phone"){
            $("#charge_message").html("Phone Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">${data.data.display_text}</label>
              <input type="text" class="form-control" id="charge_otp" name="" placeholder="Enter phone number" required="">
            `);
          }else if(data.data.status === "open_url"){
            setTimeout((e) => {
              swal({
                  title: 'Bank authorization redirection',
                  html: 'Redirecting..... <strong>2</strong> seconds.'
              });
              window.location.href = `${data.data.url}`;
            }, 2000);
          }else if(data.data.status === "failed"){
            $("#charge_message").html("Failed");
            $("#verification-method").html(`
              <p>${data.data.status}</p>
              <p>${data.data.message}</p>
            `);
          }

          $("#loading-registration").fadeOut();
          $("#submit-btn").show();
        }else if(data.status === "verified"){
          swal({
              title: data.message,
              html: 'Redirecting..... <strong>3</strong> seconds.'
          });
          $("#loading-registration").fadeOut();
          $("#submit-btn").show();
          // console.log(data);
          window.location.href = "/transactions";
        }else{
          swal("oops!", data.message, "error");
          $("#loading-registration").fadeOut();
          $("#submit-btn").show();
        }
      }).fail(function(){
        $("#loading-registration").fadeOut();
        $("#submit-btn").show();
      });

      // void form 
      return false;
    }

    // verify transaction
    function verifyTransaction(){
      $(".verify-loading").fadeIn();
      var token       = $("#token").val();
      var reference   = $("#charge_refrence").val();
      var verify_type = $("#charge_verify_type").val();
      var oneTimePass = $("#charge_otp").val();

      // query string
      var params = {
        _token: token,
        reference: reference,
        verify_type: verify_type,
        otp: oneTimePass
      };

      // console log value
      // console.log(params);

      // verify transaction
      $.post('{{url('charge/verification')}}', params, function(data, textStatus, xhr) {
        // console.log(data);
        $(".verify-loading").fadeOut();
        if(data.status == true){
          // $("#charge_message").html(data.data.status);
          $("#charge_refrence").val(data.data.reference);
          $("#charge_verify_type").val(data.data.status);
          $(".deposit-form").fadeOut(100);
          $(".verify-deposit").fadeIn(100);

          if(data.data.status === "pending"){
            $("#charge_message").html("Transaction pending...");
            $("#verification-method").html(`
              <p class="text-center">${data.data.status}</p>
            `);
          }else if(data.data.status === "send_otp"){
            $("#charge_message").html("OTP Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">${data.data.display_text}</label>
              <input type="number" min="0" class="form-control" id="charge_otp" placeholder="Enter OTP" required="">
            `);
          }else if(data.data.status === "send_pin"){
            $("#charge_message").html("Pin Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">Enter Pin</label>
              <input type="password" class="form-control" id="charge_otp" placeholder="Enter pin" required="">
            `);
          }else if(data.data.status === "send_birthday"){
            $("#charge_message").html("Birthday Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">${data.data.display_text}</label>
              <input type="date" class="form-control" id="charge_otp" required="">
            `);
          }else if(data.data.status === "send_phone"){
            $("#charge_message").html("Phone Verification...");
            $("#verification-method").html(`
              <label for="charge_otp">${data.data.display_text}</label>
              <input type="text" class="form-control" id="charge_otp" name="" placeholder="Enter phone number" required="">
            `);
          }else if(data.data.status === "open_url"){
            setTimeout((e) => {
              swal({
                  title: 'Bank authorization redirection',
                  html: 'Redirecting..... <strong>2</strong> seconds.'
              });
              window.location.href = `${data.data.url}`;
            }, 2000);
          }else if(data.data.status === "failed"){
            $("#charge_message").html("Failed");
            $("#verification-method").html(`
              <p>${data.data.status}</p>
              <p>${data.data.message}</p>
            `);
          }

          $("#loading-registration").fadeOut();
          $("#submit-btn").show();
        }else if(data.status === "verified"){
          swal({
              title: data.message,
              html: 'Redirecting..... <strong>3</strong> seconds.'
          });
          $("#loading-registration").fadeOut();
          $("#submit-btn").show();
          // console.log(data);
          window.location.href = "/transactions";
        }else{
          swal("oops!", data.message, "error");
          $("#loading-registration").fadeOut();
          $("#submit-btn").show();
        }
        if(data.status === "success"){
          swal({
              title: 'Transaction successful!',
              html: 'Redirecting..... <strong>3</strong> seconds.'
          });
          // console log data
          // console.log(data);
          window.location.href = "/transactions";
        }else if(data.status === "error"){
          swal("oops!", data.message, "error");
          $(".verify-loading").fadeOut();
        }
      });

      // void form
      return false;
    }

    // verify bin
    function verifyCardBin() {
      var cardno = $("#cc_nn").val();
      if(cardno.length == 6){
        // resolve bin
        $.get('{{url('https://api.paystack.co/decision/bin')}}/'+cardno, function(data) {
          /*optional stuff to do after success */
          // console.log(data);
          if(data.status == true){
            if(data.data.brand == "Mastercard"){
              $(".cc_type").html(`<i class="fab fa-cc-mastercard"></i> Mastercard`);
            }

            if(data.data.brand == "Visa"){
              $(".cc_type").html(`<i class="fab fa-cc-visa"></i> Visacard`);
            }
          }
        });
      }
    }

    // get transactions stats
    function getTransactionsStatistics() {
      // body...
      $.get('{{ url('load/transactions/stats') }}', function(data) {
        $(".sd").html(data.success);
        $(".pd").html(data.pending);
      });
    }

    // show pending and successful transactions
    getTransactionsStatistics();
  </script>
@endsection