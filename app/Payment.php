<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use EDEV\Mail\SendDepositNotifyMail;
use EDEV\Account;
use EDEV\Transaction;
use Auth;

class Payment
{

    /*
    |-----------------------------------------
    | CHARGE CLIENT CARD
    |-----------------------------------------
    */
    public function chargeClientCard($payload){
    	// body
    	$txRef = 'E-DEVS-ASC-'.rand(000,999).rand(111,999).rand(222,999);

        if(empty($payload->cardNo)){
            return $data = [
                'status'    => 'error',
                'message'   => 'Empty card number',
            ];
        }

        if(empty($payload->cvv)){
            return $data = [
                'status'    => 'error',
                'message'   => 'Empty card cvv',
            ];
        }

        if(empty($payload->amount)){
            return $data = [
                'status'    => 'error',
                'message'   => 'Enter a valid amount',
            ];
        }

        $card = array(
            "cvv"           => $payload->cvv,
            "number"        => $payload->cardNo,
            "expiry_month"  => $payload->cardExpMonth,
            "expiry_year"   => $payload->cardExpYear
        );

        $custom_fields = array(
            "value"         => "",
            "display_name"  => "",
            "variable_name" => "" 
        );

    	$query = array(
    		"email"         => Auth::user()->email,
            "amount"        => $payload->amount * 100,
            "metadata"      => ["custom_fields" => $custom_fields],
            "card"          => $card,
            "reference"     => $txRef
    	);

        // charge endpoint
        $endpoint   = env("PS_CHARGE_ENDPOINT");
        $headers    = array('Content-Type: application/json', 'Authorization: Bearer '.env("PS_SK_KEY"));

		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $endpoint);
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query)); //Post Fields
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 200);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    $res = curl_exec($ch);

        $data = json_decode($res, true);
        // return response()->json($data);
        
        // verify pending payment 
        $this->logPaymentRequest($data, $payload->amount);

        // verify type of request
        if($data["data"]["status"] === "success"){
            // verify instant payment
            $results = $this->verifyInstantCharge($data);
        }else{
            $results = $data;
        }

        // return 
        return $results;

        // close curl
	    curl_close($ch);
    }

    /*
    |---------------------------------------------
    | VERIFY INSTANT CARD CHARGE
    |---------------------------------------------
    */
    public function verifyInstantCharge($results){
        if($results["data"]["status"] == "success"){
            $trans_ref = $results["data"]["reference"];
            $transaction = Transaction::where("trans_ref", $trans_ref)->first();
            if($transaction !== null){
                // find and update transaction
                $update_transaction                 = Transaction::find($transaction->id);
                $update_transaction->trans_status   = "success"; 
                $update_transaction->update();

                $amount     = $transaction->amount - $transaction->fee;
                $user_id    = $transaction->user_id;

                // update user account
                $account = new Account();
                $account->creditAccount($user_id, $amount);

                $account_info = Account::where("user_id", $user_id)->first();

                // mail data
                $mail_data = [
                    'trans_status'  => $transaction->trans_status,
                    'trans_type'    => $transaction->trans_type,
                    'trans_ref'     => $transaction->trans_ref,
                    'amount'        => number_format($transaction->amount, 2),
                    'balance'       => number_format($account_info->balance, 2),
                    'trans_fee'     => number_format($transaction->trans_fee, 2)
                ];

                // send transaction mail
                \Mail::to(Auth::user()->email)->send(new SendDepositNotifyMail($mail_data));

                $data = [
                    'status'  => 'verified',
                    'message' => 'Transaction successful!'
                ];
            }else{
                $data = [
                    'status'  => 'error',
                    'message' => 'Transaction information not found!'
                ];  
            }
        }else{
            $data = [
                'status'  => 'error',
                'message' => 'Transaction verification failed!'
            ];
        }

        // return data
        return $data;
    }

    /*
    |-----------------------------------------
    | VERIFY CLIENT CARD CHARGE
    |-----------------------------------------
    */
    public function verifyClientCharge($payload){
    	// check for otp
        if($payload->has('otp')){
            // check for reference
            if($payload->has('reference')){
                // all verify endpoint
                $otp_endpoint       = env("PS_VERIFY_OTP");
                $pin_endpoint       = env("PS_VERIFY_PIN");
                $phone_endpoint     = env("PS_VERIFY_PHONE");
                $birthday_endpoint  = env("PS_VERIFY_BIRTHDAY");

                if($payload->has('verify_type')){
                    if($payload->verify_type == "send_pin"){
                        $query = array(
                            "reference" => $payload->reference,
                            "pin" => $payload->otp
                        );
                        $endpoint = $pin_endpoint;
                    }elseif($payload->verify_type == "send_otp"){
                        $query = array(
                            "reference" => $payload->reference,
                            "otp" => $payload->otp
                        );
                        $endpoint = $otp_endpoint;
                    }elseif($payload->verify_type == "send_phone"){
                        $query = array(
                            "reference" => $payload->reference,
                            "phone" => $payload->otp
                        );
                        $endpoint = $phone_endpoint;
                    }elseif($payload->verify_type == "send_birthday"){
                        $query = array(
                            "reference" => $payload->reference,
                            "birthday" => $payload->otp
                        );
                        $endpoint = $pin_endpoint;
                    }

                    // charge headers
                    $headers    = array(
                        'Content-Type: application/json', 
                        'Authorization: Bearer '.env("PS_SK_KEY")
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $endpoint);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query)); //Post Fields
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 200);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    $res = curl_exec($ch);

                    
                    $results = json_decode($res, true);
                    // return response()->json($results);

                    if($results["status"] == true && $results["data"]["status"] == "send_otp"){
                        return $results;
                    }elseif($results["status"] == true && $results["data"]["status"] == "send_phone"){
                        return $results;
                    }elseif($results["status"] == true && $results["data"]["status"] == "send_birthday"){
                        return $results;
                    }elseif($results["status"] == true && $results["data"]["status"] == "send_pin"){
                        return $results;
                    }elseif($results["status"] === false){
                        return $data = [
                            'status'    => 'error',
                            'message'   => $results["data"]["message"]
                        ];
                    }elseif($results["data"]["status"] == "success"){
                        $trans_ref = $results["data"]["reference"];
                        $transaction = Transaction::where("trans_ref", $trans_ref)->first();
                        if($transaction !== null){
                            // find and update transaction
                            $update_transaction                 = Transaction::find($transaction->id);
                            $update_transaction->trans_status   = "success"; 
                            $update_transaction->update();

                            $amount     = $transaction->amount - $transaction->fee;
                            $user_id    = $transaction->user_id;

                            // update user account
                            $account = new Account();
                            $account->creditAccount($user_id, $amount);

                            $account_info = Account::where("user_id", $user_id)->first();

                            // mail data
                            $mail_data = [
                                'trans_status'  => $transaction->trans_status,
                                'trans_type'    => $transaction->trans_type,
                                'trans_ref'     => $transaction->trans_ref,
                                'amount'        => number_format($transaction->amount, 2),
                                'balance'       => number_format($account_info->balance, 2),
                                'trans_fee'     => number_format($transaction->trans_fee, 2)
                            ];

                            // send transaction mail
                            \Mail::to(Auth::user()->email)->send(new SendDepositNotifyMail($mail_data));

                            $data = [
                                'status'  => 'success',
                                'message' => 'Transaction successful!'
                            ];
                        }else{
                            $data = [
                                'status'  => 'error',
                                'message' => 'Transaction information not found!'
                            ];  
                        }
                    }elseif($results["data"]["status"] == "failed"){
                        return $data = [
                            'status'  => 'error',
                            'message' => 'Transaction verification failed!'
                        ];
                    }else{
                        return $data = [
                            'status'    => 'error',
                            'message'   => 'Transaction verification was not successful!'
                        ];
                    }
                }else{
                    $data = [
                        'status'  => 'error',
                        'message' => 'Error, We could not complete transaction verification!'
                    ];
                }
            }else{
                $data = [
                    'status'  => 'error',
                    'message' => 'Provide authentication OTP'
                ];
            }
        }else{
            $data = [
                'status'  => 'error',
                'message' => 'Provide a valid OTP'
            ];
        }

        // return data
        return $data;

        curl_close($ch);
    }

    /*
    |-----------------------------------------
    | LOG USER PAYMENT REQUEST
    |-----------------------------------------
    */
    public function logPaymentRequest($results, $amount){
        // body
        $transaction = new Transaction();
        $transaction->saveTransaction($results, $amount);
    }

    /*
    |---------------------------------------------
    | CREATE TRANSFER RECIEPTS
    |---------------------------------------------
    */
    public function generateTransferReciept($payload){
        $account_number = $payload->account;
        $account_code   = $payload->bank_code;
        $secret_key     = env("FLW_LIVE_SEC_KEY");

        if(empty($account_number)){
            return $data = [
                'status'    => 'error',
                'message'   => 'Enter account number'
            ];
        }

        if(strlen($account_number) !== 10){
            return $data = [
                'status'    => 'error',
                'message'   => 'Enter a valid account number'
            ];
        }

        $query = array(
            'account_number'    => $account_number,
            'account_bank'      => $account_code,
            'seckey'            => $secret_key
        );

        $data_string = json_encode($query);
        $ch = curl_init('https://api.ravepay.co/flwv3-pug/getpaidx/api/validatecharge');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                              
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

        $response = curl_exec($ch);
        $results = json_decode($response, true);

        return $results;
    }

    /*
    |-----------------------------------------
    | GET ACCEPTED BANK LIST
    |-----------------------------------------
    */
    public function getAcceptedBank(){
        // body
        // $client = new Client();
    }

    /*
    |---------------------------------------------
    | GET CUSTOMER INFO
    |---------------------------------------------
    */
    public function getCustomerCableInfo($payload){
        $product_id = $payload->product_id;
        $smart_no   = $payload->smart_no;

        $product    = Product::where("id", $product_id)->first();
        $bill       = strtolower($product->name);

        $userid = env("MOBILE_API_USERID");
        $passwd = env("MOBILE_API_PASSWD");
        $uri    = 'https://mobileairtimeng.com/httpapi/customercheck';

        $endpoint = $uri.'?userid='.$userid.'&pass='.$passwd.'&bill='.$bill.'&smartno='.$smart_no;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $res = curl_exec($ch);

        $results = explode("|", $res);

        if($results[0] == "100"){
            $data = [
                'status'    => 'success',
                'message'   => 'Customer information found!',
                'data'      => [
                    'customer_name'     => $results[1],
                    'decoder_status'    => $results[2],
                    'subscription_exp'  => $results[3],
                    'invoice_period'    => $results[4],
                ]
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not retrieve customer information',
                'data'      => [
                    'customer_name'     => 'Not found!',
                    'decoder_status'    => 'Not found!',
                    'subscription_exp'  => 'Not found!',
                    'invoice_period'    => 'Not found!',
                ]
            ];
        }

        // return results
        return $data;
    }
}
