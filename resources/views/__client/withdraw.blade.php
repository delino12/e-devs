@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Withdraw
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Quick Withdraw</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6>Total Transaction</h6>
            </div>
            <div class="card-body">
              <div class="w-75 mx-auto mt-4">
                <div class="d-flex justify-content-between text-center mb-2">
                  <div class="wrapper text-success">
                    <h4>0</h4>
                    <small class="text-muted">Successful Withdraw</small>
                  </div>
                  <div class="wrapper text-danger">
                    <h4>0</h4>
                    <small class="text-muted">Pending Withdraw</small>
                  </div>
                </div>
              </div>
              {{-- <div id="morris-line-example"></div> --}}
            </div>
          </div>
        </div>
        <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6>
                Instant Withdraw
                <a href="{{ url('setting') }}" class="pull-right">
                  <i class="fa fa-plus"></i> Add Bank Account
                </a>
              </h6>
            </div>
            <div class="card-body">
              <div class="row">
                  <div class="form-group col-md-4">
                      <label for="account_number">Withdraw amount</label>
                      <input type="number" class="form-control" onkeyup="formatAmount()" step="any" min="1" id="withdraw_amount" placeholder="Eg. 1000">
                      <input type="hidden" id="withdraw_charge" value="{{ env("WITHDRAW_CHARGE") }}" name="">
                  </div>
                  <div class="form-group col-md-4">
                    <div class="format_amount"></div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-4">
                      <button class="btn btn-primary" onclick="showWithdrawModal()">Withdraw</button>
                  </div>
              </div>
              <br />
              <div class="card">
                <div class="card-body">
                  <div class="d-flex">
                    <div class="inner flex-grow">
                      <p>
                        <span class="text-danger">Note: </span>
                        <span class="text-success small">
                          Withdrawal fee of N52.00 is charged for every withdraw
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card support-pane-card">
            <div class="card-header">
              Transactions
            </div>
            <div class="card-body">
              <table class="table trans_table" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Status</th>
                        <th>Transaction</th>
                        <th>Amount</th>
                        <th>Ref ID</th>
                        <th>Date</th>
                        <th>More</th>
                    </tr>
                </thead>
                <tbody class="load-user-transaction">
                    <tr>
                        <td>Loading...</td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script type="text/javascript">
    function showWithdrawModal() {
      swal(
        "oops",
        "Withdrawal is not available at the moment!",
        "error"
      );
    }

    function formatAmount() {
      var withdraw_amount = $("#withdraw_amount").val();
      var withdraw_charge = $("#withdraw_charge").val();
      var f_amount =  (parseFloat(withdraw_charge) + parseFloat(withdraw_amount));

      $(".format_amount").html(`
        <label for="account_number">Total</label>
        <div class="form-control">
          ${numeral(f_amount).format('0,0.00')}
        </div>
      `);
    }
  </script>
@endsection