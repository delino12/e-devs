<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Transaction;
use Auth;

class GraphDataController extends Controller
{
    /*
    |---------------------------------------------
    | FETCH TRANSACTION GRAPHS DATA
    |---------------------------------------------
    */
    public function transactionGraphData(){
    	$user_id 		= Auth::user()->id;
    	$transaction 	= new Transaction();
    	$data 			= $transaction->getTransactionData($user_id);

    	// return response
    	return response()->json($data);
    }
}
