@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Airtime & Databundle Topup
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Airtime & Databundle</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
                <h5>Airtime/Data Bundle Recharge </h5>
            </div>
            <div class="card-body">
                <form method="post" onsubmit="return topUpMobile();">
                  <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="receivers_mobile"><i class="fa fa-phone"></i> Mobile number</label>
                        <input type="text" class="form-control" maxlength="11" id="receivers_mobile" placeholder="Eg. 080********" required="">
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="receivers_amount"><i class="fa fa-money"></i> Amount</label>
                        <input type="number" min="1" step="any" class="form-control" id="receivers_amount" placeholder="Eg. 100" required="">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="recharge_type"> Recharge Type</label>
                      <select class="form-control" id="recharge_type">
                        <option value=""> -- none -- </option>
                        <option value="airtime"> Airtime </option>
                        <option value="data"> Data </option>
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="recharge_type"> Select Network</label>
                      <select class="form-control" id="recharge_network">
                        <option value=""> -- none -- </option>
                        <option value="1"> Airtel </option>
                        <option value="15"> MTN </option>
                        <option value="2"> 9Mobile </option>
                        <option value="6"> Glo </option>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <label for="airtime_sender_notes">Type a note (Optional)</label>
                      <textarea class="form-control" cols="12" rows="2" id="airtime_sender_notes" placeholder="Type a note..."></textarea>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group col-sm-12">
                        <button class="btn btn-primary"><i class="fa fa-location-arrow"></i> Send 
                          <img src="svg/loading.svg" class="airtime-data-loading" style="display: none;">
                        </button>
                    </div>
                  </div>
                </form>
            </div>
          </div>
        </div>

        <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
                <h5>Bulk Airtime/Data Bundle Recharge</h5>
            </div>
            <div class="card-body">
                <h4>Send Airtime/Data in bulk is currently not available at the moment</h4>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card support-pane-card">
            <div class="card-header">
              Transactions
            </div>
            <div class="card-body">
              <table class="table trans_table" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Status</th>
                        <th>Transaction</th>
                        <th>Amount</th>
                        <th>Ref ID</th>
                        <th>Date</th>
                        <th>More</th>
                    </tr>
                </thead>
                <tbody class="load-user-transaction">
                    <tr>
                        <td>Loading...</td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script type="text/javascript">
   
  </script>
@endsection