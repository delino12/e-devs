@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Business
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Own Business</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6 class="card-title">
                Business Information
                <a href="javascript:void(0);" onclick="addBusinessModal()" class="pull-right">Add Business</a>
              </h6>
            </div>
            <div class="card-body">
              <div class="business_information"></div>
            </div>
          </div>
        </div>

        <div class="col-md-6 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6 class="card-title">
                Business Services
                <a href="javascript:void(0);" onclick="addServicesModal()" class="pull-right">Update Services</a>
              </h6>
            </div>
            <div class="card-body">
              <div class="business_services">
                Loading connected services...
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->

    <!-- include modal component -->
    @include('__components/modals')
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}} 
  <script type="text/javascript">  
    loadBusinessInfo();

    // add business modal
    function addBusinessModal() {
      $("#add-business-modal").modal('show');
    }

    // add business modal
    function addServicesModal() {
      $("#add-business-modal").modal('show');
    }


    // create new business
    function createNewBusiness() {
      var token         = $("#token").val();
      var business_name = $("#business_name").val();
      var total_agent   = $("#total_agent").val();
      var prod_1        = $("#prod_1").val();
      var prod_2        = $("#prod_2").val();
      var prod_3        = $("#prod_3").val();
      var prod_4        = $("#prod_4").val();
      var prod_5        = $("#prod_5").val();
      var prod_6        = $("#prod_6").val();

      // check bills and utility
      if($("#prod_1").is(":checked")){
        prod_1 = "Dstv";
      }

      if($("#prod_2").is(":checked")){
        prod_2 = "Gotv";
      }
      if($("#prod_3").is(":checked")){
        prod_3 = "eBank";
      }
      if($("#prod_4").is(":checked")){
        prod_4 = "Airtime";
      }
      if($("#prod_5").is(":checked")){
        prod_5 = "Sunking";
      }
      if($("#prod_6").is(":checked")){
        prod_6 = "Electricity";
      }

      // verify params
      var params = {
        _token: token,
        business_name: business_name,
        total_agent: total_agent,
        prod_1: prod_1,
        prod_2: prod_2,
        prod_3: prod_3,
        prod_4: prod_4,
        prod_5: prod_5,
        prod_6: prod_6
      };

      // console log value
      // console.log(params);

      // post data set
      $.post('{{url('create/new/business')}}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          swal(
            data.status,
            data.message,
            data.status
          );
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });

      // void form
      return false;
    }

    // load business information
    function loadBusinessInfo() {
      $.get('{{ url('load/business/information') }}', function(data) {
        // console log value
        // console.log(data);
        if(data !== ""){
          $(".business_information").html(`
            <table class="table card card-body">
              <tr>
                <td>Business Name</td>
                <td>${data.business.name}</td>
              </tr>

              <tr>
                <td>No. of Agent</td>
                <td>${data.business.total_agent}</td>
              </tr>

              <tr>
                <td>Status</td>
                <td>${data.business.status}</td>
              </tr>

              <tr>
                <td>Date created</td>
                <td>${data.business.created_at}</td>
              </tr>
            </table>

            <br /><br />
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agent ID</th>
                  <th>Balance(&#8358)</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody class="all-agents">
                <tr>
                  <td>Checking...</td>
                </tr>
              </tbody>
            </table>
            <div class="view-more pt-1"></div>
          `);

          $(".business_services").html("");

          var sn = 0;
          $.each(data.services, function(index, val) {
            sn++;

            if(val !== "on"){
              $(".business_services").append(`
                <table class="table">
                  <tr>
                    <td>${val} Subscriptions</td>
                    <td>
                      <label class="switch">
                        <input type="checkbox">
                        <span class="slider round"></span>
                      </label>
                    </td>
                  </tr>
                </table>
              `);
            }
          });

        }else{
          $(".business_information").html(`
            <h3>You do not have any business setup yet</h3>
          `);
        }

        $(".all-agents").html("");
        var sn = 0;
        $.each(data.agents, function(index, val) {
          /* iterate through array or object */
          sn++;
          if(val.name == null){
            val.name = "none";
          }

          if(val.balance == null){
            val.balance = "0.00";
          }

          $(".all-agents").append(`
            <tr>
              <td>${val.name}</td>
              <td>${val.agent_code}</td>
              <td>&#8358; ${val.balance}</td>
              <td><a href="javascript:void(0);">Edit</a></td>
            </tr>
          `);
          if(sn > 1){
            return false;
          }
        });

        $(".view-more").append(`
          <center>
            <a href="javascript:void(0);">View More</a>
          </center>
        `);
      });
    }
  </script>
@endsection