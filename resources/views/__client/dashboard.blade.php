@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Dashboard
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Dashboard</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">Summary for
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
                <h5>Airtime/Data Bundle Recharge </h5>
            </div>
            <div class="card-body">
                <form method="post" onsubmit="return topUpMobile();">
                  <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="receivers_mobile"><i class="fa fa-phone"></i> Mobile number</label>
                        <input type="tel" class="form-control" maxlength="11" id="receivers_mobile" placeholder="Eg. 080********" required="">
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="receivers_amount"><i class="fa fa-money"></i> Amount</label>
                        <input type="number" min="1" step="any" class="form-control" id="receivers_amount" placeholder="Eg. 100" required="">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="recharge_type"> Recharge Type</label>
                      <select class="form-control" id="recharge_type">
                        <option value=""> -- none -- </option>
                        <option value="airtime"> Airtime </option>
                        <option value="data"> Data </option>
                      </select>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="recharge_type"> Select Network</label>
                      <select class="form-control" id="recharge_network">
                        <option value=""> -- none -- </option>
                        <option value="1"> Airtel </option>
                        <option value="15"> MTN </option>
                        <option value="2"> 9Mobile </option>
                        <option value="6"> Glo </option>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <label for="airtime_sender_notes">Type a note (Optional)</label>
                      <textarea class="form-control" cols="12" rows="2" id="airtime_sender_notes" placeholder="Type a note..."></textarea>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="form-group col-sm-12">
                        <button class="btn btn-primary"><i class="fa fa-location-arrow"></i> Send 
                          <img src="svg/loading.svg" class="airtime-data-loading" style="display: none;">
                        </button>
                    </div>
                  </div>
                </form>
            </div>
          </div>
        </div>

        <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
                <h5>Quick Transfer</h5>
            </div>
            <div class="card-body">
              <form method="post" onsubmit="return processB2BTransfer()">
                <input type="hidden" id="transfer-reciept-ref" name="">
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="account_number">Recipient's Account No.</label>
                          <input type="text" class="form-control" maxlength="10" id="receivers_account" placeholder="Eg. 010000000" required="">
                      </div>
                      <div class="form-group col-sm-6">
                          <label for="bank_code">Bank Name</label>
                          <select onchange="resolveBankInfo()" id="bank_code" class="form-control"> 
                              <option value=""> -- select bank --</option>
                          </select>
                      </div>
                  </div>
                  <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="receivers_amount">Amount</label>
                          <input type="number" class="form-control" onblur="createTransferReciept()" step="any" min="1" id="transfer_amount" placeholder="Eg. 1000" required="">
                      </div>
                      <div class="form-group col-sm-6" id="resolve-account">
                        <img src="{{asset('svg/loading.svg')}}" id="loading-account-info" style="display: none;">
                      </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-12">
                      <label for="senders_note">Type a note (Optional)</label>
                      <textarea class="form-control" cols="12" rows="2" id="senders_note" placeholder="Type a note..."></textarea>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6">
                          <button class="btn btn-primary"><i class="fa fa-location-arrow"></i>    Transfer
                              <img src="svg/loading.svg" class="transfer-loading" style="display: none;">
                          </button>
                      </div>
                      <div class="col-sm-6">
                        <span class="pull-right small">
                          <span style="color:#a30a14;">Note:</span> <br />
                          Transfer charges &#8358;{{env("TRANSFER_CHARGE")}} Convenience fees. <br />
                          Transfer maximum limit is <strong>&#8358;500,000.00</strong> <br />
                        </span>
                      </div>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              Transactions
            </div>
            <div class="card-body">
              <table class="table trans_table" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Status</th>
                        <th>Transaction</th>
                        <th>Amount</th>
                        <th>Ref ID</th>
                        <th>Last Updated</th>
                        <th>More</th>
                    </tr>
                </thead>
                <tbody class="load-user-transaction">
                    <tr>
                        <td>Loading...</td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script type="text/javascript">
    // process bank to bank
    function processB2BTransfer() {
      $(".transfer-loading").fadeIn();
      var token       = $("#token").val();
      var names       = $("#recievers_account_name").val();
      var account     = $("#receivers_account").val();
      var amount      = $("#transfer_amount").val();
      var bank_code   = $("#bank_code").val();
      var note        = $("#senders_note").val();
      var reciept_ref = $("#transfer-reciept-ref").val();

      // console log value
      var params = {
        _token: token,
        account: account,
        amount: amount,
        names: names,
        bcode: bank_code,
        reciept_ref: reciept_ref,
        note: note
      };

      // create transfer reciept
      $.post('{{url("complete/transfer/b2b")}}', params, function(data, textStatus, xhr) {
        $(".transfer-loading").fadeIn();
        if(data.status == true){
          $(".transfer-loading").fadeOut();
          swal("Ok", data.message, "success");
        }else{
          $(".transfer-loading").fadeOut();
          swal("oops!", data.message, "error");
        }
      }).fail(function (e){
        $(".transfer-loading").fadeOut();
        swal("oops!", "failed to process request!", "error");
      });

      // void form
      return false;
    }

    // create transfer reciepts
    function createTransferReciept() {
      $(".transfer-loading").fadeIn();
      var token       = $("#token").val();
      var names       = $("#recievers_account_name").val();
      var account     = $("#receivers_account").val();
      var bank_code   = $("#bank_code").val();
      var note        = $("#senders_note").val();

      if(bank_code == ""){
        swal(
          "oops",
          "Select recipient bank",
          "error"
        );
        $("#loading-acct_no").fadeOut();
        return false;
      }

      var params = {
        _token: token,
        account: account,
        names: names,
        bcode: bank_code,
        note: note
      };

      // create transfer reciept
      $.post('{{url("create/transfer/reciept")}}', params, function(data, textStatus, xhr) {
        if(data.status === true){
          $(".transfer-loading").fadeOut();
          $("#transfer-reciept-ref").val(data.data.recipient_code);
        }else{
          $(".transfer-loading").fadeOut();
          swal("oops!", data.message, "error");
        }
      }).fail(function (e){
        $(".transfer-loading").fadeOut();
        swal("oops!", "failed to process request !", "error");
      });

      // void form
      return false;
    }
  </script>
@endsection