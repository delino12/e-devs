@extends('layouts.auth-skin')

{{-- title section --}}
@section('title')
	E-Devs | Login
@endsection


{{-- contents --}}
@section('contents')
	<section class="site-section bg-light">
    <div class="container">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"> 
          <div class="box card-feel">
            <span class="lead">Login
              <span class="pull-right">
                <a href="{{url('/')}}"><i class="fa fa-arrow-left"></i> Home</a>
              </span>
            </span><hr />
            <form onsubmit="return loginUser()" method="post">
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="name">Email</label>
                  <input type="text" id="email" class="form-control" placeholder="Email" required="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="name">Password</label>
                  <input type="password" id="password" class="form-control" placeholder="Password" required="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-7 form-group">
                  <button class="btn btn-primary" id="login-btn" type="submit">
                    Login
                  </button>
                </div>
                <div class="col-md-5 form-group">
                  <span class="lead small"><a href="javascript:void(0);">Forgot Password?</a></span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <span class="lead small">I don't have account? <a href="{{url('/register')}}">Create new account</a></span>
                </div>
              </div>

              <hr />
              <div class="row">
                <div class="col-md-12 form-group">
                  <span class="lead">Login via</span>
                  &nbsp; &nbsp;
                  <a class="text-primary" href="javascript:void(0);">
                    <i class="fa fa-facebook"></i>acebook
                  </a> 
                  &nbsp;
                  or  
                  &nbsp;
                  <a href="javascript:void(0);">
                    <i class="fa fa-google"></i>oogle
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
  </section>
  <!-- END section -->
@endsection


{{-- scripts --}}
@section('scripts')
	{{-- scripts --}}
  <script type="text/javascript">
    function loginUser() {
      $("#login-btn").html(`
        Processing...
      `);
      var token     = $("#token").val();
      var email     = $("#email").val();
      var password  = $("#password").val();
      
      var params = {
        _token: token,
        email: email,
        password: password
      };

      $.post('{{url('login')}}', params, function(data, textStatus, xhr) {
        if(data.status == 'success'){
          window.location.href = '/dashboard';
        }else{
          swal("oops!", data.message, "error");
          $("#login-btn").html(`
            Login
          `);
        }
      });

      // void form;
      return false;
    }
  </script>
@endsection