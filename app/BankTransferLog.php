<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;

class BankTransferLog extends Model
{
    /*
    |---------------------------------------------
    | ADD NEW PAYLOAD LOGS
    |---------------------------------------------
    */
    public function addLog($user_id, $payload){
    	$this->user_id 		= $user_id;
    	$this->payload 		= $payload;
    	$this->is_deleted 	= false;
    	$this->save();
    }

    /*
    |---------------------------------------------
    | GET TRANSFER RECIPENTS ACCOUNT
    |---------------------------------------------
    */
    public function getBankTransferRecipent($transaction_ref){
        // charge endpoint
        $endpoint   = "https://api.paystack.co/transfer/".$transaction_ref;
        $headers    = array('Content-Type: application/json', 'Authorization: Bearer '.env("PS_SK_KEY"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);

        // return response()->json($res); 
        $data = json_decode($res, true);

        if($data['status'] == true && $data['data']['status'] == "pending"){
            $data = [
                'status'            => $data['data']['status'],
                'account_bank'      => '',
                'account_number'    => '',
                'account_name'      => ''
            ];
        }elseif($data['status'] == true && $data['data']['status'] == "success"){
            $data = [
                'status'            => $data['data']['status'],
                'account_bank'      => $data['data']['recipient']['details']['bank_name'],
                'account_number'    => $data['data']['recipient']['details']['account_number'],
                'account_name'      => $data['data']['recipient']['name'],
            ];
        }else{
            $data = [
                'status'            => 'info',
                'account_bank'      => '',
                'account_number'    => '',
                'account_name'      => ''
            ];
        }

        // return
        return $data;

        curl_close($ch);
    }
}
