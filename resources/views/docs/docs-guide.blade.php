@include('__components.docs-header')
  <div class=" container-scroller">
      <div class="container-fluid page-body-wrapper">
        <div class="main-panel w-100">
          <div class="content-wrapper documentation">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12 doc-header">
                  <a class="text-dark font-weight-medium" href="{{ url('/') }}"><i class="mdi mdi-home text-primary mr-2"></i>Back to home</a>
                  <h1 class="text-primary mt-4">Documentation</h1>
                </div>
              </div>
              <div class="row doc-content">
                <div class="col-12 col-md-3 grid-margin doc-table-contents">
                  <div class="card">
                    <div class="card-body">
                      <h3 class="mb-4">Table of contents</h3>
                      <ul class="list-arrow">
                        <li>
                          <a href="documentation.html#doc-intro">Introduction</a>
                        </li>
                        <li>
                          <a href="documentation.html#doc-started">Getting started</a>
                        </li>
                        <li>
                          <a href="documentation.html#doc-structure">Basic structure</a>
                        </li>
                        <li>
                          <a href="documentation.html#doc-components">Components</a>
                          <ul class="list-arrow">
                            <li>
                              <a href="documentation.html#doc-basic-ui">Basic UI Elements</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-basic-ui-2">Basic UI Elements-2</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-advanced-ui">Advanced UI Elements</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-media">Media</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-tables">Tables</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-charts">Charts</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-maps">Maps</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-forms">Forms</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-additional-forms">Additional form elements</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-icons">Icons</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-file-upload">File upload</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-form-picker">Form picker</a>
                            </li>
                            <li>
                              <a href="documentation.html#doc-editors">Editors</a>
                            </li>
                          </ul>
                        </li>
                        <li>
                          <a href="documentation.html#doc-credits">Credits</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-md-9 offset-md-3 grid-margin">
                  <div class="col-12 grid-margin" id="doc-intro">
                      <div class="card">
                          <div class="card-body">
                              <h3 class="mb-4">Introduction</h3>
                              <p>Chroma Admin is a responsive HTML template that is based on the CSS framework Bootstrap 4 and it is built with Sass. Sass compiler makes it easier to code and customize. If you are unfamiliar with Bootstrap or Sass, visit their
                                  website and read through the documentation. All of Bootstrap components have been modified to fit the style of Chroma Admin and provide a consistent look throughout the template.</p>
                              <p>Before you start working with the template, we suggest you go through the pages that are bundled with the theme. Most of the template example pages contain quick tips on how to create or use a component which can
                                  be really helpful when you need to create something on the fly.</p>
                              <p class="d-inline"><strong>Note</strong>: We are trying our best to document how to use the template. If you think that something is missing from the documentation, please do not hesitate to tell us about it. If you have any questions or issues regarding this theme please use Envato Marketplace contact form on our profile or email us at </p><a class="d-inline ml-2 text-info" href="mailto:support@urbanui.com">support@urbanui.com</a>
                          </div>
                      </div>
                  </div>
                  <div class="col-12 grid-margin" id="doc-started">
                      <div class="card">
                          <div class="card-body">
                              <h3 class="mb-4">Getting started</h3>
                              <p>You can directly use the compiled and ready-to-use the version of the template. But in case you plan to customize the template extensively the template allows you to do so.</p>
                              <p>Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:</p>
                              <textarea class="shell-mode">
                              </textarea>
                              <div class="alert alert-success mt-4 d-flex" role="alert">
                                <i class="mdi mdi-alert-circle"></i>
                                <p>We have bundled up the vendor files needed for demo purpose into a folder 'vendors', you may not need all those vendors in your application.  If you want to make any change in the vendor package files, you need to change the src path for related tasks in the file gulpfile.js and run the task <code>bundleVendors</code> to rebuild the vendor files.</p>
                              </div>
                              <hr class="mt-5">
                              <h4 class="mt-4">Installation</h4>
                              <p class="mb-0">
                                You need to install package files/Dependencies for this project if you want to customize it. To do this, you must have <span class="font-weight-bold">node and npm</span> installed in your computer.
                              </p>
                              <p class="mb-0">Installation guide of the node can be found <a href="https://nodejs.org/en/">here</a>. As npm comes bundled with a node, a separate installation of npm is not needed.</p>
                              <p>
                                  If you have installed them, just go to the root folder and run the following command in your command prompt or terminal (for the mac users).
                              </p>
                              <textarea class="shell-mode"></textarea>
                              <p class="mt-4">
                               This will install the dev dependencies in the local <span class="font-weight-bold">node_modules</span> folder in your root directory.
                              </p>
                              <p class="mt-2">
                               Then you will need to install <span class="font-weight-bold">Gulp</span>. We use the Gulp task manager for the development processes. Gulp will watch for changes to the SCSS files and automatically compile the files to CSS.
                              </p>
                              <p>Getting started with Gulp is pretty simple. The <a href="https://gulpjs.com/" target="_blank">Gulp</a> site is a great place to get information on installing Gulp if you need more information. You need to first install Gulp-cli in your machine using the below command.</p>
                              <textarea class="shell-mode"></textarea>
                              <p class="mt-4">This installs Gulp-cli globally to your machine. The other thing that Gulp requires, which, is really what does all the work, is the gulpfile.js. In this file, you set up all of your tasks that you will run.</p>
                              <p>Don't worry. We have this file already created for you!</p>
                              <p>To run this project in development mode enter the following command below. This will start the file watch by gulp and whenever a file is modified, the SCSS files will be compiled to create the CSS file.</p>
                              <textarea class="shell-mode"></textarea>           
                              <div class="alert alert-warning mt-4" role="alert">
                                <i class="mdi mdi-alert-circle-outline"></i>It is important to run <code>gulp serve</code> command from the directory where the gulpfile.js is located.
                              </div>
                              <hr class="mt-5">
                              <h4 class="mt-4">Layout Configurations</h4>
                              <p>
                                We have provided a bunch of layout options for you with a single class change!
                                You can use the following classes for each layout.
                              </p>
                              <table class="table table-striped table-bordered mt-4">
                                <thead>
                                    <tr>
                                        <th>Layout</th>
                                        <th>Class to be added on the body tag</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Compact menu</td>
                                        <td>sidebar-mini</td>
                                    </tr>
                                    <tr>
                                        <td>Icon menu</td>
                                        <td>sidebar-icon-only</td>
                                    </tr>
                                    <tr>
                                        <td>Sidebar Hidden (togglable)</td>
                                        <td>sidebar-toggle-display, sidebar-hidden</td>
                                    </tr>
                                    <tr>
                                        <td>Sidebar Overlay</td>
                                        <td>sidebar-absolute, sidebar-hidden</td>
                                    </tr>
                                    <tr>
                                      <td>Horizontal menu 1</td>
                                      <td>horizontal-menu (However, horizontal-menu has different HTML structure. Refer pages/layout/horizontal-menu.html)</td>
                                    </tr>
                                    <tr>
                                      <td>Horizontal menu 2</td>
                                      <td>horizontal-menu-2 (However, horizontal-menu-2 has different HTML structure. Refer pages/layout/horizontal-menu-2.html)</td>
                                    </tr>
                                    <tr>
                                        <td>Boxed layout</td>
                                        <td>boxed-layout</td>
                                    </tr>
                                    <tr>
                                        <td>RTL layout</td>
                                        <td>rtl</td>
                                    </tr>
                                </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
                  <div class="col-12 grid-margin" id="doc-structure">
                    <div class="card">
                        <div class="card-body">
                              <h3>Basic structure</h3>
                              <p class="mb-5">
                                The below snippet shows the basic HTML structure of Chroma Admin.
                                Please note that all the stylesheets and script files in the below snippet should be included to render Chroma Admin styles.
                              </p>
                              <textarea class="multiple-codes mt-5">
                                  
                              </textarea>
                        </div>
                    </div>
                  </div>
                  <div class="col-12 grid-margin" id="doc-components">
                    <div class="card">
                              <div class="card-body">
                                <!-- New Docs Starts Here -->
                                <h3 class="my-4">Components</h3>
                                <hr class="hr" id="doc-basic-ui">
                                <h4 class="my-4">Basic UI Elements</h4>
                                <div class="demo-tabs">
                                  <!-- Tabs Starts -->
                                  <div data-pws-tab="Button" data-pws-tab-name="Button">
                                    <h5 class="mb-2 mt-4">Button with the single color</h5>
                                    <div class="fluid-container py-4">
                                      <button type="button" class="btn btn-primary">Primary</button>
                                      <button type="button" class="btn btn-secondary">Secondary</button>
                                      <button type="button" class="btn btn-success">Success</button>
                                      <button type="button" class="btn btn-info">Info</button>
                                      <button type="button" class="btn btn-warning">Warning</button>
                                      <button type="button" class="btn btn-danger">Danger</button>
                                    </div>
                                    <textarea class="multiple-codes">
                                      <div class="row">
                                        <button type="button" class="btn btn-primary">Primary</button>
                                        <button type="button" class="btn btn-secondary">Secondary</button>
                                        <button type="button" class="btn btn-success">Success</button>
                                        <button type="button" class="btn btn-info">Info</button>
                                        <button type="button" class="btn btn-warning">Warning</button>
                                        <button type="button" class="btn btn-danger">Danger</button>
                                      </div>
                                                                        </textarea>
                                                                        <h5 class="mb-2 mt-4">Outlined</h5>
                                                                        <div class="fluid-container py-4">
                                                                          <button type="button" class="btn btn-outline-primary">Primary</button>
                                                                          <button type="button" class="btn btn-outline-secondary">Secondary</button>
                                                                          <button type="button" class="btn btn-outline-success">Success</button>
                                                                          <button type="button" class="btn btn-outline-info">Info</button>
                                                                          <button type="button" class="btn btn-outline-warning">Warning</button>
                                                                          <button type="button" class="btn btn-outline-danger">Danger</button>
                                                                        </div>
                                                                        <textarea class="multiple-codes">
                                      <div class="row">
                                        <button type="button" class="btn btn-outline-primary">Primary</button>
                                        <button type="button" class="btn btn-outline-secondary">Secondary</button>
                                        <button type="button" class="btn btn-outline-success">Success</button>
                                        <button type="button" class="btn btn-outline-info">Info</button>
                                        <button type="button" class="btn btn-outline-warning">Warning</button>
                                        <button type="button" class="btn btn-outline-danger">Danger</button>
                                      </div>
                                    </textarea>
                                    <h5 class="mb-2 mt-4">Sizes</h5>
                                    <div class="fluid-container py-4">
                                      <button type="button" class="btn btn-primary btn-lg">Large</button>
                                      <button type="button" class="btn btn-primary">Medium</button>
                                      <button type="button" class="btn btn-primary btn-sm">Small</button>
                                    </div>
                                    <textarea class="multiple-codes">
                                      <div class="row">
                                        <button type="button" class="btn btn-primary btn-lg">Small</button>
                                        <button type="button" class="btn btn-secondary">Medium</button>
                                        <button type="button" class="btn btn-success btn-sm">Large</button>
                                      </div>
                                    </textarea>
                                  </div>
                                  <!-- Tabs Ends -->
                                  <!-- Tabs Starts -->
                                  <div data-pws-tab="Accordions" data-pws-tab-name="Accordions">
                                    <h5 class="mb-2 mt-4">Bootstrap Accordion</h5>
                                    <div class="fluid-container py-4">
                                      <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                          <div class="card">
                                              <div class="card-header" role="tab" id="headingOne">
                                                  <h5 class="mb-0">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="documentation.html#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                      Collapsible Group Item #1
                                                      </a>
                                                  </h5>
                                              </div>
  
                                              <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                  <div class="card-body">
                                                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                                      non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                                      eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                                      single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh
                                                      helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                                      Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                                      raw denim aesthetic synth nesciunt you probably haven't heard of them
                                                      accusamus labore sustainable VHS.
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="card">
                                              <div class="card-header" role="tab" id="headingTwo">
                                                  <h5 class="mb-0">
                                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="documentation.html#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                      Collapsible Group Item #2
                                                      </a>
                                                  </h5>
                                              </div>
                                              <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                  <div class="card-body">
                                                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                                      non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                                      eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                                      single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh
                                                      helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                                      Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                                      raw denim aesthetic synth nesciunt you probably haven't heard of them
                                                      accusamus labore sustainable VHS.
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="card">
                                              <div class="card-header" role="tab" id="headingThree">
                                                  <h5 class="mb-0">
                                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="documentation.html#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                      Collapsible Group Item #3
                                                      </a>
                                                  </h5>
                                              </div>
                                              <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                  <div class="card-body">
                                                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                                      non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                                      eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                                                      single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh
                                                      helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                                      Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                                      raw denim aesthetic synth nesciunt you probably haven't heard of them
                                                      accusamus labore sustainable VHS.
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <textarea class="multiple-codes">
  <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
    <div class="card">
      <div class="card-header" role="tab" id="headingOne">
        <h5 class="mb-0">
          <a data-toggle="collapse" data-parent="#accordion" href="documentation.html#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Collapsible Group Item #1
          </a>
        </h5>
      </div>
  
      <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
        <div class="card-body">
            ....
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" role="tab" id="headingTwo">
        <h5 class="mb-0">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="documentation.html#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Collapsible Group Item #2
          </a>
        </h5>
      </div>
      <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="card-body">
            ....
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" role="tab" id="headingThree">
        <h5 class="mb-0">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="documentation.html#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Collapsible Group Item #3
          </a>
        </h5>
      </div>
      <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="card-body">
            ....
        </div>
      </div>
    </div>
  </div>
                                  </textarea>
                                  </div>
                                  <!-- Tabs Ends -->
                                  <!-- Tabs Starts -->
                                  <div data-pws-tab="Dropdown" data-pws-tab-name="Dropdown">
                                    <h5 class="mb-2 mt-4">Bootstrap Dropdown</h5>
                                    <div class="fluid-container py-4">
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Action
                                        </button>
                                        <div class="dropdown-menu">
                                          <a class="dropdown-item" href="documentation.html#">Action</a>
                                          <a class="dropdown-item" href="documentation.html#">Another action</a>
                                          <a class="dropdown-item" href="documentation.html#">Something else here</a>
                                          <div class="dropdown-divider"></div>
                                          <a class="dropdown-item" href="documentation.html#">Separated link</a>
                                        </div>
                                      </div>
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Action
                                        </button>
                                        <div class="dropdown-menu">
                                          <a class="dropdown-item" href="documentation.html#">Action</a>
                                          <a class="dropdown-item" href="documentation.html#">Another action</a>
                                          <a class="dropdown-item" href="documentation.html#">Something else here</a>
                                          <div class="dropdown-divider"></div>
                                          <a class="dropdown-item" href="documentation.html#">Separated link</a>
                                        </div>
                                      </div>
  
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Action
                                        </button>
                                        <div class="dropdown-menu">
                                          <a class="dropdown-item" href="documentation.html#">Action</a>
                                          <a class="dropdown-item" href="documentation.html#">Another action</a>
                                          <a class="dropdown-item" href="documentation.html#">Something else here</a>
                                          <div class="dropdown-divider"></div>
                                          <a class="dropdown-item" href="documentation.html#">Separated link</a>
                                        </div>
                                      </div>
  
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Action
                                        </button>
                                        <div class="dropdown-menu">
                                          <a class="dropdown-item" href="documentation.html#">Action</a>
                                          <a class="dropdown-item" href="documentation.html#">Another action</a>
                                          <a class="dropdown-item" href="documentation.html#">Something else here</a>
                                          <div class="dropdown-divider"></div>
                                          <a class="dropdown-item" href="documentation.html#">Separated link</a>
                                        </div>
                                      </div>
  
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          Action
                                        </button>
                                        <div class="dropdown-menu">
                                          <a class="dropdown-item" href="documentation.html#">Action</a>
                                          <a class="dropdown-item" href="documentation.html#">Another action</a>
                                          <a class="dropdown-item" href="documentation.html#">Something else here</a>
                                          <div class="dropdown-divider"></div>
                                          <a class="dropdown-item" href="documentation.html#">Separated link</a>
                                        </div>
                                      </div>
                                    </div>
                                    <textarea class="multiple-codes">
  <div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  <div class="btn-group">
    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
    <a class="dropdown-item" href="documentation.html#">Action</a>
    <a class="dropdown-item" href="documentation.html#">Another action</a>
    <a class="dropdown-item" href="documentation.html#">Something else here</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  
  <div class="btn-group">
    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  
  <div class="btn-group">
    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  
  <div class="btn-group">
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  </textarea>
  <h5 class="mb-2 mt-4">Dropdown Outlined</h5>
  <div class="fluid-container py-4">
    <div class="btn-group">
      <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="documentation.html#">Action</a>
        <a class="dropdown-item" href="documentation.html#">Another action</a>
        <a class="dropdown-item" href="documentation.html#">Something else here</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="documentation.html#">Separated link</a>
      </div>
    </div>
    <div class="btn-group">
      <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="documentation.html#">Action</a>
        <a class="dropdown-item" href="documentation.html#">Another action</a>
        <a class="dropdown-item" href="documentation.html#">Something else here</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="documentation.html#">Separated link</a>
      </div>
    </div>

    <div class="btn-group">
      <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="documentation.html#">Action</a>
        <a class="dropdown-item" href="documentation.html#">Another action</a>
        <a class="dropdown-item" href="documentation.html#">Something else here</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="documentation.html#">Separated link</a>
      </div>
    </div>

    <div class="btn-group">
      <button type="button" class="btn btn-outline-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="documentation.html#">Action</a>
        <a class="dropdown-item" href="documentation.html#">Another action</a>
        <a class="dropdown-item" href="documentation.html#">Something else here</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="documentation.html#">Separated link</a>
      </div>
    </div>

    <div class="btn-group">
      <button type="button" class="btn btn-outline-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="documentation.html#">Action</a>
        <a class="dropdown-item" href="documentation.html#">Another action</a>
        <a class="dropdown-item" href="documentation.html#">Something else here</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="documentation.html#">Separated link</a>
      </div>
    </div>
  </div>
  <textarea class="multiple-codes">
  <div class="btn-group">
    <button type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  <div class="btn-group">
    <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
    <a class="dropdown-item" href="documentation.html#">Action</a>
    <a class="dropdown-item" href="documentation.html#">Another action</a>
    <a class="dropdown-item" href="documentation.html#">Something else here</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  
  <div class="btn-group">
    <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  
  <div class="btn-group">
    <button type="button" class="btn btn-outline-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
  
  <div class="btn-group">
    <button type="button" class="btn btn-outline-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
    </button>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="documentation.html#">Action</a>
      <a class="dropdown-item" href="documentation.html#">Another action</a>
      <a class="dropdown-item" href="documentation.html#">Something else here</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="documentation.html#">Separated link</a>
    </div>
  </div>
</textarea>
  
</div>
  <!-- Tabs Ends -->

  <!-- Tabs Starts -->
  <div data-pws-tab="Tabs" data-pws-tab-name="Tabs">
    <h5 class="mb-2 mt-4">Bootstrap Tabs</h5>
    <div class="fluid-container py-4">
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" href="documentation.html#">Active</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="documentation.html#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="documentation.html#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="documentation.html#">Disabled</a>
        </li>
      </ul>
    </div>
    <textarea class="multiple-codes">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" href="documentation.html#">Active</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documentation.html#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documentation.html#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="documentation.html#">Disabled</a>
      </li>
    </ul>
      </textarea>
    </div>
    <!-- Tabs Ends -->
    <!-- Tabs Starts -->
    <div data-pws-tab="Modals" data-pws-tab-name="Modals">
                                      <h5 class="mb-2 mt-4">Bootstrap Modal</h5>
                                      <div class="fluid-container py-4">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#defaultModal">Launch demo modal</button>
                                        <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>This is a modal with default size</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary">Save changes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                      <textarea class="multiple-codes">
    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>This is a modal with default size</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
                                    </textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="bootstrap-Pagination" data-pws-tab-name="Pagination">
                                      <h5 class="mb-2 mt-4">Bootstrap Pagination</h5>
                                      <div class="fluid-container py-4">
                                        <nav aria-label="Page navigation example">
                                          <ul class="pagination">
                                            <li class="page-item"><a class="page-link" href="documentation.html#">Previous</a></li>
                                            <li class="page-item"><a class="page-link" href="documentation.html#">1</a></li>
                                            <li class="page-item active"><a class="page-link" href="documentation.html#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="documentation.html#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="documentation.html#">Next</a></li>
                                          </ul>
                                        </nav>
                                      </div>
                                      <textarea class="multiple-codes">
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item"><a class="page-link" href="documentation.html#">Previous</a></li>
        <li class="page-item"><a class="page-link" href="documentation.html#">1</a></li>
        <li class="page-item active"><a class="page-link" href="documentation.html#">2</a></li>
        <li class="page-item"><a class="page-link" href="documentation.html#">3</a></li>
        <li class="page-item"><a class="page-link" href="documentation.html#">Next</a></li>
      </ul>
    </nav>
                                      </textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="bootstrap-Badge" data-pws-tab-name="Badge">
                                      <h5 class="mb-2 mt-4">Bootstrap Badges</h5>
                                      <div class="fluid-container py-4">
                                        <div class="badge badge-primary">Default</div>
                                        <div class="badge badge-primary badge-pill">Pill</div>
                                        <div class="badge badge-outline-primary badge-pill">Outlined</div>
                                      </div>
                                      <textarea class="multiple-codes">
    <div class="badge badge-primary">Default</div>
    <div class="badge badge-primary badge-pill">Pill</div>
    <div class="badge badge-outline-primary badge-pill">Outlined</div>
                                      </textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                  </div>
                                  <!-- demo-tabs container ends -->
    
                                  <!-- New Docs Ends Here -->
                                  <!-- New Docs Starts Here -->
    
                                  <hr class="hr" id="doc-basic-ui-2">
                                  <h4 class="my-4">Basic UI Elements - 2</h4>
                                  <div class="demo-tabs">
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="bootstrap-Breadcrumb" data-pws-tab-name="Breadcrumb">
                                      <h5 class="mb-2 mt-4">Bootstrap Breadcrumb</h5>
                                      <div class="fluid-container py-4">
                                        <nav aria-label="breadcrumb">
                                          <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="documentation.html#">Home</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                                          </ol>
                                        </nav>
                                      </div>
                                      <textarea class="multiple-codes">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="documentation.html#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
      </ol>
    </nav>
                                      </textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="bootstrap-Progressbar" data-pws-tab-name="Progressbar">
                                      <h5 class="mb-2 mt-4">Bootstrap Progressbar</h5>
                                      <div class="fluid-container py-4">
                                        <div class="progress progress-md w-50">
                                          <div class="progress-bar bg-primary" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                      </div>
                                      <textarea class="multiple-codes">
    <div class="progress progress-md">
      <div class="progress-bar bg-primary" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
                                      </textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="bootstrap-tooltip" data-pws-tab-name="Tooltip">
                                      <h5 class="mb-2 mt-4">Bootstrap Tooltip</h5>
                                      <div class="container-fluid py-4">
                                        <button class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Basic tooltip">Hover me</button>
                                      </div>
                                      <p class="pt-4">
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                        To create a clipboard, add the following code:
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                  </div>
                                  <!-- demo-tabs container ends -->
    
                                  <!-- New Docs Ends Here -->
                                  <!-- New Docs Starts Here -->
    
                                  <hr class="hr" id="doc-advanced-ui">
                                  <h4 class="my-4">Advanced UI Elements</h4>
                                  <div class="demo-tabs">
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="Clipboard" data-pws-tab-name="Clipboard">
                                      <p>
                                        <a href="https://clipboardjs.com/">Clipboard</a>, a modern approach to copy text to the clipboard.
                                      </p>
                                      <h4 class="mt-5 mb-4">Usage</h4>
                                      <p>
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                        To create a clipboard, add the following code:
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="TodoList" data-pws-tab-name="Todo List">
                                      <p>
                                        <a href="http://www.jqueryscript.net/demo/Minimal-To-do-List-Task-Manager-App-Using-jQuery-Local-Storage/">Todolist</a> is a minimal jQuery plugin to create checklist.
                                      </p>
                                      <h4 class="mt-5 mb-4">Usage</h4>
                                      <p>
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes">
                                      </textarea>
                                      <p>
                                        To create a filterable list, add the following code:
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="Dragula" data-pws-tab-name="Dragula">
                                      <p>
                                        <a href="https://bevacqua.github.io/dragula/">Dragula</a> ,Drag and drop so simple it hurts.
                                      </p>
                                      <h4 class="mt-5 mb-4">Usage</h4>
                                      <p>
                                        To use Dragula in your application, include the following files in &lt;head&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                        To create a drag n drop list, add the following code:
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="context-menu" data-pws-tab-name="ContextMenu">
                                      <p>
                                        The contextMenu Plugin was designed for web applications in need of menus on a possibly large amount of objects. <a target="_blank" href="https://swisnl.github.io/jQuery-contextMenu/">Click Here</a> to see the official documentation.
                                      </p>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Ui-slider Starts -->
                                    <div data-pws-tab="UISlider" data-pws-tab-name="UI Slider">
                                      <p>
                                        <a href="https://refreshless.com/nouislider/">noUiSlider</a> is a range slider without bloat.
                                      </p>
                                      <h4 class="mt-5 mb-4">Usage</h4>
                                      <p>
                                        To use NoUISlider in your application, include the following files in &lt;head&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                    </div>
                                    <!-- Ui slider Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="Range-slider" data-pws-tab-name="Range Slider">
                                      <p>
                                        Easy, flexible and responsive range slider with skin support. <a target="_blank" href="http://ionden.com/a/plugins/ion.rangeSlider/en.html">Click Here</a> to see the official documentation.
                                      </p>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="Colgade" data-pws-tab-name="Colcade Grid">
                                      <p>
                                        <a href="https://github.com/desandro/colcade">Colcade</a> is a simple lightweight masonry layout.
                                      </p>
                                      <h4 class="mt-5 mb-4">Usage</h4>
                                      <p>
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                            <p>
                                              To create a responsive, add the following code:
                                            </p>
                                            <textarea class="multiple-codes">
                                            </textarea>
                                          </div>
                                          <!-- Tabs Ends -->
                                  </div>
                                  <!-- demo-tabs container ends -->
    
                                  <!-- New Docs Starts Here -->
    
                                  <hr class="hr" id="doc-media">
                                  <h4 class="my-4">Media</h4>
                                  <div class="demo-tabs">
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="LightBox" data-pws-tab-name="Lightbox">
                                      <p>
                                        <a href="http://sachinchoolur.github.io/lightGallery/">Light Gallery</a> is a customizable, modular, responsive, Lightbox gallery plugin for jQuery.
                                      </p>
                                      <h4 class="mt-5 mb-4">Usage</h4>
                                      <p>
                                        To use LightGallery in your application, include the following files in &lt;head&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                        To create Lightbox Gallery, add the following code:
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                    <!-- Tabs Starts -->
                                    <div data-pws-tab="owl-carousel" data-pws-tab-name="Owl Carousel">
                                      <p>
                                        <a href="https://owlcarousel2.github.io/OwlCarousel2/">Owl Carousel</a> is a touch enabled jQuery plugin that lets you create a beautiful responsive carousel slider.
                                      </p>
                                      <h4 class="mt-5 mb-4">Usage</h4>
                                      <p>
                                        To use Owl Carousel in your application, include the following files in &lt;head&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                      Add the following script files in &lt;body&gt;.
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                      <p>
                                        To create a carousel, add the following code:
                                      </p>
                                      <textarea class="multiple-codes"></textarea>
                                    </div>
                                    <!-- Tabs Ends -->
                                  </div>
                                  <!-- demo-tabs container ends -->
                                  <!--Tables starts-->
                                  <hr class="hr" id="doc-tables">
                                  <h4 class="my-4">Tables</h4>
                                  <div class="demo-tabs">
                                      <!-- Basic table starts -->
                                      <div data-pws-tab="basic-table" data-pws-tab-name="Basic table">
                                        <p>
                                          To create a basic Twitter Bootstrap table, add the following code.
                                        </p>
                                        <textarea class="multiple-codes"></div>
                                      <!-- Basic table Ends -->
                                      <!-- bootstrap-table Starts -->
                                      <div data-pws-tab="bootstrap-table" data-pws-tab-name="Bootstrap table">
                                        <p>
                                            <a href="http://bootstrap-table.wenzhixin.net.cn/">Bootstrap-table</a> is an extended Bootstrap table with radio, checkbox, sort, pagination, and other added features.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Bootstrap-table in your application, include the following files in &lt;head&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          Activate Bootstrap table without writing JavaScript, set data-toggle="table" on a normal table.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Bootstrap-table Ends -->
                                      <!-- Js-grid Starts -->
                                      <div data-pws-tab="js-grid" data-pws-tab-name="Js-grid">
                                        <p>
                                            <a href="http://js-grid.com/">Js-grid</a> creates simple responsive chartsis a lightweight client-side data grid control based on jQuery.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Js-grid in your application, include the following files in &lt;head&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a basic table using Js-grid, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Js-grid Ends -->
                                      <!-- Table-sorter Starts -->
                                      <div data-pws-tab="SortableTable" data-pws-tab-name="Sortable table">
                                        <p>
                                          <a href="http://tablesorter.com/docs/">Tablesorter</a> is a jQuery plugin for turning a standard HTML table with THEAD and TBODY tags into a sortable table without page refreshes.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Table in your application, include the following files in &lt;head&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></script>
                                        </textarea>
                                        <p>
                                          To create a Table, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Table-sorter Ends -->
                                  </div>
                                  <!--Tables ends-->
    
                                  <!-- New Docs Starts Here -->
                                  <!-- New Docs Ends Here -->
                                    <hr class="hr" id="doc-charts">
                                    <h4 class="my-4">Charts</h4>
                                    <div class="demo-tabs">
                                      <div data-pws-tab="anynameyouwant1" data-pws-tab-name="Chart.js">
                                        <p>
                                          <a href="http://www.chartjs.org/">Chart.js</a> is a simple yet flexible JavaScript charting for designers & developers.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Chart.js in your application, include the following files in &lt;head&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          and the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a simple chart, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <div data-pws-tab="Float-Chart" data-pws-tab-name="Float.js">
                                        <p>
                                          <a href="http://www.flotcharts.org/">Float.js</a> is a pure JavaScript plotting library for jQuery, with a focus on simple usage, attractive looks and interactive features.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          Add following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a simple chart, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <div data-pws-tab="google-chart" data-pws-tab-name="Google">
                                        <p>
                                          <a href="https://developers.google.com/chart/">Google</a> chart tools are powerful, simple to use, and free. Try out our rich gallery of interactive charts and data tools.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          Basic chart structure:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Tabs Starts -->
                                      <div data-pws-tab="c3" data-pws-tab-name="C3.js">
                                          <p>
                                              <a href="http://c3js.org/">C3.js</a> is a D3-based reusable chart library.
                                          </p>
                                          <h6>Usage</h6>
                                          <p>
                                              To use C3 charts in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                              To create a simple chart, add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Tabs Ends -->
                                      <!-- Chartist Starts -->
                                      <div data-pws-tab="chartist" data-pws-tab-name="Chartist">
                                        <p>
                                          <a href="https://gionkunz.github.io/chartist-js/">Chartist</a> creates simple responsive charts.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Chartist in your application, include the following files in &lt;head&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a simple line chart using Chartist, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Chartist Ends -->
                                      <!-- Morris Starts -->
                                      <div data-pws-tab="morris" data-pws-tab-name="Morris">
                                        <p>
                                          <a href="http://morrisjs.github.io/morris.js/">Morris</a> creates pretty time-series line graphs.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Morris in your application, include the following files in &lt;head&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a simple line chart using Morris, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Morris Ends -->
                                      <!-- Sparkline Starts -->
                                      <div data-pws-tab="sparkline" data-pws-tab-name="Sparkline">
                                        <p>
                                          <a href="https://omnipotent.net/jquery.sparkline/#s-about">Jquery Sparkline</a> generates sparklines (small inline charts) directly in the browser using data supplied either inline in the HTML, or via Javascript.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Sparkline in your application, add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a line chart using Sparkline, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Sparkline Ends -->
                                      <!-- Tabs Starts -->
                                      <div data-pws-tab="JustGage" data-pws-tab-name="Just Gage">
                                        <p>
                                          <a href="http://justgage.com/">Just Gage</a> is a handy JavaScript plugin for generating and animating nice & clean gauges.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a gage, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Tabs Ends -->
                                    </div>
                                    <!-- New Docs Starts Here -->
                                    <hr class="hr" id="doc-maps">
                                    <h4 class="my-4">Maps</h4>
                                    <div class="demo-tabs">
                                      <div data-pws-tab="Vector-map" data-pws-tab-name="Vector Map">
                                        <p>
                                          <a href="http://jvectormap.com/">JvectorMap</a> uses only native browser technologies like JavaScript, CSS, HTML, SVG or VML.
                                        </p>
                                        <h4 class="mt-5 mb-4">Usage</h4>
                                        <p>
                                          To use Vector map in your application, include the following files in &lt;head&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                        <p>
                                          To create a simple map, add the following code:
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Tabs Ends -->
                                      <!-- Tabs Starts -->
                                      <div data-pws-tab="Mapeal" data-pws-tab-name="Mapeal">
                                        <p>
                                          <a href="https://www.vincentbroute.fr/mapael/">Mapeal Map</a> Ease the build of pretty data visualizations on dynamic vector maps.
                                        </p>
                                        <p>
                                        Add the following script files in &lt;body&gt;.
                                        </p>
                                        <textarea class="multiple-codes"></textarea>
                                                <p>
                                                  To create a simple map, add the following code:
                                                </p>
                                                <textarea class="multiple-codes"></textarea>
                                      </div>
                                      <!-- Tabs Ends -->
                                      </div>
                                      <!-- demo-tabs container ends -->
                                      <!-- New Docs Ends Here -->
                                      <!--Forms starts-->
                                      <hr class="hr" id="doc-forms">
                                      <h4 class="my-4">Forms</h4>
                                      <div class="demo-tabs">
                                          <!-- Basic elements Starts -->
                                          <div data-pws-tab="basic-elements" data-pws-tab-name="Basic elements">
                                            <p>
                                              The basic form elements can be added to your application as below:
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Basic elements Ends -->
                                          <!-- Form validation Starts -->
                                          <div data-pws-tab="validation" data-pws-tab-name="Validation">
                                            <p>
                                                We are using <a href="https://jqueryvalidation.org/">Jquery validation</a> for simple clientside form validation.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use jquery validation in your application, include the following script files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              The following code shows validation of a simple form:
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Form validation Ends -->
                                          <!-- Wizard Starts -->
                                          <div data-pws-tab="Wizard" data-pws-tab-name="Wizard">
                                            <p>
                                                We are using <a href="http://www.jquery-steps.com/">Jquery steps</a> in our template to create form addons.
                                                It is an all-in-one wizard plugin that is extremely flexible, compact and feature-rich.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use jquery.steps in your application, include the following files &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              The following code generates a simple form wizard.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Wizard Ends -->
                                          <!-- Repeater Starts -->
                                          <div data-pws-tab="FormRepeater" data-pws-tab-name="Form Repeater">
                                            <p>
                                              <a href="http://briandetering.net/repeater">Jquery Repeater</a> is an interface to add and remove a repeatable group of input elements.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                            Add the following script files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              To create a Repeater, add the following code:
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Repeater Ends -->
                                      </div>
                                      <!--Forms ends-->
    
                                      <!--Additional form elements starts-->
                                      <hr class="hr" id="doc-additional-forms">
                                      <h4 class="my-4">Additional form elements</h4>
                                      <div class="demo-tabs">
                                        <!-- Tags Starts -->
                                        <div data-pws-tab="tags" data-pws-tab-name="Tags">
                                          <p>
                                              <a href="http://xoxco.com/projects/code/tagsinput/">jQuery-Tags-Input</a> magically convert a simple text input into a cool tag list with this jQuery plugin.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use jQuery-Tags-Input in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            To convert an input to tag using jQuery-Tags-Input, add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Tags Ends -->
                                        <!-- Rating Starts -->
                                        <div data-pws-tab="BarRating" data-pws-tab-name="Rating">
                                          <p>
                                            <a href="http://antenna.io/demo/jquery-bar-rating/examples/">jQuery Bar Rating</a> Plugin works by transforming a standard select field into a rating widget.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use Chroma rating in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            To create a simple rating, add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Rating Ends -->
                                        <!-- Tabs Starts -->
                                        <div data-pws-tab="BTMaxLength" data-pws-tab-name="Bootstrap Max-Length">
                                          <p>
                                            <a href="http://mimo84.github.io/bootstrap-maxlength/">Bootstrap MaxLength</a> uses a Twitter Bootstrap label to show a visual feedback to the user about the maximum length of the field where the user is inserting text. Uses the HTML5 attribute "maxlength" to work.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                                <p>
                                                  To create a Maxlength input, add the following code:
                                                </p>
                                                <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Tabs Ends -->
                                        <!-- Form mask Starts -->
                                        <div data-pws-tab="form-mask" data-pws-tab-name="Input Mask">
                                          <p>
                                              <a href="http://robinherbots.github.io/Inputmask/">Input-mask</a> helps the user with the input by ensuring a predefined format. This can be useful for dates, numerics, phone numbers etc.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use Inputmask in your application, include the following files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            The below code shows an example of input mask for date.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Form mask Ends -->
                                        <!-- Typeahead Starts -->
                                        <div data-pws-tab="typeahead" data-pws-tab-name="Typeahead">
                                          <p>
                                            <a href="https://twitter.github.io/typeahead.js/">Typeahead.js</a> is a flexible JavaScript library that provides a strong foundation for building robust typeaheads.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use Typeahead.js in your application, include the following files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            A sample typeahead can be generated as below:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Typeahead Ends -->
                                      </div>
                                      <!--Additional form elements ends-->
                                      <!--Icons starts-->
                                      <hr class="hr" id="doc-icons">
                                      <h4 class="my-4">Icons</h4>
                                      <div class="demo-tabs">
                                        <!-- MDI icon Starts -->
                                        <div data-pws-tab="mdi-icon" data-pws-tab-name="Material Icons">
                                          <p>
                                            <a href="https://materialdesignicons.com/">Material Design Icons</a> growing icon collection allows designers and developers targeting various platforms to download icons in the format, color and size they need for any project.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use Material Design Icons in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            To generate an icon, add the following code:
                                          </p>
                                          <textarea class="multiple-codes">
                                          </textarea>
                                        </div>
                                        <!-- MDI icon Ends -->
                                          <!-- Font awesome starts -->
                                          <div data-pws-tab="font-awesome" data-pws-tab-name="Font Awesome">
                                            <p>
                                                <a href="http://fontawesome.io/">Font Awesome</a> gives you scalable vector icons that can instantly be customized.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use Font Awesome in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              To create an address-book icon, add the following code:
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Font awesome Ends -->
                                          <!-- Themify Starts -->
                                          <div data-pws-tab="themify" data-pws-tab-name="Themify Icons">
                                            <p>
                                                <a href="https://themify.me/themify-icons">Themify Icons</a> Themify Icons is a complete set of icons for use in web design and apps.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use themify icons in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              To generate an icon, add the following code:
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Themify Ends -->
                                          <!-- Simple line icon Starts -->
                                          <div data-pws-tab="simple-line-icon" data-pws-tab-name="Simple Line Icons">
                                            <p>
                                                <a href="http://simplelineicons.com/">Simple Line Icons</a> is a set of simple and minimal line icons.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use Simple Line Icons in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              To generate an icon, add the following code:
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Simple line icon Ends -->
                                          <!-- Flag icon Starts -->
                                          <div data-pws-tab="flag-icon" data-pws-tab-name="Flag Icons">
                                            <p>
                                                <a href="http://flag-icon-css.lip.is/">Flag Icons</a> is a collection of all country flags in SVG — plus the CSS for easier integration.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use Simple Line Icons in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              To generate an icon, add the following code:
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                          </div>
                                          <!-- Simple line icon Ends -->
                                      </div>
                                      <!--icons ends-->
    
                                      <hr class="hr" id="doc-file-upload">
                                      <h4 class="my-4">File Upload</h4>
                                      <div class="demo-tabs">
                                        <!-- Tabs Starts -->
                                        <div data-pws-tab="Dropify" data-pws-tab-name="Dropify">
                                          <p>
                                            <a href="http://jeremyfagis.github.io/dropify/">Dropify</a> is a simple drag n drop file upload.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use Dropify in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes">
                                          </textarea>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes">
                                          </textarea>
                                          <p>
                                            To create a Dropify file upload, add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Tabs Ends -->
                                        <!-- Tabs Starts -->
                                        <div data-pws-tab="Dropzone" data-pws-tab-name="Dropzone">
                                          <p>
                                            <a href="http://www.dropzonejs.com/">Dropzone</a> is an open source library that provides drag’n’drop file uploads with image previews.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            To create a Dropzone file upload,add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Tabs Ends -->
                                        <!-- Tabs Starts -->
                                        <div data-pws-tab="jquery-file-upload" data-pws-tab-name="Jquery file upload">
                                          <p>
                                            <a href="http://hayageek.com/docs/jquery-upload-file.php">jQuery File UPload</a> plugin provides multiple file uploads with progress bar. jQuery File Upload Plugin depends on Ajax Form Plugin, So Github contains source code with and without form plugin.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use file upload in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            To create a Jquery file upload,add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Tabs Ends -->
                                      </div>
                                      <!-- demo-tabs container ends -->
                                      <!-- New Docs Ends Here -->
                                      <!-- New Docs Starts Here -->
                                      <hr class="hr" id="doc-form-picker">
                                      <h4 class="my-4">Form Picker</h4>
                                      <div class="demo-tabs">
                                        <!-- Tabs Starts -->
                                        <div data-pws-tab="clock-picker" data-pws-tab-name="Clock Picker">
                                          <p>
                                            We are using <a href="https://tempusdominus.github.io/bootstrap-4/">Tempus Dominus plugin</a> in our template to create beautiful time picker.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use clock picker in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            To create a clock picker, add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Tabs Ends -->
                                        <!-- Tabs Starts -->
                                        <div data-pws-tab="date-picker" data-pws-tab-name="Date Picker">
                                          <p>
                                            <a href="https://bootstrap-datepicker.readthedocs.io/en/latest/">Bootstrap Date Picker</a> provides a flexible datepicker widget in the Bootstrap style.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use bootstrap date picker in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                          Add the following script files in &lt;body&gt;.
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                          <p>
                                            To create a datepicker, add the following code:
                                          </p>
                                          <textarea class="multiple-codes"></textarea>
                                        </div>
                                        <!-- Tabs Ends -->
                                        <!-- Tabs Starts -->
                                        <div data-pws-tab="color-picker" data-pws-tab-name="Color Picker">
                                          <p>
                                            <a href="http://www.oss.io/p/amazingSurge/jquery-asColorPicker">AsColor Picker</a> is a jQuery plugin that convent input into color picker.
                                          </p>
                                          <h4 class="mt-5 mb-4">Usage</h4>
                                          <p>
                                            To use color picker in your application, include the following files in &lt;head&gt;.
                                          </p>
                                          <textarea class="multiple-codes">
<link rel="stylesheet" href="http://www.urbanui.com/chroma/documentation/path-to/node_modules/jquery-asColorPicker/dist/css/asColorPicker.min.css" />
  </textarea>
  <p>
  Add the following script files in &lt;body&gt;.
  </p>
  <textarea class="multiple-codes">
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/jquery-asColor/dist/jquery-asColor.min.js"></script>
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/jquery-asGradient/dist/jquery-asGradient.min.js"></script> //optional
                                          </textarea>
                                          <p>
                                            To create a color picker, add the following code:
                                          </p>
                                          <textarea class="multiple-codes">
<input type='text' class="color-picker" value="#ffe74c" />

<script>
$('.color-picker').asColorPicker()
</script></textarea>
                                        </div>
                                        <!-- Tabs Ends -->
                                      </div>
                                      <!-- demo-tabs container ends -->

                                      <!-- New Docs Ends Here -->
                                      <!--Form editors starts-->
                                      <hr class="hr" id="doc-editors">
                                      <h4 class="my-4">Editors</h4>
                                      <div class="demo-tabs">
                                          <!-- Tinymce Starts -->
                                          <div data-pws-tab="tinymce" data-pws-tab-name="Tinymce">
                                            <p>
                                                <a href="https://www.tinymce.com/">Tinymce</a> is a full-featured web editing tool.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use Tinymce in your application, include the following files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes"></textarea>
                                            <p>
                                              To create an editor using tinymce, add the following code:
                                            </p>
                                            <textarea class="multiple-codes">
                                            <textarea id='tinyMceExample'></textarea>
                                          </div>
                                          <!-- Tinymce Ends -->
                                          <!-- X-editable Starts -->
                                          <div data-pws-tab="x-editable" data-pws-tab-name="X-editable">
                                            <p>
                                                <a href="https://vitalets.github.io/x-editable/">X-editable</a> allows you to create editable elements on your page. It can be used with any engine (Bootstrap, jQuery-UI, jQuery only) and includes both popup and inline modes.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use x-editable in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<link rel="stylesheet" href="http://www.urbanui.com/chroma/documentation/path-to/node_modules/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" /></textarea>
                                            <p>
                                            Add the following script files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js"></script></textarea>
                                            <p>
                                              To create a simple editable text field, add the following code:
                                            </p>
                                            <textarea class="multiple-codes">
<a href="documentation.html#" id="username" data-type="text" data-pk="1">awesome</a>

<script>
$.fn.editable.defaults.mode = 'inline';
$.fn.editableform.buttons =
    '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
    '<i class="fa fa-fw fa-check"></i>' +
    '</button>' +
    '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
    '<i class="fa fa-fw fa-times"></i>' +
    '</button>';
$('#username').editable({
    type: 'text',
    pk: 1,
    name: 'username',
    title: 'Enter username'
});
</script></textarea>
                                          </div>
                                          <!-- X-editable Ends -->
                                          <!-- Summernote Starts -->
                                          <div data-pws-tab="summernote" data-pws-tab-name="Summernote">
                                            <p>
                                                <a href="http://summernote.org/">Summernote</a> is a super simple WYSIWYG Editor.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use summernote in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<link rel="stylesheet" href="http://www.urbanui.com/chroma/documentation/path-to/node_modules/summernote-bootstrap4/dist/summernote.css" /></textarea>
                                            <p>
                                            Add the following script files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/summernote-bootstrap4/dist/summernote.min.js"></script></textarea>
                                            <p>
                                              To create a summernote editor, add the following code:
                                            </p>
                                            <textarea class="multiple-codes">
<div id="summernoteExample"></div>

<script>
$('#summernoteExample').summernote({
    height: 300,
    tabsize: 2
});
</script></textarea>
                                          </div>
                                          <!-- Summernote Ends -->
                                          <!-- SimpleMde Starts -->
                                          <div data-pws-tab="simplemde" data-pws-tab-name="SimpleMDE">
                                            <p>
                                                <a href="https://simplemde.com/">SimpleMDE</a> is a simple, beautiful, and embeddable JavaScript Markdown editor.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use simpleMDE in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<link rel="stylesheet" href="http://www.urbanui.com/chroma/documentation/path-to/node_modules/simplemde/dist/simplemde.min.css" /></textarea>
                                            <p>
                                            Add the following script files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/simplemde/dist/simplemde.min.js"></script></textarea>
                                            <p>
                                              To create an editor using simpleMDE, add the following code:
                                            </p>
                                            <textarea class="multiple-codes">
<textarea id="simpleMde">Start editing here&lt;/textarea&gt;

<script>
if($("#simpleMde").length) {
  var simplemde = new SimpleMDE({ element: $("#simpleMde")[0] });
}
</script></textarea>
                                          </div>
                                          <!-- SimpleMDE Ends -->
                                          <!-- Quill Starts -->
                                          <div data-pws-tab="quill" data-pws-tab-name="Quill">
                                            <p>
                                                <a href="https://quilljs.com/">Quill</a> is a free, open source WYSIWYG editor built for the modern web.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use Quill in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<link rel="stylesheet" href="http://www.urbanui.com/chroma/documentation/path-to/node_modules/quill/dist/quill.snow.css" /></textarea>
                                            <p>
                                            Add the following script files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/quill/dist/quill.min.js"></script></textarea>
                                            <p>
                                              To create an editor using Quill, add the following code:
                                            </p>
                                            <textarea class="multiple-codes">
<div id="quillExample" class="quill-container"></div>

<script>
var quill = new Quill('#quillExample', {
    modules: {
        toolbar: [
            [{
                header: [1, 2, false]
            }],
            ['bold', 'italic', 'underline'],
            ['image', 'code-block']
        ]
    },
    placeholder: 'Compose an epic...',
    theme: 'snow' // or 'bubble'
});
</script></textarea>
                                          </div>
                                          <!-- Quill Ends -->
                                          <!-- Ace Starts -->
                                          <div data-pws-tab="ace" data-pws-tab-name="Ace">
                                            <p>
                                                <a href="https://ace.c9.io/">Ace</a> is an embeddable code editor written in JavaScript.
                                                It matches the features and performance of native editors such as Sublime, Vim and TextMate.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use ace editor in your application, include the following files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/ace-builds/src/ace.js"></script>
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/ace-builds/src-min/mode-javascript.js"></script>
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/ace-builds/src-min/theme-chaos.js"></script><!--Choose any theme you wish--></textarea>
                                            <p>
                                              To create a code editor using ace with a sample code, add the following code:
                                            </p>
                                            <textarea class="multiple-codes">
<div id="aceExample" class="ace-editor">
  var x = add(4, 3); // Function is called, return value will end up in x function add(a, b) { return a + b; // Function returns the sum of a and b } var y = mul(4, 3); // Function is called, return value will end up in y function add(a, b) { return a *
  b; // Function returns the product of a and b }
</div>

<script>
var editor = ace.edit("aceExample");
editor.setTheme("ace/theme/chaos"); //set theme
editor.getSession().setMode("ace/mode/javascript"); //set mode
document.getElementById('aceExample').style.fontSize='1rem'; //styling
</script></textarea>
                                          </div>
                                          <!-- Ace Ends -->
                                          <!-- Codemirror Starts -->
                                          <div data-pws-tab="codemirror" data-pws-tab-name="CodeMirror">
                                            <p>
                                                <a href="https://codemirror.net/">CodeMirror</a> is a versatile text editor implemented in JavaScript for the browser.
                                                It is specialized for editing code, and comes with a number of language modes and addons that implement more advanced editing functionality.
                                            </p>
                                            <h4 class="mt-5 mb-4">Usage</h4>
                                            <p>
                                              To use CodeMirror in your application, include the following files in &lt;head&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<link rel="stylesheet" href="http://www.urbanui.com/chroma/documentation/path-to/node_modules/codemirror/lib/codemirror.css" />
<link rel="stylesheet" href="http://www.urbanui.com/chroma/documentation/path-to/node_modules/codemirror/theme/ambiance.css" /><!--Choose a theme you wish--></textarea>
                                            <p>
                                            Add the following script files in &lt;body&gt;.
                                            </p>
                                            <textarea class="multiple-codes">
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/codemirror/lib/codemirror.js"></script>
<script src="http://www.urbanui.com/chroma/documentation/path-to/node_modules/codemirror/mode/javascript/javascript.js"></script></textarea>
                                            <p>
                                              Here is an example of an editor using CodeMirror.
                                            </p>
                                            <textarea class="multiple-codes">
<textarea rows="4" cols="50" name="code-editable" id="code-editable">
<script>
var x = 3;
var y = 4;
var z = x + y;
</script>
&lt;/textarea&gt;

<script>
var editableCodeMirror = CodeMirror.fromTextArea(document.getElementById('code-editable'), {
    mode: "javascript",
    theme: "ambiance",
    lineNumbers: true
});
</script></textarea>
                                          </div>
                                          <!--  CodeMirror Ends -->
                                      </div>
                                      <!--Form editors ends-->
                                      </div> <!-- Card Block Ends Here -->
                                    </div>
                    </div>
                    <div class="col-12 grid-margin doc-credits" id="doc-credits">
                      <div class="card">
                        <div class="card-body">
                          <h3 class="my-4">Credits</h3>
                          <p>We have used the following plugins in chroma admin</p>
                          <div class="row">
                            <div class="col-12 col-md-6">
                              <ul class="list-ticked">
                                <li>
                                  Ace editor <a href="https://ace.c9.io/" target="_blank">https://ace.c9.io/</a>
                                </li>
                                
                              </ul>
                            </div>
                            <div class="col-12 col-md-6">
                              <ul class="list-ticked">
                                <li>
                                  JQuery Toast <a href="https://kamranahmed.info/toast" target="_blank">https://kamranahmed.info/toast</a>
                                </li>
                                
                                <li>
                                  Xeditable <a href="https://vitalets.github.io/x-editable/" target="_blank">https://vitalets.github.io/x-editable/</a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
@include('__components.docs-footer')