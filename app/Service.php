<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use EDEV\Product;

class Service extends Model
{
    /*
    |---------------------------------------------
    | GET SERVICES FROM PRODUCTS
    |---------------------------------------------
    */
    public function getProductServices($product_id){
    	$services = Service::where("product_id", $product_id)->get();

    	// return
    	return $services;
    }


    /*
    |---------------------------------------------
    | ADD NEW SERVICES
    |---------------------------------------------
    */
    public function addNewService($payload){
        if($this->alreadyExist($payload)){
            $data = [
                'status'    => 'error',
                'message'   => $payload->name.' Already exist!',
            ];
        }else{

            // new servide
            $add_service                = new Service();
            $add_service->product_id    = $payload->product_id;
            $add_service->package       = $payload->name;
            $add_service->price         = $payload->price;
            $add_service->status        = 'active';
            if($add_service->save()){
                $data = [
                    'status'    => 'success',
                    'message'   => $payload->name.' added successfully!',
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Failed to add service!',
                ];
            }
        }

        // return
        return $data;
    }


    /*
    |---------------------------------------------
    | GET ALL SERVICES
    |---------------------------------------------
    */
    public function getAllServices($id){
        $product = Product::where('id', $id)->first();
        if($product !== null){
            
            $services = Service::where('product_id', $id)->get();
            $data = [
                'id'            => $product->id,
                'name'          => $product->name,
                'description'   => $product->description,
                'type'          => $product->type,
                'status'        => $product->status,
                'services'      => $services,
                'date'          => $product->created_at->diffForHumans()
            ];

        }else{

            $data = [];
        }

        // return 
        return $data;
    }


    /*
    |---------------------------------------------
    | CHECK ALREADY EXIST SERVICE
    |---------------------------------------------
    */
    public function alreadyExist($payload){
        $services = Service::where('package', $payload->name)->first();
        if($services !== null){
            return true;
        }else{
            return false;
        }
    }


    /*
    |---------------------------------------------
    | AIRTIME SERVICES
    |---------------------------------------------
    */
    public function airtimeService($id){
        $product = Product::where('id', $id)->first();
        if($product !== null){
            $mtn_airtime        = '%MTN AIRTIME%'; // 1
            $glo_airtime        = '%GLO AIRTIME%'; // 2
            $airtel_airtime     = '%AIRTEL AIRTIME%'; // 3
            $etisalat_airtime   = '%ETISALAT AIRTIME%'; // 4

            $mtn_data        = '%MTN DATA%'; // 1
            $glo_data        = '%GLO DATA%'; // 2
            $airtel_data     = '%AIRTEL DATA%'; // 3
            $etisalat_data   = '%ETISALAT DATA%'; // 4

            $service_1 = Service::where([['product_id', $id], ['package', 'LIKE', $mtn_airtime]])->get();
            $service_2 = Service::where([['product_id', $id], ['package', 'LIKE', $glo_airtime]])->get();
            $service_3 = Service::where([['product_id', $id], ['package', 'LIKE', $airtel_airtime]])->get();
            $service_4 = Service::where([['product_id', $id], ['package', 'LIKE', $etisalat_airtime]])->get();

            $service_1a = Service::where([['product_id', $id], ['package', 'LIKE', $mtn_data]])->get();
            $service_2a = Service::where([['product_id', $id], ['package', 'LIKE', $glo_data]])->get();
            $service_3a = Service::where([['product_id', $id], ['package', 'LIKE', $airtel_data]])->get();
            $service_4a = Service::where([['product_id', $id], ['package', 'LIKE', $etisalat_data]])->get();

            $data = [
                'id'            => $product->id,
                'name'          => $product->name,
                'description'   => $product->description,
                'type'          => $product->type,
                'status'        => $product->status,
                'services'      => [
                                    'airtime' => [
                                        'MTN'       => $service_1, 
                                        'GLO'       => $service_2, 
                                        'AIRTEL'    => $service_3, 
                                        'ETISALAT'  => $service_4
                                    ],
                                    'databundle' => [
                                        'MTN'       => $service_1a, 
                                        'GLO'       => $service_2a, 
                                        'AIRTEL'    => $service_3a, 
                                        'ETISALAT'  => $service_4a
                                    ],
                                ],
                'date'          => $product->created_at->diffForHumans()
            ];
        }else{
            $data = [];
        }

        // return 
        return $data;
    }
}
