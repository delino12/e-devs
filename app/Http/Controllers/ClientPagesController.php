<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\User;
use Auth;

class ClientPagesController extends Controller
{
    /*
    |-----------------------------------------
    | AUTHENTICATE CLIENT
    |-----------------------------------------
    */
    public function __construct(){
    	// body
    	$this->middleware('auth');
    }

    /*
    |-----------------------------------------
    | CLIENT DASHBOARD
    |-----------------------------------------
    */
    public function dashboard(){
    	// body
        $this->initBioData();
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
    	return view('__client.dashboard', compact('user'));
    }


    /*
    |-----------------------------------------
    | CLIENT TRANSACTIONS
    |-----------------------------------------
    */
    public function transactions(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.transactions', compact('user'));
    }

    /*
    |-----------------------------------------
    | CLIENT TRANSACTIONS
    |-----------------------------------------
    */
    public function singleTransaction($id){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.single-transaction', compact('user', 'id'));
    }


    /*
    |-----------------------------------------
    | CLIENT NOTIFICATIONS
    |-----------------------------------------
    */
    public function notifications(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.notifications', compact('user'));
    }

    /*
    |-----------------------------------------
    | CLIENT AIRTIME
    |-----------------------------------------
    */
    public function airtime(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.airtime', compact('user'));
    }

    /*
    |-----------------------------------------
    | CLIENT TRANSFER  
    |-----------------------------------------
    */
    public function transfer(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.transfer', compact('user'));
    }

    /*
    |-----------------------------------------
    | CLIENT DASHBOARD
    |-----------------------------------------
    */
    public function wallets(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.wallets', compact('user'));
    }


    /*
    |-----------------------------------------
    | CLIENT REPORTS
    |-----------------------------------------
    */
    public function reports(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.reports', compact('user'));
    }


    /*
    |-----------------------------------------
    | CLIENT BILLS
    |-----------------------------------------
    */
    public function bills(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.bills', compact('user'));
    }

    /*
    |-----------------------------------------
    | CLIENT BILLS
    |-----------------------------------------
    */
    public function withdraw(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.withdraw', compact('user'));
    }

    /*
    |-----------------------------------------
    | CLIENT BILLS
    |-----------------------------------------
    */
    public function deposit(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.deposit', compact('user'));
    }

    /*
    |---------------------------------------------
    | ADD BUSINESS
    |---------------------------------------------
    */
    public function business(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.business', compact('user'));
    }


    /*
    |-----------------------------------------
    | CLIENT SETTINGS
    |-----------------------------------------
    */
    public function setting(){
        // body
        $user_id    = Auth::user()->id;
        $user       = User::where('id', $user_id)->first();
        return view('__client.setting', compact('user'));
    }


    /*
    |---------------------------------------------
    | INITIALIZE BIO DATA
    |---------------------------------------------
    */
    public function initBioData(){
        $user_bio = new User;
        $user_bio->createBioData(Auth::user()->id, Auth::user()->name);
    }

    /*
    |-----------------------------------------
    | CLIENT LOGOUT
    |-----------------------------------------
    */
    public function logout(){
    	// body
    	Auth::logout();
    	return redirect('/');
    }
}
