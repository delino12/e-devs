<!--MAIN NAVIGATION-->
<!--===================================================-->
<nav id="mainnav-container">
    <div id="mainnav">
        <!--Menu-->
        <!--================================-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">

                    <!--Profile Widget-->
                    <!--================================-->
                    <div id="mainnav-profile" class="mainnav-profile">
                        <div class="profile-wrap text-center">
                            <div class="pad-btm">
                                <img class="img-circle img-md" src="{{ asset('admin-assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                            </div>
                            <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                <span class="pull-right dropdown-toggle">
                                    <i class="dropdown-caret"></i>
                                </span>
                                <p class="mnp-name">{{ Auth::user()->name }}</p>
                                <span class="mnp-desc">{{ Auth::user()->email }}</span>
                            </a>
                        </div>
                        <div id="profile-nav" class="collapse list-group bg-trans">
                            <a href="javascript:void(0);" class="list-group-item">
                                <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
                            </a>
                            <a href="javascript:void(0);" class="list-group-item">
                                <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                            </a>
                            <a href="javascript:void(0);" class="list-group-item">
                                <i class="demo-pli-information icon-lg icon-fw"></i> Help
                            </a>
                            <a href="{{ url('admin/logout') }}" class="list-group-item">
                                <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                            </a>
                        </div>
                    </div>


                    <!--Shortcut buttons-->
                    <!--================================-->
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="javascript:void(0);">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                    <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="javascript:void(0);">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                    <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="javascript:void(0);">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                    <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="javascript:void(0);">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                    <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--================================-->
                    <!--End shortcut buttons-->


                    <ul id="mainnav-menu" class="list-group">
			
			            <!--Category name-->
			            <li class="list-header">Navigation</li>
			            <!--Menu list item-->
			            <li>
			                <a href="{{ url('admin/dashboard') }}">
			                    <i class="fa fa-dashboard"></i>
			                    <span class="menu-title">Dashboard</span>
								<i class="arrow"></i>
			                </a>
			            </li>

                        <li class="list-divider"></li>
			
			            <!--Menu list item-->
			            <li>
			                <a href="{{ url('admin/settings') }}">
			                    <i class="fa fa-cogs"></i>
			                    <span class="menu-title">
									Configuration
								</span>
			                </a>
			            </li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-bell"></i>
                                <span class="menu-title">
                                    Notifications
                                </span>
                            </a>
                        </li>

                        <li class="list-divider"></li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-envelope-open"></i>
                                <span class="menu-title">
                                    Messaging Center
                                </span>
                            </a>
                        </li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-money"></i>
                                <span class="menu-title">
                                    Transactions
                                </span>
                            </a>
                        </li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-address-book"></i>
                                <span class="menu-title">
                                    Audit Trail
                                </span>
                            </a>
                        </li>

                        <li class="list-divider"></li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-line-chart"></i>
                                <span class="menu-title">
                                    Api Reports 
                                </span>
                            </a>
                        </li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-users"></i>
                                <span class="menu-title">
                                    All Users 
                                </span>
                            </a>
                        </li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-bank"></i>
                                <span class="menu-title">
                                    All Businesses 
                                </span>
                            </a>
                        </li>

                        <li class="list-divider"></li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-server"></i>
                                <span class="menu-title">
                                    Payment Option 
                                </span>
                            </a>
                        </li>

                        <!--Menu list item-->
                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-cog"></i>
                                <span class="menu-title">
                                    Setup Account 
                                </span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ url('admin/settings') }}">
                                <i class="fa fa-sign-out"></i>
                                <span class="menu-title">
                                    Logout 
                                </span>
                            </a>
                        </li>
			
			            <li class="list-divider"></li>
			        </ul>
                </div>
            </div>
        </div>
        <!--================================-->
        <!--End menu-->
    </div>
</nav>
<!--===================================================-->
<!--END MAIN NAVIGATION-->