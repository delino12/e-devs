<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;

class ExternalPagesController extends Controller
{
    /*
    |-----------------------------------------
    | ENTRY PAGE
    |-----------------------------------------
    */
    public function index(){
    	// body
    	return view('index');
    }

    /*
    |---------------------------------------------
    | API DOCUMENTATIONS & GUIDE
    |---------------------------------------------
    */
    public function docs(){
    	return view('docs.docs');
    }

    /*
    |---------------------------------------------
    | API DOCUMENTATIONS & GUIDE
    |---------------------------------------------
    */
    public function docsGuide(){
    	return view('docs.docs-guide');
    }

    /*
    |---------------------------------------------
    | SHOW TERMS AND CONDITIONS VIEWS
    |---------------------------------------------
    */
    public function terms(){
        return view('external-pages.terms');
    }

    /*
    |---------------------------------------------
    | SHOW POLICY VIEW
    |---------------------------------------------
    */
    public function policy(){
        return view('external-pages.policy');
    }
}
