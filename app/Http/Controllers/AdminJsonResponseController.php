<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Transfer;
use EDEV\Transaction;
use EDEV\User;
use EDEV\Product;
use EDEV\Service;

class AdminJsonResponseController extends Controller
{
    /*
    |---------------------------------------------
    | GET TRANSFER BALANCE (Paystack)
    |---------------------------------------------
    */
    public function getPaystackBalance(){
    	$transfer 	= new Transfer();
    	$data 		= $transfer->paystackBalance();

    	// return response
    	return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET TRANSFER BALANCE (E-Devs Platform)
    |---------------------------------------------
    */
    public function getPlatformBalance(){
        $transfer   = new Transfer();
        $data       = $transfer->getAdminBalance();

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | GET MOBILE AIRTIME BALANCE
    |---------------------------------------------
    */
    public function getMobileAirtimeBalance(){
        $transfer   = new Transfer();
        $data       = $transfer->getMobileBalance();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET FLUTTERWAVE BALANCE
    |---------------------------------------------
    */
    public function getFlutterwaveBalance(){
        $transfer   = new Transfer();
        $data       = $transfer->getFlutterBalance();

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | GET ALL USERS
    |---------------------------------------------
    */
    public function getAllUsers(){
        $users  = new User();
        $data   = $users->getAll();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET ALL TRANSACTIONS STAT
    |---------------------------------------------
    */
    public function getTransactionStats(){
        $transactions   = new Transaction();
        $data           = $transactions->getTransactionStatsAdmin();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | ADD PRODUCT
    |---------------------------------------------
    */
    public function addProduct(Request $request){
        $product    = new Product();
        $data       = $product->addNewProduct($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET PRODUCT
    |---------------------------------------------
    */
    public function getProducts(){
        $product    = new Product();
        $data       = $product->getProducts();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET PRODUCT
    |---------------------------------------------
    */
    public function getSingleProduct($id){
        $product    = new Product();
        $data       = $product->getOneProduct($id);

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | ADD SERVICES
    |---------------------------------------------
    */
    public function addServices(Request $request){
        $services   = new Service();
        $data       = $services->addNewService($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET ALL SERVICES
    |---------------------------------------------
    */
    public function getServices($id){
        $services   = new Service();
        $data       = $services->getAllServices($id);

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | GET ALL AIRTIME SERVICES
    |---------------------------------------------
    */
    public function getAirtimeServices($id){
        $services   = new Service();
        $data       = $services->airtimeService($id);

        // return response
        return response()->json($data);
    }
}
