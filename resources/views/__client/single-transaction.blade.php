@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | View Transaction
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">
              <span class="transaction-title"></span>
          </h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-header"><h4>Transaction Details</h4></div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Details</th>
                            <th>-------</th>
                        </tr>
                    </thead>
                    <tbody class="load-single-transactions"></tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script type="text/javascript">
    $(document).ready(function (e){
        loadSingleTransactionData();
    });

    function loadSingleTransactionData() {
        var transid = "{{$id}}";
        var params = {transid: transid};
        // body...
        $.get('{{url('load/single/transaction')}}', params, function(data) {
            console.log(data);
            var recipent;
            if(data.trans_type == "Bank Transfer"){
              if(data.details.status == "success"){
                recipent = `
                  ${data.details.account_name} <br /> 
                  (${data.details.account_bank} - ${data.details.account_number})
                `;
              }else{
                recipent = `Processing`;
              }
                
            }else{
              recipent = data.trans_to;
            }


            $(".load-single-transactions").html(`
                <tr>
                    <td>Amount</td>
                    <td>&#8358; ${data.amount}</td>
                <tr>
                <tr>
                    <td>Transaction ID</td>
                    <td>${data.trans_ref}</td>
                <tr>
                <tr>
                    <td>Recipient</td>
                    <td>${recipent}</td>
                <tr>
                <tr>
                    <td>Type of Transaction</td>
                    <td>${data.trans_type}</td>
                <tr>
                <tr>
                    <td>Transaction Note</td>
                    <td>${data.trans_note}</td>
                <tr>
                <tr>
                    <td>Date</td>
                    <td>${data.date}</td>
                <tr>
            `);

            $(".transaction-title").html(`
                ${data.trans_type} | ${data.trans_ref}
            `);
        });
    }
  </script>
@endsection