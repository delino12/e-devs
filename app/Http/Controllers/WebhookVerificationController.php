<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;
use EDEV\Transaction;
use EDEV\Account;
use EDEV\Webhook;

class WebhookVerificationController extends Controller
{
	/*
	|---------------------------------------------
	| VERIFY TRANSACTION VIA WEB-HOOK
	|---------------------------------------------
	*/
	public function verifyTransaction(Request $request){

		$this->saveWebhookPayload($request);

		$signature = $request->header('verif-hash');
		if (!$signature){
		    // only a post with rave signature header gets our attention
		    exit();
		}

		$local_signature = env('FLW_SECRET_HASH');
		if($signature !== $local_signature){
		  // silently forget this ever happened
		  exit();
		}else{
			$txRef 			= $request->txRef;
			$flwRef 		= $request->flwRef;
			$amount 		= $request->amount;
			$charged_amount = $request->charged_amount;
			$status 		= $request->status;
			$entity 		= $request->entity;

			// return response()->json($txRef);

			if($status == "successful"){
	    		$transaction = Transaction::where([['trans_ref', $txRef], ['trans_status', 'pending']])->first();
		    	if($transaction !== null){
		    		// find and update transaction
		    		$update_transaction 				= Transaction::find($transaction->id);
		    		$update_transaction->trans_status 	= 'success'; 
		    		$update_transaction->update();

		    		$amount 	= $transaction->amount - $transaction->fee;
		    		$user_id 	= $transaction->user_id;

		    		// update user account
		    		$account = new Account();
		    		$account->creditAccount($user_id, $amount);
		    	}
	    	}else{
	            $transaction = Transaction::where('trans_ref', $txRef)->first();
	            if($transaction !== null){
	                // find and update transaction
	                $update_transaction                 = Transaction::find($transaction->id);
	                $update_transaction->trans_status   = 'failed'; 
	                $update_transaction->update();

	                $amount     = $transaction->amount - $transaction->fee;
	                $user_id    = $transaction->user_id;
	            }
	        }
		}


	}

	/*
	|---------------------------------------------
	| SAVE WEBHOOK PAYLOAD
	|---------------------------------------------
	*/
	public function saveWebhookPayload($payload){
		$webhook 			= new Webhook();
		$webhook->tag 		= "flutterwave";
		$webhook->payload 	= json_encode($payload->all());
		$webhook->save();
	}
}
