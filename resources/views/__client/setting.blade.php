@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Setting
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Setting</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6 class="card-title">Profile Image</h6> 
            </div>
            <div class="card-body">
              <div id="preview-avatar">
                @if($user->bioData->avatar == null)
                  <img src="/client-assets/images/faces/face1.png" width="120" height="120" alt="Profile image">
                @else
                  <img src="{{ $user->bioData->avatar }}" width="120" height="120" style="border-radius: 5px;" alt="Profile image">
                @endif
              </div>
              <br />
              <button class="btn btn-primary" id="upload_widget_opener">
                <i class="fa fa-camera"></i>
                Choose profile image
              </button>
            </div>
          </div>
        </div>

        <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6 class="card-title">
                Account Information 
                <a href="javascript:void(0);" onclick="addBankModal()" class="pull-right">
                  <i class="fa fa-plus"></i> Add Bank
                </a>
              </h6>
            </div>
            <div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <td>Bank Name</td>
                      <td>Bank Account No.</td>
                      <td>Status</td>
                      <td>Option</td>
                    </tr>
                  </thead>
                  <tbody class="bank-details"></tbody>
                </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card support-pane-card">
            <div class="card-header">
              <h6 class="card-title">
                Profile Information
                <a href="javascript:void(0);" onclick="addProfileModal()" class="pull-right">
                 <i class="fa fa-edit"></i>  Update Profile
                </a>
              </h6>
            </div>
            <div class="card-body">
              <div class="profile_information"></div>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6 class="card-title">Change passwords</h6>
            </div>
            <div class="card-body">
              <form method="post" onsubmit="return changePassword();">
                <div class="row">
                  <div class="form-group col-sm-12">
                      <label for="new_password"><i class="fa fa-lock"></i> New password</label>
                      <input type="password" class="form-control" maxlength="11" id="new_password" placeholder="Enter new password" required="">
                  </div>

                  <div class="form-group col-sm-12">
                      <label for="confirm_new_password"><i class="fa fa-lock"></i> Confirm new password</label>
                      <input type="password" min="1" step="any" class="form-control" id="confirm_new_password" placeholder="Re-type new password" required="">
                  </div>
                  <div class="form-group col-sm-12">
                      <button class="btn btn-primary">
                        <i class="fa fa-lock"></i> Change Password 
                      </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
              <h6 class="card-title">Api Keys</h6>
            </div>
            <div class="card-body">
              <button class="btn btn-primary">
                <i class="fa fa-key"></i> Show API keys
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->

    <!-- include modal component -->
    @include('__components/modals')
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>  
  <script type="text/javascript">  
    document.getElementById("upload_widget_opener").addEventListener("click", function() {
      cloudinary.openUploadWidget({ 
        cloud_name: 'delino12', 
        upload_preset: 'znwx0uee',
        cropping: true, 
        folder: 'edevs-images'
      }, 
      function(error, result) { 
        if(error){
          swal(
            "oops",
            "Error trying to upload image, select image and try again",
            "error"
          );
        }else{
          $("#preview-avatar").html(`
            <img src="${result[0].url}" width="120" height="120" style="border-radius: 5px;" />
          `);

          var params = {
            avatar: result[0].url,
            _token:'{{ csrf_token() }}'
          }
          
          // upload avatar
          $.post('{{url('client/upload/avatar')}}', params, function(data, textStatus, xhr) {
            if(data.status == "success"){
              window.location.reload();
            }else{
              swal(
                data.status,
                data.message,
                "error"
              );
            }
          });
        }
      });
    }, false);

    getAcceptedBank();
    getListOfState();
    loadBankInfo();
    loadBusinessInfo();
    loadProfileInfo();

    // add bank modal
    function addBankModal() {
      $("#add-bank-modal").modal('show');
    }

    // add business modal
    function addBusinessModal() {
      $("#add-business-modal").modal('show');
    }

    // add profile modal
    function addProfileModal() {
      $("#update-profile-modal").modal('show');
    }

    // show auth modal
    function showAuthModal(new_password) {

      $("#verified_new_password").val(new_password);
      $("#show-auth-modal").modal('show');
    }

    // add bank information
    function updateBankAccount() {
        let token       = $("#token").val();
        let bankNumber  = $("#bank-number").val();
        let bankName    = $("#bank-name").val();
        let finalResult = bankName.split("-");

        let params = {
            _token: token,
            bank_number: bankNumber,
            bank_name: finalResult[1],
            bank_code: finalResult[0]
        };

        // console.log(params);
        $.post('{{url('create/user/bank/info')}}', params, function(data) {
            // console.log(data);
            if(data.status == "success"){
                swal(
                    "Ok",
                    data.message,
                    data.status
                );
            }else{
                swal(
                    "Ok",
                    data.message,
                    data.status
                );
            }
        });

        // void form
        return false;
    }

    // get list of bank
    function getAcceptedBank() {
        $.get('https://ravesandboxapi.flutterwave.com/banks', function(data) {
            // console.log(data);
            $("#bank-name").html("");
            $.each(data.data, function(index, val) {
                // console.log(val);
                $("#bank-name").append(`
                    <option value="${val.code}-${val.name}">${val.name}</option>
                `);
            });
        });
    }

    // get list of state
    function getListOfState() {
      $.get('{{ url('https://locationsng-api.herokuapp.com/api/v1/states') }}', function(data) {
        // console.log(data);
        // $("#state").html("");
        $.each(data, function(index, val) {
          // console.log(val);
          $("#state").append(`
            <option value="${val.name}">${val.name}</option>
          `);
        });
      });
    }

    // create new business
    function createNewBusiness() {
      var token         = $("#token").val();
      var business_name = $("#business_name").val();
      var total_agent   = $("#total_agent").val();
      var prod_1        = $("#prod_1").val();
      var prod_2        = $("#prod_2").val();
      var prod_3        = $("#prod_3").val();
      var prod_4        = $("#prod_4").val();
      var prod_5        = $("#prod_5").val();
      var prod_6        = $("#prod_6").val();

      // check bills and utility
      if($("#prod_1").is(":checked")){
        prod_1 = "Dstv";
      }

      if($("#prod_2").is(":checked")){
        prod_2 = "Gotv";
      }
      if($("#prod_3").is(":checked")){
        prod_3 = "eBank";
      }
      if($("#prod_4").is(":checked")){
        prod_4 = "Airtime";
      }
      if($("#prod_5").is(":checked")){
        prod_5 = "Sunking";
      }
      if($("#prod_6").is(":checked")){
        prod_6 = "Electricity";
      }

      // verify params
      var params = {
        _token: token,
        business_name: business_name,
        total_agent: total_agent,
        prod_1: prod_1,
        prod_2: prod_2,
        prod_3: prod_3,
        prod_4: prod_4,
        prod_5: prod_5,
        prod_6: prod_6
      };

      // console log value
      // console.log(params);

      // post data set
      $.post('{{url('create/new/business')}}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          swal(
            data.status,
            data.message,
            data.status
          );
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });

      // void form
      return false;
    }

    // load business information
    function loadBusinessInfo() {
      $.get('{{ url('load/business/information') }}', function(data) {
        // console log value
        // console.log(data);
        if(data !== ""){
          $(".business_information").html(`
            <table class="table card card-body">
              <tr>
                <td>Business Name</td>
                <td>${data.business.name}</td>
              </tr>

              <tr>
                <td>No. of Agent</td>
                <td>${data.business.total_agent}</td>
              </tr>

              <tr>
                <td>Status</td>
                <td>${data.business.status}</td>
              </tr>

              <tr>
                <td>Date created</td>
                <td>${data.business.created_at}</td>
              </tr>
            </table>

            <br /><br />
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agent ID</th>
                  <th>Balance(&#8358)</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody class="all-agents">
                <tr>
                  <td>Checking...</td>
                </tr>
              </tbody>
            </table>
            <div class="view-more pt-1"></div>
          `);
        }else{
          $(".business_information").html(`
            <h3>You do not have any business setup yet</h3>
          `);
        }

        $(".all-agents").html("");
        var sn = 0;
        $.each(data.agents, function(index, val) {
          /* iterate through array or object */
          sn++;
          if(val.name == null){
            val.name = "none";
          }

          if(val.balance == null){
            val.balance = "0.00";
          }

          $(".all-agents").append(`
            <tr>
              <td>${val.name}</td>
              <td>${val.agent_code}</td>
              <td>&#8358; ${val.balance}</td>
              <td><a href="javascript:void(0);">Edit</a></td>
            </tr>
          `);
          if(sn > 1){
            return false;
          }
        });

        $(".view-more").append(`
          <center>
            <a href="javascript:void(0);">View More</a>
          </center>
        `);
      });
    }

    // get user bank information
    function loadBankInfo() {
      $.get('{{ url('loan/user/bank/info') }}', function(data) {
        $(".bank-details").html("");
        $.each(data, function(index, val) {
          var status;
          if(val.isActive == 1){
            status = `<span class="text-success"> Active </span>`;
          }else{
            status = `<span class="text-danger"> Inactive </span>`;
          }
          $(".bank-details").append(`
            <tr>
              <td>${val.account_bank}</td>
              <td>${val.account_number}</td>
              <td>${status}</td>
              <td><a href="javascript:void(0);">Disable</a></td>
            </tr>
          `);
        });
      });
    }

    // update bio data
    function updateBioData() {
      var token     = $("#token").val();
      var firstname = $("#first-name").val();
      var lastname  = $("#last-name").val();
      var email     = $("#email").val();
      var phone     = $("#phone").val();
      var gender    = $("#gender").val();
      var dob       = $("#date_of_birth").val();
      var country   = $("#country").val();
      var state     = $("#state").val();
      var address   = $("#address").val();

      var params = {
        _token: token,
        firstname: firstname,
        lastname: lastname,
        email: email,
        phone: phone,
        gender: gender,
        dob: dob,
        country: country,
        state: state,
        address: address
      }

      $.post('{{ url('client/update/biodata') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          swal(
            data.status,
            data.message,
            data.status
          );
        }else{
          swal(
            data.status,
            data.message,
            "error"
          );
        }
      });

      // void form
      return false;
    }

    // load profile info
    function loadProfileInfo() {
      $.get('{{ url('load/profile/information') }}', function(data) {
        // console log value
        if(data !== null){

          if(data.phone == null){
            data.phone = 'none';
          }
          
          if(data.address == null){
            data.address = 'none';
          }

          $(".profile_information").html(`
            <table class="table">
              <tr>
                <td>First Name</td>
                <td>${data.firstname}</td>
              </tr>

              <tr>
                <td>Last Name</td>
                <td>${data.lastname}</td>
              </tr>

              <tr>
                <td>Email</td>
                <td>${data.email}</td>
              </tr>

              <tr>
                <td>Phone</td>
                <td>${data.phone}</td>
              </tr>

              <tr>
                <td>Gender</td>
                <td>${data.gender}</td>
              </tr>

              <tr>
                <td>Birth Date</td>
                <td>${data.dob}</td>
              </tr>

              <tr>
                <td>Country</td>
                <td>${data.country}</td>
              </tr>

              <tr>
                <td>State</td>
                <td>${data.state}</td>
              </tr>

              <tr>
                <td>Address</td>
                <td style="white-space: normal;">
                  ${data.address}
                </td>
              </tr>

              <tr>
                <td>Last updated</td>
                <td>${data.date}</td>
              </tr>
            </table>
          `);
        }else{
          $(".profile_information").html(`
            <h3>Update your profile information</h3>
          `);
        }
      });
    }

    // change user password
    function changePassword() {
      var newPassword     = $("#new_password").val();
      var verifyPassword  = $("#confirm_new_password").val();
      if(newPassword == verifyPassword){
        // show auth modal
        showAuthModal(newPassword);
      }else{

        swal(
          "oops",
          "Password does not match !",
          "error"
        );
      }

      // void
      return false;
    }

    // complete password change
    function completePasswordChange() {
      var token       = $("#token").val();
      var newPassword = $("#verified_new_password").val();
      var oldPassword = $("#old_password").val();

      var params = {
        _token: token,
        new_password: newPassword,
        old_password: oldPassword
      }


      $.post('{{ url('client/password/change') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          setTimeout(function(){
            // reload windows
            window.location.herf = "{{ url('/logout') }}";
          }, 1000);
          swal(
            "success",
            data.message,
            data.status
          );
        }else{
          swal(
            "error",
            data.message,
            data.status
          );
        }
      });

      // void form
      return false;
    }
  </script>
@endsection