<?php

namespace EDEV\Http\Controllers;

use Illuminate\Http\Request;

class TestMailController extends Controller
{
    /*
    |---------------------------------------------
    | return test mail
    |---------------------------------------------
    */
    public function testMail1(){
    	return view("mails.account-confirmation");
    }

    /*
    |---------------------------------------------
    | return test mail
    |---------------------------------------------
    */
    public function testMail2(){
    	return view("mails.deposit-notification");
    }

    /*
    |---------------------------------------------
    | return test mail
    |---------------------------------------------
    */
    public function testMail3(){
        return view("mails.transfer-notify-sender");
    }

    /*
    |---------------------------------------------
    | return test mail
    |---------------------------------------------
    */
    public function testMail4(){
        return view("mails.transfer-notify-receiver");
    }

    /*
    |---------------------------------------------
    | return test mail
    |---------------------------------------------
    */
    public function testMail5(){
        return view("mails.deposit-notification");
    }
}
