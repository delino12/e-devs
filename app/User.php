<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use EDEV\Mail\ResendAccountActivationLink;
use EDEV\Mail\AccountConfirmationMail;
use EDEV\Activation;
use EDEV\BioData;
use EDEV\Account;
use Hash;
use Auth;

class User extends Model
{
    /*
    |---------------------------------------------
    | USER RELATIONSHIP WITH ACCOUNT
    |---------------------------------------------
    */
    public function account(){
        $relative       = Account::class;
        $related_key    = "user_id";

        // return chain
        return $this->hasOne($relative, $related_key);
    }

    /*
    |-----------------------------------------
    | USER RELATIONSHIP WITH BIODATA
    |-----------------------------------------
    */
    public function bioData(){
        // body
        $relative   = BioData::class;
        $relate_key = 'user_id';
        return $this->hasOne($relative, $relate_key);
    }

    /*
    |-----------------------------------------
    | CREATE NEW USER
    |-----------------------------------------
    */
    public function addUser($request){
        // body
        $email      = $request->email;
        $names      = $request->names;
        $password   = $request->password;

        // init new user
        $user           = new User();
        $user->name     = $names;
        $user->email    = $email;
        $user->password = bcrypt($password);

        // if registration successful!
        if($user->save()){
            
            // user account
            $user = User::where("email", $email)->first();

            // user_id
            $user_id = $user->id;

            // create bio data
            $this->createBioData($user_id, $names);
            $this->createClientAccount($user_id);

            // create activation system
            $this->createAccountActivation($email);

            // attemp auth login
            if(Auth::attempt(['email' => $email, 'password' => $request->password])){
                return true;  
            }
        }else{
            return false;
        }
    }

    /*
    |-----------------------------------------
    | CHECK ALREADY EXISTING USER
    |-----------------------------------------
    */
    public function alreadyExistEmail($request){
        // body
        $email = $request->email;

        // check already exist
        $already_exist = User::where('email', $email)->first();
        if($already_exist !== null){
            return true;
        }else{
            return false;
        }
    }

    /*
    |-----------------------------------------
    | CREATE ACCOUNT
    |-----------------------------------------
    */
    public function createClientAccount($user_id){
        // body
        $account_id = 'NGN-00'.rand(000,999).rand(111,999).rand(222,999);
        $balance    = 0;
        $status     = "active";
        $isActive   = 0;

        $new_account                = new Account();
        $new_account->user_id       = $user_id;
        $new_account->account_id    = $account_id;
        $new_account->balance       = $balance;
        $new_account->status        = $status;
        $new_account->isActive      = $isActive;
        $new_account->save();
    }

    /*
    |---------------------------------------------
    | CREATE USER ID
    |---------------------------------------------
    */
    public function createBioData($user_id, $names){
        $bio_data = BioData::where('user_id', $user_id)->first();
        if($bio_data == null){

            $client_name = explode(" ", $names);

            if(count($client_name) > 1){
                // create new bio data
                $new_biodata            = new BioData();
                $new_biodata->firstname = $client_name[0];
                $new_biodata->lastname  = $client_name[1];
                $new_biodata->user_id   = $user_id;
                $new_biodata->save();
            }else{
                // create new bio data
                $new_biodata            = new BioData();
                $new_biodata->firstname = $client_name[0];
                $new_biodata->lastname  = $client_name[0];
                $new_biodata->user_id   = $user_id;
                $new_biodata->save();
            }
        }

        // return 
        return true;
    }

    /*
    |---------------------------------------------
    | FETCH USER ACCOUNT INFORMATION
    |---------------------------------------------
    */
    public function getUserInfo($user_id){
        $user_info = User::where("id", $user_id)->first();
        return $user_info;
    }

    /*
    |---------------------------------------------
    | CREATE & SEND ACTIVATION MAIL
    |---------------------------------------------
    */
    public function createAccountActivation($email){
        // generate token
        $token = bcrypt($email);

        // init account confirmation 
        $activation = new Activation();
        $activation->createActivationToken($email, $token);

        // mail data
        $mail_data = [
            'email' => $email,
            'token' => $token
        ];

        // send activation mail
        \Mail::to($email)->send(new AccountConfirmationMail($mail_data));

        // return 
        return true;
    }

    /*
    |---------------------------------------------
    | RESEND NEW ACTIVATION LINK
    |---------------------------------------------
    */
    public function resendNewActivationLink($email){
        // generate token
        $token = bcrypt($email);

        // init account confirmation 
        $activation = new Activation();
        $activation->resendNewLink($email, $token);

        // mail data
        $mail_data = [
            'email' => $email,
            'token' => $token
        ];

        // send activation mail
        \Mail::to($email)->send(new ResendAccountActivationLink($mail_data));

        $data = [
            'status'    => 'success',
            'message'   => 'Activation link has been sent to <b>'.$email.'</b>',
        ];

        // return data
        return $data;
    }

    /*
    |---------------------------------------------
    | CHANGE PASSWORD
    |---------------------------------------------
    */
    public function changeUserPassword($payload){
        $user_id = Auth::user()->id;
        $old_password = $payload->old_password;
        $new_password = $payload->new_password;

        $user = User::where("id", $user_id)->first();
        if(Hash::check($old_password, $user->password)){
            $update_password            = User::find($user_id);
            $update_password->password  = bcrypt($new_password);
            $update_password->save();

            $data = [
                'status'    => 'success',
                'message'   => 'Password changed successful!',
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid password!',
            ];
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | GET ALL USERS
    |---------------------------------------------
    */
    public function getAll(){
        $users = User::orderBy("id", "DESC")->get();
        if(count($users) > 0){
            $users_box = [];
            foreach ($users as $el) {
                $account        = new Account();
                $account_info   = $account->getAccountInfoFromAdmin($el->id);

                $biodata    = new BioData();
                $user_info  = $biodata->loadProfileInfoFromAdmin($el->id);

                $data = [
                    'id'        => $el->id,
                    'name'      => $el->name,
                    'email'     => $el->email,
                    'account'   => $account_info,
                    'biodata'   => $user_info,
                    'date'      => $el->created_at->diffForHumans()
                ];

                // return 
                array_push($users_box, $data);
            }
        }else{
            $users_box = [];
        }

        // return
        return $users_box;
    }
}
