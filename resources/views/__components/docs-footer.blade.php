
<!-- partial:../partials/_footer.html -->
    <footer class="footer">
      <div class="container-fluid clearfix">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
          <a href="http://urbanui.com" target="_blank">UrbanUI</a>. All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
          <i class="mdi mdi-heart-outline text-danger"></i>
        </span>
      </div>
    </footer>
    <!-- partial -->
  </div>
</div>
</div>

<!-- plugins:js -->
<script src="/client-assets/vendors/js/vendor.bundle.base.js"></script>
<script src="/client-assets/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="/client-assets/js/off-canvas.js"></script>
<script src="/client-assets/js/hoverable-collapse.js"></script>
<script src="/client-assets/js/misc.js"></script>
<script src="/client-assets/js/settings.js"></script>
<script src="/client-assets/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="/client-assets/js/codeEditor.js"></script>
<script src="/client-assets/js/tabs.js"></script>
<script src="/client-assets/js/tooltips.js"></script>
<script src="/client-assets/documentation/documentation.js"></script>
<!-- End custom js for this page-->
</body>

</html>