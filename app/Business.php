<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use EDEV\Agent;
use Auth;

class Business extends Model
{
    /*
    |---------------------------------------------
    | CREATE NEW BUSINESS
    |---------------------------------------------
    */
    public function addBusiness($payload){

    	$user_id 	= Auth::user()->id;
    	$name 		= $payload->business_name;
    	$agent_no 	= $payload->total_agent;
    	$prod_1 	= $payload->prod_1;
    	$prod_2 	= $payload->prod_2;
    	$prod_3 	= $payload->prod_3;
    	$prod_4 	= $payload->prod_4;
    	$prod_5 	= $payload->prod_5;
    	$prod_6 	= $payload->prod_6;

    	$products = [
    		'prod_1' => $prod_1,
    		'prod_2' => $prod_2,
    		'prod_3' => $prod_3,
    		'prod_4' => $prod_4,
    		'prod_5' => $prod_5,
    		'prod_6' => $prod_6
    	];

    	// convert to string
    	$services = implode(",", $products);

    	// check if business already exist
    	$already_exist = Business::where("user_id", $user_id)->first();
    	if($already_exist !== null){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $name. ' Already exist, You can not add another business at the moment!',
    		];
    	}else{
    		$new_business 				= new Business();
    		$new_business->user_id 		= $user_id;
    		$new_business->name 		= $name;
    		$new_business->total_agent 	= $agent_no;
    		$new_business->services 	= $services;
    		$new_business->status 		= "pending";
    		if($new_business->save()){

                // create agent account
                $new_agent = new Agent();
                $new_agent->generateAgent($new_business->id, $agent_no);

    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> $name. ' has been created successfully, pending activation',
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'failed to create new business, try again!',
    			];
    		}
    	}

    	// return
    	return $data;
    }

    /*
    |---------------------------------------------
    | GET ALL AGENTS
    |---------------------------------------------
    */
    public function getAllAgents(){
        $user_id = Auth::user()->id;
        $business = Business::where("user_id", $user_id)->first();
        if($business !== null){
            // get all agent
            $all_agent = Agent::where("biz_id", $business->id)->get();
            $data = [
                'business'  => $business,
                'agents'    => $all_agent
            ];
        }else{
            $data = [];
        }

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | GET ALL BUSINESS
    |---------------------------------------------
    */
    public function getAllBusiness(){
        $user_id = Auth::user()->id;
        $business = Business::where("user_id", $user_id)->first();
        if($business !== null){
            // get all agent
            $all_agent = Agent::where("biz_id", $business->id)->get();

            $services = explode(",", $business->services);

            $data = [
                'business'  => $business,
                'services'  => $services,
                'agents'    => $all_agent
            ];
        }else{
            $data = [];
        }

        // return
        return $data;
    }
}
