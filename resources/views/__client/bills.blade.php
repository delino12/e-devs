@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Bills payment
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Pay Bills</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
                <h5><i class="fa fa-tv"></i> Cable subscription</h5>
            </div>
            <div class="card-body">
              <form method="post" onsubmit="return payCableSubscription()">

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Select Cable Provider</label>
                      <select class="form-control" id="cable_provider" onchange="getCableServices()">
                        <option value=""> --select-- </option>
                      </select>
                    </div>
                    <div class="cable-option"></div>
                  </div>

                  <div class="col-md-6">
                    <div class="customer_information"></div>
                  </div>

                  <div class="col-md-12">
                    <button class="btn btn-primary">Pay Subscription</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-header">
                <h5><i class="fa fa-lightbulb-o"></i> Electricity & Solar subscription</h5>
            </div>
            <div class="card-body">
              <form method="post" onsubmit="return payElectricityCharges()">

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Select Electricity Provider</label>
                      <select class="form-control" id="power_provider" onchange="checkPowerProvider()">
                        <option value=""> --select-- </option>
                        <option value="sunking"> Sunking </option>
                        <option value="ekedc"> Eko Electricity (EKEDC) </option>
                      </select>
                    </div>

                    <div class="power-option"></div>
                  </div>

                  <div class="col-md-6">

                  </div>

                  <div class="col-md-12">
                    <button class="btn btn-info">Pay Subscription</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card support-pane-card">
            <div class="card-header">
              Transactions
            </div>
            <div class="card-body">
              <table class="table trans_table" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Status</th>
                        <th>Transaction</th>
                        <th>Amount</th>
                        <th>Ref ID</th>
                        <th>Date</th>
                        <th>More</th>
                    </tr>
                </thead>
                <tbody class="load-user-transaction">
                    <tr>
                        <td>Loading...</td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script type="text/javascript">
    // load modules
    getCableProducts();

    // check providers
    function checkCableProviderD() {
      var cable_provider = $("#cable_provider").val();
      if(cable_provider == ""){
        swal(
          "oops",
          "Select a cable provider",
          "info"
        );
      }else if(cable_provider == "dstv"){
        $(".cable-option").html(`
          <div class="form-group">
            <label>Dstv IUC (Smart card no)</label>
            <input type="text" pattern="[0-9]^" placeholder="Smart Card No..." id="decoder_no" class="form-control">
          </div>

          <div class="form-group">
            <label>Decoder Package</label>
            <select class="form-control" id="decoder_package">
              <option value="">-- none --</option>
              <option value="1">Dstv Premium</option>
              <option value="2">Dstv Compact Plus </option>
              <option value="3">Dstv Compact </option>
              <option value="4">Dstv Family</option>
              <option value="5">Dstv Access</option>
              <option value="6">Dstv FTA Plus</option>
              <option value="7">Dstv Indian</option>
              <option value="8">Great Wall Africa Bouquet</option>
            </select>
          </div>
        `);

        // get dstv products
        $.get('{{ url('get/dstv/packages') }}', function(data) {
          $.each(data, function(index, val) {
             /* iterate through array or object */
          });
        });


      }else if(cable_provider == "gotv"){
        $(".cable-option").html(`
          <div class="form-group">
            <label>Gotv IUC (Decoder's No)</label>
            <input type="text" pattern="[0-9]^" placeholder="IUC No..." id="decoder_no" class="form-control">
          </div>

          <div class="form-group">
            <label>Decoder Package</label>
            <select class="form-control" id="decoder_package">
              <option value="">-- none --</option>
              <option value="1">Gotv Max</option>
              <option value="2">Gotv Plus </option>
              <option value="3">Gotv Value </option>
              <option value="4">Gotv Lite (Monthly)</option>
              <option value="5">Gotv Lite (Quarterly)</option>
              <option value="6">Gotv Lite (Annual)</option>
            </select>
          </div>
        `);
      }else if(cable_provider == "startime"){
        $(".cable-option").html(`
          <br />
          <p>Startime is currently not available at the moment!</p>
          <br />
        `);
      }
    }

    // check power supply providers
    function checkPowerProvider() {
      var power_provider = $("#power_provider").val();
      if(power_provider == ""){
        swal(
          "oops",
          "Select a power provider",
          "info"
        );
      }else if(power_provider == "sunking"){
        $(".power-option").html(`
          <br />
          <p>Sunking is currently not available at the moment!</p>
          <br />
        `);
      }else if(power_provider == "ekedc"){
        $(".power-option").html(`
          <br />
          <p>EKEDC is currently not available at the moment!</p>
          <br />
        `);
      }
    }

    // get all cable services
    function getCableServices() {
      // body...
      var product_id = $("#cable_provider").val();
      $.get('{{ url('load/cable/services')}}/'+product_id, function(data) {
        /*optional stuff to do after success */
        $(".cable-option").html(`
          <div class="form-group">
            <label>Dstv IUC (Smart card no)</label>
            <input type="text" pattern="[0-9]^" maxlength="12" onblur="fetchCustomerInfo('${product_id}')" placeholder="Smart Card No..." id="decoder_no" class="form-control">
          </div>
          <div class="form-group">
            <label>Decoder Package</label>
            <select class="form-control" id="decoder_package">
              <option value="">-- none --</option>
            </select>
          </div>
        `);

        $("#decoder_package").val("");
        $.each(data, function(index, val) {
          $("#decoder_package").append(`
            <option value="${val.id}">${val.package} ${numeral(val.price).format('0,0.00')}</option>
          `);
        });
      });
    }

    // get all products
    function getCableProducts() {
      $.get('{{ url('load/cable/services') }}', function(data) {
        $("#cable_provider").val("");
        $.each(data, function(index, val) {
          /* iterate through array or object */
          $("#cable_provider").append(`
            <option value="${val.id}"> ${val.name} </option>
          `);
        });
      });
    }

    // fetch customer information
    function fetchCustomerInfo(product_id) {
      var smart_no = $("#decoder_no").val();

      if(smart_no.length > 9){
        var params = {
          product_id: product_id,
          smart_no: smart_no
        };

        $.get('{{ url('fetch/cable/customer/info') }}', params, function(data) {
          $(".customer_information").html("");
          $(".customer_information").html(`
            <h3 class="lead">Customer information</h3>
            <table class="table">
              <tbody>
                <tr>
                  <td>Customer Name:</td>
                  <td>${data.data.customer_name}</td>
                </tr>
                <tr>
                  <td>Decoder Status:</td>
                  <td>${data.data.decoder_status}</td>
                </tr>
                <tr>
                  <td>Subscription Exp Date:</td>
                  <td>${data.data.subscription_exp}</td>
                </tr>
              </tbody>
            </table>
          `);
        });
      }
    }

    // subscribe cable
    function payCableSubscription() {

      swal(
        "ok",
        "subscription successful!",
        "success"
      );      

      // void form
      return false;
    }
  </script>
@endsection