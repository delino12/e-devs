@extends('layouts.admin-auth')

{{-- title --}}
@section('title')
	Login page | E-DEVS - Admin
@endsection

{{-- contents --}}
@section('contents')
	<!-- LOGIN FORM -->
	<!--===================================================-->
	<div class="cls-content">
	    <div class="cls-content-sm panel">
	        <div class="panel-body">
	            <div class="mar-ver pad-btm">
	                <h1 class="h3">Admin Login</h1>
	                <p>Sign In to your account</p>
	            </div>
	            <form onsubmit="return loginAdmin()" method="post">
	                <div class="form-group">
	                    <input type="text" id="email" class="form-control" placeholder="Username" autofocus required="">
	                </div>
	                <div class="form-group">
	                    <input type="password" id="password" class="form-control" placeholder="Password" required="">
	                </div>
	                <div class="checkbox pad-btm text-left">
	                    <input id="remember" class="magic-checkbox" type="checkbox">
	                    <label for="remember">Remember me</label>
	                </div>
	                <button class="btn btn-primary btn-lg btn-block" id="signin-btn" type="submit">Sign In</button>
	            </form>
	        </div>
	    </div>
	</div>
	<!--===================================================-->
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		// process login
		function loginAdmin(){
			$("#signin-btn").html(`
				<i class="fa fa-spinner fa-spin"></i> processing...
			`);

			var token 		= $("#token").val();
			var email 		= $("#email").val();
			var password 	= $("#password").val();
			var remember 	= $("#remember").val();

			var params = {
				_token: token,
				email: email,
				password: password,
				remember: remember
			}

			// console log value
			// console.log(params);
			$.post('{{ url('admin/login') }}', params, function(data, textStatus, xhr) {
				/*optional stuff to do after success */
				if(data.status == "success"){
					window.location.href = "/admin/dashboard";
				}else{
					swal("oops!", data.message, data.status);
					$("#signin-btn").html(`
						Sign In
					`);
				}
			}).fail(function (err){
				console.log(err);
				$("#signin-btn").html(`
					Sign In
				`);
			});

			// void form
			return false;
		}
	</script>
@endsection