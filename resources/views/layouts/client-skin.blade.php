<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <meta name="description" content="E-Devs, The future is flexible, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business." />
  <meta name="keywords" content="Bank Transfers, Payment Solutions, Bills & Cable Subscriptions, Airtime & Data Purchase, Api Friendly" />
  <meta name="author" content="Ekpoto Liberty" />
  <!-- Facebook and Twitter integration -->
  <meta property="og:title" content="E-Devs, The future is flexible, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business."/>
  <meta property="og:image" content="https://e-devs.com/images/feature.jpg"/>
  <meta property="og:url" content="https://e-devs.com"/>
  <meta property="og:site_name" content="E-Devs"/>
  <meta property="og:description" content="We at E-Devs we say payment should not be stressful, transfer, pay and receive money anytime or everytime."/>

  {{-- twitter --}}
  <meta name="twitter:title" content="E-Devs, The future is flexible, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business." />
  <meta name="twitter:image" content="https://e-devs.com/images/feature.jpg" />
  <meta name="twitter:url" content="https://e-devs.com" />
  <meta name="twitter:card" content="E-Devs, The future is flexible, enjoy easy funds transfer, bills & cable subscriptions and multi-agent system for all business." />
  
  <!-- plugins:css -->
  <link rel="stylesheet" href="/client-assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="/client-assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="/client-assets/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css">
  <link rel="stylesheet" href="/client-assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="/client-assets/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="/client-assets/css/style.css" />
  <link rel="stylesheet" href="/fonts/fontawesome/css/font-awesome.min.css" />
  <!-- endinject -->
  <link rel="shortcut icon" href="/images/logo.png" />
  <link rel="stylesheet" type="text/css" href="/css/toast.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132345822-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-132345822-1');
  </script>
  
  <!-- Start of Async Drift Code -->
  <script>
    "use strict";
    !function() {
      var t = window.driftt = window.drift = window.driftt || [];
      if (!t.init) {
        if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
        t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
        t.factory = function(e) {
          return function() {
            var n = Array.prototype.slice.call(arguments);
            return n.unshift(e), t.push(n), t;
          };
        }, t.methods.forEach(function(e) {
          t[e] = t.factory(e);
        }), t.load = function(t) {
          var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
          o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
          var i = document.getElementsByTagName("script")[0];
          i.parentNode.insertBefore(o, i);
        };
      }
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('mpdx3e9kz73m');
  </script>
  <!-- End of Async Drift Code -->
</head>

<body>
  <div class="container-scroller">
    <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
    @include('__components.topbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      @include('__components.theme-setting')
      @include('__components.sidebar')

      <!-- partial -->
      <div class="main-panel">
        @yield('contents')

        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © {{ date("Y") }}
            <a href="javascript:void(0);">E-Devs</a>. All rights reserved.</span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="/client-assets/vendors/js/vendor.bundle.base.js"></script>
  <script src="/client-assets/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="/client-assets/js/off-canvas.js"></script>
  <script src="/client-assets/js/hoverable-collapse.js"></script>
  <script src="/client-assets/js/misc.js"></script>
  <script src="/client-assets/js/settings.js"></script>
  <script src="/client-assets/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="/client-assets/js/dashboard.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@6.6.2/dist/sweetalert2.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="/js/toast.js"></script>
  <script src="https://cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js"></script>

  <!-- End custom js for this page-->
  <script type="text/javascript">
    @if(Auth::check())
      // display account
      displayBalance();
      getAcceptedBank();
      loadTransactionsData();
      loadTotalDeposit();
      loadTotalWithdraw();
      loadSuccessfulTransaction();
      loadPendingTransactions();
      loadFailedTransactions();
      showAccountActivationStatus();
    @endif

    // get user account balance
    function displayBalance() {
      $.get('{{url('load/user/account/balance')}}', function(data) {
        $(".account_balance").html(`
          &#8358;${data.balance}
        `);

        $(".account_id").html(`
          <i class="fa fa-key"></i> ${data.account_id}
        `);
      });
    }

    // verify transaction
    function verifyTransaction() {
      $(".verify-loading").fadeIn();
      let token     = $("#token").val();
      let reference   = $("#charge_refrence").val();
      let oneTimePass = $("#charge_otp").val();

      // query string
      var params = {
        _token: token,
        reference: reference,
        otp: oneTimePass
      };

        // console log data
        // console.log(params);

      // verify transaction
      $.post('{{url('charge/verification')}}', params, function(data, textStatus, xhr) {
        // console.log(data);
        $(".verify-loading").fadeOut();
        if(data.status === 'success'){
                swal({
                    title: 'Transaction successful!',
                    html: 'Redirecting..... <strong>3</strong> seconds.'
                });
        // console log data
                // console.log(data);
                window.location.href = "/transactions";
        }else{
          swal("Ops!", data.message);
                $(".verify-loading").fadeOut();
        }
      });

      // void form
      return false;
    }

    // get list of bank
    function getAcceptedBank() {
        $.get('{{url('get/bank/list')}}', function(data) {
            // console.log(data);
            $("#bank_code").html("");
            $("#bank_code").append(`
              <option value=""> -- select bank --</option>
            `);
            $.each(data.data, function(index, val) {
                $("#bank_code").append(`
                    <option value="${val.code}">${val.name}</option>
                `);
            });
        });
    }

    // transaction data
    function loadTransactionsData() {
        $.get('{{url('load/user/transactions')}}', function(data) {
            $(".load-user-transaction").html("");
            let sn = 0;
            $.each(data, function(index, val) {
                sn++;

                // check status
                let status;
                if(val.trans_status == 'pending'){
                    status = `<i class="fa fa-circle-o text-warning"></i> ${val.trans_status}`;
                }

                if(val.trans_status == 'failed'){
                    status = `<i class="fa fa-circle-o text-danger"></i> ${val.trans_status}`;
                }

                if(val.trans_status == 'success'){
                    status = `<i class="fa fa-circle-o text-success"></i> ${val.trans_status}`;
                }

                // console.log(val);
                $(".load-user-transaction").append(`
                    <tr>
                        <td>${sn}</td>
                        <td>${status}</td>
                        <td>${val.trans_type}</td>
                        <td>&#8358; ${val.amount}</td>
                        <td>${val.trans_ref}</td>
                        <td>${val.date}</td>
                        <td><a href="{{url('transaction-details')}}/${val.id}">vew</a></td>
                    </tr>
                `);
            });

            $('.trans_table').DataTable();
        });
    }

    // top up mobile
    function topUpMobile() {
      $(".airtime-data-loading").fadeIn();
      var token   = $("#token").val();
      var mobile  = $("#receivers_mobile").val();
      var amount  = $("#receivers_amount").val();
      var topType = $("#recharge_type").val();
      var note    = $("#airtime_sender_notes").val();
      var network = $("#recharge_network").val();

      var params = {
        _token: token,
        mobile: mobile,
        amount: amount,
        topType: topType,
        network: network,
        note: note
      };

      $.post('{{url('process/mobile/topup')}}', params, function(data) {
        if(data.status == "success"){
          $(".airtime-data-loading").fadeOut();
          swal("Ok", data.message, "success");
          loadTransactionsData();
          displayBalance();
          loadSuccessfulTransaction();
          loadPendingTransactions();
          loadFailedTransactions();
        }else{
          $(".airtime-data-loading").fadeOut();
          swal("oops!", data.message, "error");
        }
      }).fail(function (err){
        $(".airtime-data-loading").fadeOut();
        swal("oops!", "check internet connection!", "error");
      });
      return false;
    }

    // load total deposit 
    function loadTotalDeposit() {
      $.get('{{ url('/load/total/deposit') }}', function(data) {
        // console.log(data);
        $(".total-deposit").html(data.total_deposit);
      });
    }

    // load total withdraw 
    function loadTotalWithdraw() {
      $.get('{{ url('/load/total/withdraw') }}', function(data) {
        // console.log(data);
        $(".total-withdraw").html(data.total_withdraw);
      });
    }

    // load successful transaction 
    function loadSuccessfulTransaction() {
      $.get('{{ url('/load/total/success') }}', function(data) {
        // console.log(data);
        $(".total-success").html(data.total_successful);
      });
    }

    // load pending transaction 
    function loadPendingTransactions() {
      $.get('{{ url('/load/total/pending') }}', function(data) {
        // console.log(data);
        $(".total-pending").html(data.total_pending);
      });
    }

    // load failed transaction 
    function loadFailedTransactions() {
      $.get('{{ url('/load/total/failed') }}', function(data) {
        // console.log(data);
        $(".total-failed").html(data.total_failed);
      });
    }

    // local transfer
    function transferToAnotherUser() {
      $(".tt-transfer-loading").fadeIn();
      var token       = $("#token").val();
      var account_id  = $("#tt_account_id").val();
      var amount      = $("#tt_amount").val();
      var note        = $("#tt_note").val();

      var params = {
        _token: token,
        account_id: account_id,
        amount: amount,
        note: note
      };

      $.post('{{ url('process/internal/transfer/') }}', params, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        if(data.status == "success"){
          $(".tt-transfer-loading").fadeOut();
          swal("Ok", data.message, "success");
          loadTransactionsData();
          displayBalance();
          loadSuccessfulTransaction();
          loadPendingTransactions();
          loadFailedTransactions();
        }else{
          $(".tt-transfer-loading").fadeOut();
          swal("oops!", data.message, "error");
        }
      }).fail(function (err){
        $(".tt-transfer-loading").fadeOut();
        swal("oops!", "oops!, check internet connectivity!", "error");
      });

      return false;
    }

    // show account activation status
    function showAccountActivationStatus() {
      $.get('{{ url('account/activation/status') }}', function(data) {
        // console log value
        // console.log(data);
        if(data.status == "inactive"){
          // show update notification
          $("#activation-status-bar").html(`
            <span style="color:#ff0000;">
              Account is not activated, click 
                <a href="javascript:void(0);" onclick="resendActivationLink()">resend</a> 
              for activation link!
            </span>
          `);
        }
      });
    }

    // resend activation link
    function resendActivationLink(argument) {
      // body...
      var token = $("#token").val();
      var params = {
        _token: token
      }
      $.post('{{ url('resend/activation/link') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          $("#activation-status-bar").html(`
            <span style="color:#14a51a;">
              ${data.message}
            </span>
          `);
        }
      });
    }

    // resolve bank information
    function resolveBankInfo() {
      $("#loading-account-info").fadeIn();
      // body...
      var account = $("#receivers_account").val();
      var bcode   = $("#bank_code").val();

      if(account == ""){
        swal(
          "oops",
          "Enter recipient account number",
          "error"
        );
        $("#loading-acct_no").fadeOut();
        return false;
      }

      // params
      var params = {
        account: account,
        bcode: bcode
      };

      $.get('{{ url('resolve/account/bin') }}', params, function(data, textStatus, xhr) {
        // console.log(data);
        if(data.status === true){
          $("#resolve-account").html(`
            <label for="generated_reciept">Beneficiary</label>
            <input type="text" class="form-control" id="recievers_account_name" value="${data.data.account_name.toUpperCase()}" readonly>
          `);
          $("#loading-account-info").fadeOut();
        }else{
          swal(
            "oops",
            data.message,
            "error"
          );
          $("#loading-account-info").fadeOut();
        }
      });
    }
  </script>
  @yield('scripts')
</body>

</html>