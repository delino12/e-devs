<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    /*
    |---------------------------------------------
    | CHECK IF ACCOUNT IS VERIFIED
    |---------------------------------------------
    */
    public function verifyAccountActivation($email){
    	$check_verify = Activation::where([["email", $email], ["status", "active"]])->first();
    	if($check_verify !== null){
    		return true;
    	}else{
    		return false;
    	}
    }

    /*
    |---------------------------------------------
    | ACTIVATE USER ACCOUNT
    |---------------------------------------------
    */
    public function activateAccount($token){
    	$verify_tokenize = Activation::where("token", $token)->first();
    	if($verify_tokenize !== null){
            
            $update_activation          = Activation::find($verify_tokenize->id);
            $update_activation->status  = "active";

            if($update_activation->update()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Account verification succcessful!',
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'oops verification was not succcessful!',
                ];
            }
    	}else{
			$data = [
				'status' 	=> 'error',
				'message' 	=> 'verification failed, token expired or account does not exist!',
			];
    	}

    	// return
    	return $data;
    }

     /*
    |---------------------------------------------
    | CHECK ACTIVATE STATE
    |---------------------------------------------
    */
    public function activateState($email, $token){
        $check_already_verify = Activation::where([["email", $email], ["status", "active"]])->first();
        if($check_already_verify !== null){
            $data = [
                'status'    => 'success',
                'message'   => 'Account is already active!',
            ];
        }else{
            $check_new_verification = Activation::where([["email", $email], ["status", "inactive"]])->first();
            if($check_new_verification !== null){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Account confirmation successful!',
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => $email.' activation failed, check if account exist!',
                ];
            }
        }

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | CREATE NEW ACTIVATION
    |---------------------------------------------
    */
    public function createActivationToken($email, $token){
    	$check_already_created = Activation::where("email", $email)->first();
    	if($check_already_created !== null){
    		// update existing
    		$update_existing 		= Activation::find($check_already_created->id);
    		$update_existing->token = $token;
    		$update_existing->update();
    	}else{
    		// create new
    		$create_activation 			= new Activation();
    		$create_activation->token 	= $token;
    		$create_activation->email 	= $email;
    		$create_activation->status 	= "inactive";
    		$create_activation->save();
    	}

    	// return
    	return true;
    }

    /*
    |---------------------------------------------
    | GET ACCOUNT ACTIVATION STATUS
    |---------------------------------------------
    */
    public function getActivationStatus($email){
        $activation = Activation::where("email", $email)->first();
        if($activation !== null){
            if($activation->status == "inactive"){
                $data = $activation;
            }else{
                $data = [];
            }

            // return
            return $data;
        }else{

            // generate token
            $token = bcrypt($email);

            // create new
            $create_activation          = new Activation();
            $create_activation->token   = $token;
            $create_activation->email   = $email;
            $create_activation->status  = "inactive";
            $create_activation->save();

            $activation = Activation::where("email", $email)->first();
            $data = $activation;

            // return 
            return $data;
        }
    }

    /*
    |---------------------------------------------
    | RESEND ACTIVATION LINK
    |---------------------------------------------
    */
    public function resendNewLink($email, $token){
        $check_already_created = Activation::where("email", $email)->first();
        if($check_already_created !== null){
            // update existing
            $update_existing            = Activation::find($check_already_created->id);
            $update_existing->token     = $token;
            $update_existing->status    = "inactive";
            $update_existing->update();

            return true;
        }else{
            return false;
        }
    }
}
