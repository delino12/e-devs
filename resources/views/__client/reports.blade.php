@extends('layouts.client-skin')

{{-- title section --}}
@section('title')
  E-Devs | Reports
@endsection

{{-- contents --}}
@section('contents')
    <div class="content-wrapper">
      <div class="row mb-4">
        <div class="col-12 d-flex align-items-center justify-content-between">
          <h4 class="page-title">Reports</h4>
          <div class="d-flex align-items-center">
            <div class="wrapper mr-4 d-none d-sm-block">
              <p class="mb-0">
                <b class="mb-0">{{ date("D M' Y") }}</b>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h6 class="card-title">Total Transaction</h6>
              <div class="w-75 mx-auto mt-4">
                <div class="d-flex justify-content-between text-center mb-2">
                  <div class="wrapper text-success">
                    <h4>0</h4>
                    <small class="text-muted">Successful Deposit</small>
                  </div>
                  <div class="wrapper text-danger">
                    <h4>0</h4>
                    <small class="text-muted">Pending Deposit</small>
                  </div>
                </div>
              </div>
              {{-- <div id="morris-line-example"></div> --}}
            </div>
          </div>
        </div>
        <div class="col-md-7 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="account_number">Deposit amount</label>
                        <input type="number" class="form-control" step="any" min="1" id="deposit_amount" placeholder="Eg. 1000">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <button class="btn btn-default" onclick="showDepositModal()">Deposit</button>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card support-pane-card">
            <div class="card-body">
              <table class="table small">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Status</th>
                        <th>Transaction</th>
                        <th>Amount</th>
                        <th>Ref ID</th>
                        <th>Date</th>
                        <th>More</th>
                    </tr>
                </thead>
                <tbody class="load-user-transaction">
                    <tr>
                        <td>Loading...</td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- content-wrapper ends -->

    <!-- Modal -->
    <div id="deposit-modal" class="modal fade small" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Credit Card Payment</h4>
          </div>
          <div class="modal-body">
                <!-- form card cc payment -->
                <div class="card" id="start-transaction">
                    <div class="card-body">
                        <h3 class="text-center" id="title-deposit-amount"></h3>
                        <hr>
                        <form class="deposit-form" role="form" onsubmit="return processDeposit()" autocomplete="on">
                            <div class="form-group">
                                <label>Card Number</label>
                                <input type="text" id="cc_nn" class="form-control" autocomplete="on" maxlength="20" pattern="\d{16}" title="Credit card number" placeholder="Enter card number" required="">
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12">Card Exp. Date</label>
                                <div class="col-md-4">
                                    <select class="form-control" id="cc_exp_mo">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" id="cc_exp_yr">
                                        <option value="18">2018</option>
                                        <option value="19">2019</option>
                                        <option value="20">2020</option>
                                        <option value="21">2021</option>
                                        <option value="22">2022</option>
                                        <option value="23">2023</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="password" class="form-control" autocomplete="on" maxlength="3" pattern="\d{3}" id="cc_cvv" title="Three digits at back of your card" required="" maxlength="3" placeholder="CVC">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <input type="password" class="form-control" id="cc_pin" name="" placeholder="****" required="">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-default">
                                        Submit 
                                        <img src="svg/loading.svg" class="loading" style="display: none;">
                                    </button>
                                </div>
                            </div>
                        </form>

                        <form class="verify-deposit" style="display: none;" onsubmit="return verifyTransaction();">
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <div id="charge_message"></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <input type="hidden" id="charge_refrence" value="">
                                    <input type="number" step="any" min="0" class="form-control" id="charge_otp" name="" placeholder="****" required="">
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">
                                        Verify 
                                        <img src="svg/loading.svg" class="verify-loading" style="display: none;">
                                    </button>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /form card cc payment -->
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- scripts here --}}
  <script type="text/javascript">
    
  </script>
@endsection