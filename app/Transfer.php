<?php

namespace EDEV;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use EDEV\Account;
use EDEV\Transaction;
use Auth;

class Transfer extends Model
{
    /*
    |---------------------------------------------
    | PROCESS INTERNAL TRANSFER
    |---------------------------------------------
    */
    public function sendAmount($request){
    	$user_id = Auth::user()->id;

    	$account_id = $request->account_id;
    	$amount 	= $request->amount;
    	$note 		= $request->note;

    	// check user account balance
        $check_account = new Account();
        if(!$check_account->verifyBalance($user_id, $amount)){
            $data = [
                'status'    => 'error',
                'message'   => 'insufficient balance!',
            ];
        }else{
            // process payload data bundle top up
            $data = $this->processInternalTransfer($user_id, $account_id, $amount, $note);
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | COMPLETE INTERNAL TRANSFER
    |---------------------------------------------
    */
    public function processInternalTransfer($user_id, $account_id, $amount, $note){
    	// check if account exist
    	$account_exist = Account::where("account_id", $account_id)->first();
    	if($account_exist !== null){
    		$charge_account = new Account();
    		// debit sender account
    		$charge_account->debitAccount($user_id, $amount);

    		// credit receiver
    		if($charge_account->creditAccount($account_exist->user_id, $amount)){
    			// body
        		$transaction = new Transaction();
    			$transaction->saveInternalTransferTransaction($user_id, $account_id, $amount, $note);

    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> 'Transfer successful',
    			];
    		}else{
    			$data = [
    				'status' 	=> 'error',
    				'message' 	=> 'Transfer not successful',
    			];
    		}
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $account_id.' does not exist!'
    		];
    	}

    	return $data;
    }

    /*
    |---------------------------------------------
    | INITIATE TRANSFER RECIEPTS
    |---------------------------------------------
    */
    public function initiateTransfer($payload){

        $endpoint = env("PS_TRANSFER_RECEIPT");
        $headers  = array(
            'Content-Type: application/json',
            'Authorization: Bearer '.env("PS_SK_KEY")
        );

        $query = array( 
           "type"           => "nuban",
           "name"           => $payload->names,
           "description"    => $payload->note,
           "account_number" => $payload->account,
           "bank_code"      => $payload->bcode,
           "currency"       => "NGN",
           "metadata"       => array("Job" => "E-devs e-wallet client")
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        $data = json_decode($result, true);

        // log transaction request

        // return
        return $data;

        curl_close($ch);
    }

    /*
    |---------------------------------------------
    | COMPLETE EXTERNAL TRANSFER
    |---------------------------------------------
    */
    public function processExternalTansfer($payload){
        $user_id    = Auth::user()->id;
        $account    = Account::where("user_id", $user_id)->first();
        $account_id = $account->id;
        $amount     = $payload->amount;
        $note       = $payload->note;

        // body
        $txRef = 'edevs_trans'.rand(000,999).rand(111,999).rand(222,999);

        // complete transfer
        $endpoint = env("PS_TRANSFER_ENDPOINT");
        $headers  = array(
            'Content-Type: application/json',
            'Authorization: Bearer '.env("PS_SK_KEY")
        );

        $query = array( 
           "type"           => "nuban",
           "source"         => "balance",
           "reason"         => trim($payload->note),
           "amount"         => $payload->amount * 100,
           "recipient"      => $payload->reciept_ref,
           "currency"       => "NGN",
           "reference"      => $txRef
        );

        // charge amount
        $flat_fee = env("TRANSFER_CHARGE");

        // verified account balance 
        if($this->verifyUserWalletBalance($payload->amount, $flat_fee)){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $endpoint);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
            curl_setopt($ch, CURLOPT_TIMEOUT, 200);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($ch);

            // transactions
            $data = json_decode($result, true);

            // close link
            curl_close($ch);

            if($data["status"] !== false){

                // debit sender account
                $charge_account = new Account();
                $charge_account->debitAccount($user_id, $amount);

                // log transaction
                $this->logBankToBankTransfer($user_id, $result, $amount, $note, $data);
                return $data = [
                    'status'    => true,
                    'message'   => 'Transfer was successful!',
                ];
            }else{
                return $data = [
                    'status'    => false,
                    'message'   => 'Transfer was not successful, Try again!',
                ];
            }
        }else{
            return $data = [
                'status'    => false,
                'message'   => 'Insufficient funds!, You can <a href="/deposit">topup e-wallet click here</a>',
            ];
        }
    }

    /*
    |---------------------------------------------
    | LOG INTER BANK TRANSFER
    |---------------------------------------------
    */
    public function logBankToBankTransfer($user_id, $result, $amount, $note, $payload){
        $transactions = new Transaction();
        $transactions->saveExternalTransferTransaction($user_id, $result, $amount, $note, $payload);
    }

    /*
    |---------------------------------------------
    | FETCH ALL ACCEPTED BANK
    |---------------------------------------------
    */
    public function fetchBankList(){
        
        // charge endpoint
        $endpoint   = env("PS_BANK_LIST");
        $headers    = array('Content-Type: application/json', 'Authorization: Bearer '.env("PS_SK_KEY"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);

        // return response()->json($res); 
        $data = json_decode($res, true);

        // return
        return $data;

        curl_close($ch);
    }

    /*
    |---------------------------------------------
    | RESOLVE BANK ACCOUNT INFO
    |---------------------------------------------
    */
    public function resolveBank($payload){
        $endpoint = env("PS_BANK_INFO");
        $headers  = array(
            'Content-Type: application/json',
            'Authorization: Bearer '.env("PS_SK_KEY")
        );

        $query = "/?account_number=".$payload->account."&bank_code=".$payload->bcode;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint.$query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        $data = json_decode($result, true);

        // return
        return $data;

        curl_close($ch);
    }

    /*
    |---------------------------------------------
    | VERIFY USER WALLET BALANCE
    |---------------------------------------------
    */
    public function verifyUserWalletBalance($amount, $fee){
        $charge_amount = $amount + $fee;
        $user_id = Auth::user()->id;
        $account = Account::where('user_id', $user_id)->first();
        if($account->balance < $charge_amount){
            return false;
        }else{
            return true;
        }
    }

    /*
    |---------------------------------------------
    | GET PAYSTACK BALANCE
    |---------------------------------------------
    */
    public function paystackBalance(){
        $endpoint = env("PS_TRANSFER_BALANCE");
        $headers  = array(
            'Authorization: Bearer '.env("PS_SK_KEY")
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        $data = json_decode($result, true);

        // return
        return $data;

        curl_close($ch);
    }

    /*
    |---------------------------------------------
    | GET ADMIN BALANCE
    |---------------------------------------------
    */
    public function getAdminBalance(){
        $account    = new Account(); 
        $data       = $account->getTotalBalance();

        return $data;
    }

    /*
    |---------------------------------------------
    | GET MOBILE AIRTIME BALANCE
    |---------------------------------------------
    */
    public function getMobileBalance(){
        $url        = env("MOBILE_BAL_URL");
        $user_id    = env("SMS_API_USER");
        $password   = env("SMS_API_PASS");

        // $endpoint = $url.'?userid='.$user_id.'&?pass='.$password;
        $endpoint = "https://mobileairtimeng.com/httpapi/balance.php?userid=09092367163&pass=f9d492f89cf650d60468d40";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $result = curl_exec($ch);

        // return
        return number_format($result, 2);

        curl_close($ch);
    }

    /*
    |---------------------------------------------
    | GET MOBILE AIRTIME BALANCE
    |---------------------------------------------
    */
    public function getFlutterBalance(){
        
        $data = json_encode([
            "currency" => "NGN"
        ]);

        $key    = $this->flutterwaveEncrypt(env("FLW_LIVE_SEC_KEY")); 
        $query  = $this->encrypt3es($data, $key);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://ravesandboxapi.flutterwave.com/v2/gpx/balance");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query)); //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        $headers = array('Content-Type: application/json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec($ch);
    }


    /*
    |---------------------------------------------
    | ENCRYPT FLUTTERWAVE
    |---------------------------------------------
    */
    public function flutterwaveEncrypt($seckey){
        $hashedkey = md5($seckey);
        $hashedkeylast12 = substr($hashedkey, -12);

        $seckeyadjusted = str_replace("FLWSECK-", "", $seckey);
        $seckeyadjustedfirst12 = substr($seckeyadjusted, 0, 12);

        $encryptionkey = $seckeyadjustedfirst12.$hashedkeylast12;
        return $encryptionkey;
    }   

    /*
    |---------------------------------------------
    | ENCRYPT FLUTTERWAVE WITH 3DS
    |---------------------------------------------
    */
    public function encrypt3es($data, $key){
        $encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);
        return base64_encode($encData);
    }
}
