@extends('layouts.auth-skin')

{{-- title section --}}
@section('title')
	E-Devs | Sign Up 
@endsection

{{-- contents --}}
@section('contents')
	<section class="site-section bg-light">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
        </div>
        
        <div class="col-md-4">
          <div class="box card-feel">
            <span class="lead">Create new account
              <span class="pull-right">
                <a href="{{url('/')}}"><i class="fa fa-arrow-left"></i> Home</a>
              </span>
            </span><hr />
            <form onsubmit="return signupUser()" method="post">
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="name">First</label>
                  <input type="text" id="first_name" class="form-control" placeholder="Enter name" required="">
                </div>
                <div class="col-md-12 form-group">
                  <label for="name">Last</label>
                  <input type="text" id="last_name" class="form-control" placeholder="Enter name" required="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="name">Email</label>
                  <input type="text" id="email" class="form-control" placeholder="Enter email address" required="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="name">Password</label>
                  <span class="pull-right">
                    <a href="javascript:void(0);" onclick="showPassword()">
                      <i class="fa fa-eye"></i>
                    </a>
                  </span>
                  <input type="password" id="password" class="form-control" placeholder="Choose a password" required="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <button type="submit" class="btn btn-primary btn-sm">
                    <span id="submit-btn">Register</span> 
                    <span id="loading-registration" style="display: none;"> 
                      <img src="{{asset('svg/loading.svg')}}"> We are setting up your account !
                    </span>
                  </button>
                  <br /><br />
                  <span class="lead">I already have an account? <a href="{{url('/login')}}">Login</a></span>
                </div>
              </div>
              <hr />
              <div class="row">
                <div class="col-md-12 form-group">
                  <span class="lead">Signup with</span>
                  &nbsp; &nbsp;
                  <a class="text-primary" href="javascript:void(0);">
                    <i class="fa fa-facebook"></i>acebook
                  </a> 
                  &nbsp;
                  or  
                  &nbsp;
                  <a href="javascript:void(0);">
                    <i class="fa fa-google"></i>oogle
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>

        <div class="col-md-4">
        </div>
      </div>
    </div>
  </section>
@endsection


{{-- scripts --}}
@section('scripts')
	{{-- scripts --}}
  <script type="text/javascript">
    function signupUser() {
      $("#loading-registration").fadeIn();
      $("#submit-btn").hide();
      var token           = $("#token").val();
      var first_name      = $("#first_name").val();
      var last_name       = $("#last_name").val();
      var names           = last_name +' '+ first_name;
      var email           = $("#email").val();
      var password        = $("#password").val();

      // query string
      var params = {
        _token: token,
        email: email,
        names: names,
        password: password
      };

      // post query 
      $.post('{{url('register')}}', params, function(data, textStatus, xhr) {
        if(data.status == 'success'){
          swal("Welcome!", "Registration successful !", "success");
          window.location.href = '/dashboard';
        }else{
          // return error message
          swal("Ops!", data.message, "error");
          $("#loading-registration").fadeOut();
          $("#submit-btn").show();
        }
      }).fail(function(err){
        $("#loading-registration").fadeOut();
        $("#submit-btn").show();
      });

      // void form
      return false;
    }
  </script>
@endsection