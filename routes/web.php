<?php
/*
|---------------------------------------------------------------------------------------------------
| APPLICATION TEST MODE URI
|---------------------------------------------------------------------------------------------------
*/
Route::get('/new/index',					'TestPagesController@index');
Route::get('/test-mail/1',					'TestMailController@testMail1');
Route::get('/test-mail/2',					'TestMailController@testMail2');
Route::get('/test-mail/3',					'TestMailController@testMail3');
Route::get('/test-mail/4',					'TestMailController@testMail4');
Route::get('/test-pusher',					'TestPuhserController@fireEvent');

/*
|---------------------------------------------------------------------------------------------------
| APPLICATION ENTRY POINT URI
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/', 							'ExternalPagesController@index')->name('index');
Route::get('/blogs', 						'ExternalPagesController@index')->name('index');
Route::get('/docs',							'ExternalPagesController@docs')->name('docs');
Route::get('/docs/guide',					'ExternalPagesController@docsGuide')->name('docs_guide');
Route::get('/terms',						'ExternalPagesController@terms')->name('terms');
Route::get('/policy',						'ExternalPagesController@policy')->name('policy');

/*
|---------------------------------------------------------------------------------------------------
| LOGIN OR 	REGISTER NEW USER URI
|---------------------------------------------------------------------------------------------------
*/
Route::get('/register',						'RegisterUserController@showRegister')->name('show_signup_form');
Route::post('/register',					'RegisterUserController@processRegister')->name('process_signup_form');
Route::get('/login', 						'LoginUserController@showLogin')->name('login');
Route::post('/login', 						'LoginUserController@processLogin')->name('process_login_form');

/*
|---------------------------------------------------------------------------------------------------
| ACCOUNT VERIFICATION AND CONFIRMATION ENTRY POINT URI
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/account/confirmation',			'AccountConfirmationController@verify');

/*
|---------------------------------------------------------------------------------------------------
| CLIENT PAGES AND VIEWS URI
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/dashboard', 					'ClientPagesController@dashboard')->name('dashboard');
Route::get('/transactions', 				'ClientPagesController@transactions')->name('transactions');
Route::get('/notifications', 				'ClientPagesController@notifications')->name('notifications');
Route::get('/airtime', 						'ClientPagesController@airtime')->name('airtime');
Route::get('/transfer', 					'ClientPagesController@transfer')->name('transfer');
Route::get('/reports', 						'ClientPagesController@reports')->name('reports');
Route::get('/bills', 				        'ClientPagesController@bills')->name('bills');
Route::get('/withdraw', 				    'ClientPagesController@withdraw')->name('withdraw');
Route::get('/deposit', 				        'ClientPagesController@deposit')->name('deposit');
Route::get('/setting', 				        'ClientPagesController@setting')->name('setting');
Route::get('/logout', 				        'ClientPagesController@logout')->name('logout');
Route::get('/business',						'ClientPagesController@business')->name('add_business');
Route::get('transaction-details/{id}',		'ClientPagesController@singleTransaction')->name('single_transaction');


/*
|---------------------------------------------------------------------------------------------------
| CLIENT PAGES AND VIEWS URI
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/load/user/transactions',		'ClientJsonResponseController@getUserTransaction');
Route::get('/load/user/account/balance',	'ClientJsonResponseController@getUserAccountBalance');
Route::get('/load/single/transaction',		'ClientJsonResponseController@getSingleTransaction');
Route::get('/load/total/deposit',			'ClientJsonResponseController@totalDeposit');
Route::get('/load/total/withdraw',			'ClientJsonResponseController@totalWithdraw');
Route::get('/load/total/success',			'ClientJsonResponseController@totalSuccessfulTransaction');
Route::get('/load/total/pending',			'ClientJsonResponseController@totalPendingTransaction');
Route::get('/load/total/failed',			'ClientJsonResponseController@totalFailedTransaction');
Route::get('/load/transactions/stats',      'ClientJsonResponseController@loadTransactionStats');
Route::get('/get/dstv/packages',			'ClientJsonResponseController@getDstvPackages');
Route::get('/load/cable/services',			'ClientJsonResponseController@getAllCableProducts');
Route::get('/load/cable/services/{id}',		'ClientJsonResponseController@getCableServices');

Route::get('/account/activation/status',	'ClientJsonResponseController@getAccountStatus');
Route::post('/resend/activation/link',		'ClientJsonResponseController@resendActivation');


Route::post('/create/new/business',			'ClientJsonResponseController@createNewBusiness');
Route::get('/load/business/information',	'ClientJsonResponseController@loadBusiness');


Route::post('/create/user/bank/info',		'ClientJsonResponseController@addUserBankInfo');
Route::get('/loan/user/bank/info',			'ClientJsonResponseController@getUserBank');
Route::get('/get/agent/list',				'ClientJsonResponseController@getUserAgents');

Route::post('/client/upload/avatar', 		'ClientJsonResponseController@uploadAvatar');
Route::post('/client/update/biodata',		'ClientJsonResponseController@updateBiodata');
Route::get('/load/profile/information',		'ClientJsonResponseController@loadProfileInfo');
Route::post('/client/password/change',		'ClientJsonResponseController@changePassword');



/*
|---------------------------------------------------------------------------------------------------
| AIRTIME AND DATA TOPUP PAYMENTS URI 
|---------------------------------------------------------------------------------------------------
|
*/
Route::post('/process/mobile/topup', 		"AirtimeController@processTopupMobile");




/*
|---------------------------------------------------------------------------------------------------
| CLIENT PAYMENTS URI 
|---------------------------------------------------------------------------------------------------
|
*/
Route::post('/charge/client/card', 			'ClientPaymentsController@chargeCard')->name('accept_deposit');
Route::post('/charge/verification',			'ClientPaymentsController@verifyCharge')->name('verify_deposit');
Route::post('/create/transfer/reciept',		'ClientPaymentsController@createReciept')->name('create_reciept');
Route::post('/process/internal/transfer',	'ClientPaymentsController@internalTransfer')->name('tt_transfer');
Route::get('/get/bank/list',				'ClientPaymentsController@getAcceptedBankList')->name('bank_list');
Route::get('/resolve/account/bin',			'ClientPaymentsController@resolveBankAccount')->name('bank_info');
Route::post('/complete/transfer/b2b',		'ClientPaymentsController@completeTransfer')->name('complete_transfer');
Route::get('/fetch/cable/customer/info', 	'ClientPaymentsController@fetchCustomerInfo');

/*
|---------------------------------------------------------------------------------------------------
| ACCOUNT VERIFICATION AND CONFIRMATION ENTRY POINT URI
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/get/transaction/graph/data',	'GraphDataController@transactionGraphData');




/*
|---------------------------------------------------------------------------------------------------
| ADMIN AUTHENTICATION SECTION
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/admin/login',					'AdminAuthenticationController@showAuthLogin');
Route::post('/admin/login',					'AdminAuthenticationController@processLogin');

/*
|---------------------------------------------------------------------------------------------------
| ADMIN PAGES CONTROLLER
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/admin/dashboard',				'AdminPagesController@dashboard');
Route::get('/admin/settings',				'AdminPagesController@settings');
Route::get('/admin/transactions',			'AdminPagesController@transactions');
Route::get('/admin/audits',					'AdminPagesController@audits');
Route::get('/admin/users',					'AdminPagesController@users');
Route::get('/admin/logout',					'AdminPagesController@logout');
Route::get('/admin/view/product/{id}',		'AdminPagesController@viewOneProduct');

/*
|---------------------------------------------------------------------------------------------------
| ADMIN PAGES CONTROLLER
|---------------------------------------------------------------------------------------------------
|
*/
Route::get('/admin/get/balance/1',			'AdminJsonResponseController@getPaystackBalance'); // paystack
Route::get('/admin/get/balance/2',			'AdminJsonResponseController@getPlatformBalance'); // edevs
Route::get('/admin/get/balance/3',			'AdminJsonResponseController@getMobileAirtimeBalance'); // mobile airtime
Route::get('/admin/get/balance/4',			'AdminJsonResponseController@getFlutterwaveBalance'); // flutterwave
Route::get('/admin/get/all/users',			'AdminJsonResponseController@getAllUsers');
Route::get('/admin/all/transaction/stats',	'AdminJsonResponseController@getTransactionStats');
Route::post('/admin/add/product',			'AdminJsonResponseController@addProduct');
Route::get('/admin/get/product',			'AdminJsonResponseController@getProducts');
Route::get('/admin/get/product/{id}',		'AdminJsonResponseController@getSingleProduct');
Route::post('/admin/add/service',			'AdminJsonResponseController@addServices');
Route::get('/admin/get/services/{id}',		'AdminJsonResponseController@getServices');
Route::get('/admin/airtime/service/{id}',	'AdminJsonResponseController@getAirtimeServices');

/*
|---------------------------------------------------------------------------------------------------
| DEVELOPMENT SECTION CALL TRIGGER FOR APP MAINTAINANCE
|---------------------------------------------------------------------------------------------------
|
*/
// clear config & caches files
Route::get('/dev-clear', function (){
	Artisan::call('config:clear');
	Artisan::call('view:clear');	
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	return redirect()->back();
});

// start maintainance
Route::get('/dev-down', function (){
	Artisan::call('down');
	return redirect()->back();
});

// stop maintainance
Route::get('/dev-up', function (){
	Artisan::call('up');
	return redirect()->back();
});

// application upgrading mode
Route::get('/dev-mode', function(){
	return view('dev-mode.dev-mode');
});